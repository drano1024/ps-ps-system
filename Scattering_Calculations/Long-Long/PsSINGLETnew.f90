Program PsSINGLETnew
#ifdef _OPENMP
	use omp_lib
#endif
	implicit none
	integer, parameter :: nfi=60
	real*8 rx(nfi),rw(nfi),rx0(nfi),rw0(nfi),lcfac,pi
	real*8 start,finish,time
	real*8 costp(nfi),sintp(nfi),ang(nfi,nfi,nfi)
	real*8 rcm(nfi,nfi,nfi,nfi),rcp(nfi,nfi,nfi,nfi)
	real*8 rij(nfi,nfi,nfi),volelemr(nfi,nfi),volelemt(nfi,nfi)
	real*8 sls1(nfi),sls2(nfi),sls(nfi),test,alp,bet,gam
	real*8 c13,c23,c34,expqt,f,fac,pinv,pot,qtpinv,r0rcmt,r0rcpt
	real*8 r0t,r12,r12andt,r14,r1c1,r23,r2c2,r34,rijt,rt1,rt2
	real*8 sint,vol,volrt,voltt,w0t,w1t,w2t,wa,wtp,wtt,r12angt
	real*8 mu,kk,cls1(nfi),cls2(nfi),cls(nfi)
	real*8 rt1d,rt2d,r1c1d,r2c2d,rcmd(nfi,nfi,nfi,nfi),rcpd(nfi,nfi,nfi,nfi)
	real*8 volelemrd(nfi,nfi,nfi),rijd(nfi,nfi,nfi),rijtd,volrtd
	real*8 r0rcptd,r0rcmtd,wttd,r12angtd,r12d,r34d,r14d,r23d,potd
	real*8 exppv,R0p,vold,fd,sinfac,sintd,cost,shf,shfd
	real*8 clc1(nfi),clc2(nfi),clc(nfi),dshf,ddshf,f1,f2
	real*8 costd,f1d,f2d,sinfacd,slc1(nfi),slc2(nfi),slc(nfi)
	integer iphi,ir0,ir1,ir2,it1,it2,qr0,i,j
	integer qr,qtp,diff,seconds,minutes
	logical slsi,clsi,clci,slci
	pi = 4.d0*ATAN(1.d0)
	pinv = 1.d0/pi
	call cpu_time(start)
	!Long-Long Range Integrals
	slsi = .true.
	clsi = .true.
	clci = .true.
	slci = .true.
	
#ifdef _OPENMP
	print *, 'We are using',OMP_GET_NUM_THREADS(),' threads'
#endif

	open(unit=1,file='fort.new',status='old')
	open(unit=5,file='constnew',status='unknown') !linear powers for phi_i
	open(unit=12,file='outonenew',status='unknown') !General information output
	open(unit=11,file='Plot',status='unknown') !output file for plot of phase-shifts
	open(unit=13,file='DirectExchange',status='unknown')

	read(1,*) mu
	read(1,*) qr0,qr,qtp
	qtpinv = 1.d0/real(qtp,8)
	wtp = pi*qtpinv
	wa = 2.d0*wtp*wtp*wtp
	sls1=0.d0
	sls2=0.d0
	sls=0.d0
	cls1=0.d0
	cls2=0.d0
	cls=0.d0
	clc1=0.d0
	clc2=0.d0
	clc=0.d0
	slc1=0.d0
	slc2=0.d0
	slc=0.d0
	
	print *, qr0,qr,qtp
	call gaulag(rx0,rw0,qr0,nfi)
	call gaulag(rx,rw,qr,nfi)

	!ANGULAR QUADRATURE POINTS CALCULATIONS
	do it1=1,qtp
		costp(it1)=Cos((2.d0*it1-1.d0)*pi*0.5d0*qtpinv)
		sintp(it1)=Sqrt(1.d0-costp(it1)**2.d0)
	end do!it1

	!ANGULAR CALCULATIONS
	do it1=1,qtp
		do it2=1,qtp
			do iphi=1,qtp
				ang(it1,it2,iphi)=costp(it1)*costp(it2)+sintp(it1)*sintp(it2)*costp(iphi)
			end do!iphi
			volelemt(it1,it2)=sintp(it1)*sintp(it2)
		end do!it2
	end do!it1
	
	!RADIAL & ANGULAR CALCULATION
	do ir1=1,qr
		rt1 = rx(ir1)
		rt1d = 2.d0*rx(ir1)
		do it1=1,qtp
			r1c1=rt1*costp(it1)
			r1c1d = rt1d*costp(it1)
			do ir2=1,qr
				rt2 = rx(ir2)
				rt2d = 2.d0*rx(ir2)
				do it2=1,qtp
					r2c2=rt2*costp(it2)
					r2c2d=rt2d*costp(it2)
					rcp(ir1,ir2,it1,it2)=r1c1+r2c2
					rcm(ir1,ir2,it1,it2)=r1c1-r2c2
					rcpd(ir1,ir2,it1,it2)=r1c1d+r2c2d
					rcmd(ir1,ir2,it1,it2)=r1c1d-r2c2d
				end do!qtp
			end do!ir1
		end do!it1
	end do!ir0

	!RADIAL
	do ir2=1,qr
		rt2 = rx(ir2)
		rt2d = 2.d0*rx(ir2)
		do ir1=1,qr
			rt1 = rx(ir1)
			rt1d = 2.d0*rx(ir1)
			volelemr(ir1,ir2)=(rt1**2.d0)*(rt2**2.d0)
			do ir0=1,qr
				r0t = rx0(ir0)
				rij(ir0,ir1,ir2)=r0t**(2.d0)+0.25d0*(rt1**(2.d0)+rt2**(2.d0))
				rijd(ir0,ir1,ir2)=r0t**(2.d0)+0.25d0*(rt1d**(2.d0)+rt2d**(2.d0))
				volelemrd(ir1,ir2,ir0) = r0t*(rt1d**2.d0)*(rt2d**2.d0)
			end do!ir2
		end do!ir1
	end do!ir0
	
	print *, "Beginning Integral Calculations"
	!Long-Long range integrals calculated here.
	kk=0.d0
	!$OMP PARALLEL DEFAULT(SHARED)
	!$OMP DO PRIVATE(kk,i)
	do i=1,12
		kk = i*0.05d0
		do ir2=1,qr
			rt2 = rx(ir2)
			rt2d = 2.d0*rx(ir2)
			w2t = rw(ir2)
			do ir1=1,qr
				rt1 = rx(ir1)
				rt1d = 2.d0*rx(ir1)
				w1t = rw(ir1)
				volrt = volelemr(ir1,ir2)
				do ir0=1,qr
					r0t = rx0(ir0)
					w0t = rw(ir0)
					rijt = rij(ir0,ir1,ir2)
					rijtd = rijd(ir0,ir1,ir2)
					volrtd = volelemrd(ir1,ir2,ir0)
					expqt = Exp(r0t)
					wtt=w0t*w1t*w2t*wa*expqt
					wttd = 4.d0*wtt
					do it1=1,qtp
						do it2=1,qtp
							r0rcpt=r0t*rcp(ir1,ir2,it1,it2)
							r0rcmt=r0t*rcm(ir1,ir2,it1,it2)
							r0rcptd=r0t*rcpd(ir1,ir2,it1,it2)
							r0rcmtd=r0t*rcmd(ir1,ir2,it1,it2)
							voltt=volelemt(it1,it2)
							do iphi=1,qtp
								sint = Sin(kk*r0t)
								cost = Cos(kk*r0t)
								r12angt=0.5d0*rt1*rt2*ang(it1,it2,iphi)
								r12=Sqrt(rijt-r0rcmt-r12angt)
								r14=Sqrt(rijt+r0rcpt+r12angt)
								r23=Sqrt(rijt-r0rcpt+r12angt)
								r34=Sqrt(rijt+r0rcmt-r12angt)
								pot=2.d0/r12+2.d0/r34-2.d0/r14-2.d0/r23
								vol=volrt*wtt*voltt
								shf=(1.d0-Exp(-mu*r0t)*(1.d0+mu*r0t/2.d0))
								
								if(slsi) then
									!Direct (SLS)
									f=pot*vol*sint**2.d0
									sls1(i)=sls1(i)+f
								end if
								
								if(clsi) then
									!Direct (CLS)
									f=pot*sint*cost*shf*vol
									cls1(i)=cls1(i)+f
								end if
								
								if(clci) then
									!Direct (CLC)
									f1=((mu**3.d0)*r0t)+pot*(-2.d0+2.d0*Exp(mu*r0t)-mu*r0t)+2.d0*mu*kk*(1.d0+mu*r0t)*Tan(kk*r0t)
									clc1(i)=clc1(i)+f1*(-2.d0+2.d0*Exp(mu*r0t)-mu*r0t)*(cost**2.d0)*vol*Exp(-2.d0*mu*r0t)
								end if
								
								if(slci) then
									!Direct (SLC)
									f1=((mu**3.d0)*r0t)+pot*(-2.d0+2.d0*Exp(mu*r0t)-mu*r0t)+2.d0*mu*kk*(1.d0+mu*r0t)*Tan(kk*r0t)
									slc1(i)=slc1(i)+f1*vol*cost*sint*Exp(-mu*r0t)
								end if
								
								r12angtd=0.5d0*rt1d*rt2d*ang(it1,it2,iphi)
								r12d=Sqrt(rijtd-r0rcmtd-r12angtd)
								r14d=Sqrt(rijtd+r0rcptd+r12angtd)
								r23d=Sqrt(rijtd-r0rcptd+r12angtd)
								r34d=Sqrt(rijtd+r0rcmtd-r12angtd)
								R0p=0.5d0*Sqrt(rt1d**(2.d0)+rt2d**(2.d0)-4.d0*r12angtd)
								potd=2.d0/r12d+2.d0/r34d-2.d0/r14d-2.d0/r23d
								exppv = Exp(-0.5d0*(r14d+r23d))
								sinfac = sint*Sin(kk*R0p)
								vold=volrtd*wttd*voltt*exppv*sinfac
								shfd=(1.d0-Exp(-mu*R0p)*(1.d0+mu*R0p/2.d0))
								
								if(slsi) then
									!Exchange (S'LS)
									fd=vold*potd/R0p
									sls2(i)=sls2(i)+fd
								end if
								
								vold=exppv*wttd*volrtd*voltt
								sinfac=sint*Cos(kk*R0p)/R0p
								if(clsi) then
									!Exchange (C'LS)
									cls2(i)=cls2(i)+potd*vold*shfd*sinfac
								end if
								
								if(clci) then
									!Exchange (C'LC)
									costd=Cos(kk*R0p)
									f1d=(r0t*mu**3.d0+potd*(-2.d0+2.d0*Exp(mu*r0t)-mu*r0t)+2.d0*mu*kk*(1.d0+mu*r0t)*Tan(kk*r0t))/R0p
									clc2(i)=clc2(i)+f1d*(-2.d0+2.d0*Exp(mu*R0p)-mu*R0p)*vold*cost*costd*Exp(-mu*(r0t+R0p))
								end if
								
								if(slci) then
									!Exhcange (S'LC)
									slc2(i)=slc2(i)+f1d*Sin(kk*R0p)*cost*vold*Exp(-mu*r0t)
								end if
							end do
						end do!iphi
					end do!it2
				end do!it1
			end do!ir2
		end do!ir1
		print *, 'Kappa Value',kk
	end do!ir0
	!$OMP END DO
	!$OMP END PARALLEL

	kk = 0.d0
	!$OMP SINGLE
	do i=1,12
		kk = kk+0.05d0
		fac=1.d0/(16.d0*pi*kk)
		
		!Singlet is (+) between 1 and 2 integrals
		!Triplet is (-) between 1 and 2 integrals
		!Change as needed
		sls1(i) = sls1(i)*fac
		sls2(i) = sls2(i)*fac
		sls(i) = 2.d0*(sls1(i)+sls2(i))
		cls1(i) = cls1(i)*fac
		cls2(i) = cls2(i)*fac
		cls(i) = 2.d0*(cls1(i)+cls2(i))
		clc1(i) = clc1(i)*fac/4.d0
		clc2(i) = clc2(i)*fac/4.d0
		clc(i) = 2.d0*(clc1(i)+clc2(i))
		slc1(i) = slc1(i)*fac/2.d0
		slc2(i) = slc2(i)*fac/2.d0
		slc(i) = 2.d0*(slc1(i)+slc2(i))
		print *, 'Kappa Value',kk
		print *, "sls:",sls1(i),sls2(i)
		print *, "cls:",cls1(i),cls2(i)
		print *, "clc:",clc1(i),clc2(i)
		print *, "slc:",slc1(i),slc2(i)
		print *, "****************************************************"
		write(13,*) 'Kappa Value',kk
		write(13,*) "sls:",sls1(i),sls2(i)
		write(13,*) "cls:",cls1(i),cls2(i)
		write(13,*) "clc:",clc1(i),clc2(i)
		write(13,*) "slc:",slc1(i),slc2(i)
		write(13,*) "****************************************************"
	end do!i
	!$OMP END SINGLE

	kk=0.d0
	do i=1,12
		kk=kk+0.05d0
		write(11,*) 'Kappa Value',real(kk,4)
		write(11,*) "sls:",sls(i)
		write(11,*) "cls:",cls(i)
		write(11,*) "clc:",clc(i)
		write(11,*) "slc:",slc(i)
		write(11,*) "**************************"
	end do!i

	call cpu_time(finish)
	diff = int((finish-start))
	seconds = int(mod(diff,60))
	minutes = int((diff-seconds)/60.0)
	time = finish - start
	write(12,*) 'Time:',time,'Seconds'
	write(12,*) 'Total Time:',minutes,'Minutes and',seconds,'Seconds'
	print *, "Time:",time,"Seconds"

End Program PsSINGLETnew

Subroutine gaulag(x,w,n,nfi)
	implicit none
	integer nfi,n
	real*8 x(nfi),w(nfi)
	real*8 anu(n)
	real*8, parameter :: eps=3.0d-13
	real*8, parameter :: pi=4.d0*atan(1.d0)
	integer j,its,i
	integer, parameter :: maxit=10
	real*8, parameter :: c1=9.084064d-1,c2=5.214976d-2
	real*8, parameter :: c3=2.579930d-3,c4=3.986126d-3
	real*8 rhs(n),r2(n),r3(n),theta(n),p1(n),p2(n),p3(n)
	real*8 pp(n),z(n),z1(n)
	logical unfinished(n)
	x=0.d0
	w=0.d0
	anu = 4.d0*n+2.d0
	rhs(1) = (4.d0*n-1.d0)*pi/anu(1)
	do i=2,n
		rhs(i)=(4.d0*n-4.d0*i+3.d0)*pi/anu(i)
	end do
	r3 = rhs**(1.d0/3.d0)
	r2 = r3**2.d0
	theta = r3*(c1+r2*(c2+r2*(c3+r2*c4)))
	z = anu*(Cos(theta)**2.d0)
	unfinished = .true.
	do its=1,maxit
		where (unfinished)
			p1=1.d0
			p2=0.d0
		end where
		do j=1,n
			where (unfinished)
				p3=p2
				p2=p1
				p1=((2.d0*real(j,8)-1.d0-z)*p2-(real(j,8)-1.d0)*p3)/real(j,8)
			end where
		end do
		where (unfinished)
			pp=(n*p1-n*p2)/z
			z1=z
			z=z1-p1/pp
			unfinished = (abs(z-z1) > eps*z)
		end where
		if (.not. any(unfinished)) exit
	end do
	if (its == maxit+1) print *, 'too many iterations in gaulag'
	x=z
	w=-1.d0/(pp*real(n,8)*p2)
End Subroutine
