Program PsSINGLETnew
#ifdef _OPENMP
	use omp_lib
#endif
	implicit none
	integer, parameter :: nfi=100
	real*8 rx(nfi),rw(nfi),rx0(nfi),rw0(nfi),pi
	real*8 start,finish,time,alp,bet,gam
	real*8 costp(nfi),sintp(nfi),ang(nfi,nfi,nfi)
	real*8 rcm(nfi,nfi,nfi,nfi),rcp(nfi,nfi,nfi,nfi)
	real*8 rij(nfi,nfi,nfi),volelemt(nfi,nfi),test
	real*8 expqt,f,fac,pinv,pot,qtpinv,r0rcmt,r0rcpt,expqv
	real*8 r0t,r12,r12andt,r14,r1c1,r23,r2c2,r34,rijt,rt1,rt2
	real*8 sint,vol,voltt,w0t,w1t,w2t,wa,wtp,wtt,r12angt,lin
	real*8 mu,kk,exppv,sinfac,cost,shf,LC
	real*8 phils1(nfi,nfi),phils2(nfi,nfi),phils3(nfi,nfi),phils4(nfi,nfi),phils(nfi,nfi)
	real*8 philc1(nfi,nfi),philc2(nfi,nfi),philc3(nfi,nfi),philc4(nfi,nfi),philc(nfi,nfi)
	real*8 volrt,R1p,R2p,R0p,linp,expp,exp2,lam
	real*8 t12,t13,t14,t23,t24,t34
	integer iphi,ir0,ir1,ir2,it1,it2,qr0,i,j,ie
	integer qr,qtp,diff,seconds,minutes
	integer mega,nt,j12(nfi),j13(nfi),j14(nfi),j23(nfi),j24(nfi),j34(nfi)
	logical philsi,philci
	character(len=1024) :: directfile,P12file,P34file,P1234file
	pi = 4.d0*ATAN(1.d0)
	pinv = 1.d0/pi
	call cpu_time(start)
	!Long-Long Range Integrals
	philsi = .true.
	philci = .true.
	
#ifdef _OPENMP
	print *, 'We are using',OMP_GET_NUM_THREADS(),' threads'
#endif

	open(unit=1,file='fort.new',status='old')
	open(unit=5,file='constnew',status='unknown') !linear powers for phi_i
	open(unit=5,file='constnew',status='unknown') !linear powers for phi_i
	open(unit=12,file='outonenew',status='unknown') !General information output
	open(unit=11,file='Plot',status='unknown') !output file for plot of phase-shifts
	
	read(1,*) mega
	call const(j12,j13,j14,j23,j24,j34,mega,nt,nfi)
	write(12,*) 'Nt=',nt
	print *, 'nt = ',nt
	read(1,*) alp
	read(1,*) bet
	read(1,*) gam
	read(1,*) mu
	read(1,*) lam
	read(1,*) qr0,qr,qtp
	write(directfile,"(A7,I1)") "Direct,",mega
	open(unit=13,file=directfile,status='unknown')
	
	write(P12file,"(A4,I1)") "P12,",mega
	open(unit=14,file=P12file,status='unknown')
	
	write(P34file,"(A4,I1)") "P34,",mega
	open(unit=15,file=P34file,status='unknown')
	
	write(P1234file,"(A6,I1)") "P1234,",mega
	open(unit=16,file=P1234file,status='unknown')
	
	qtpinv = 1.d0/real(qtp,8)
	wtp = pi*qtpinv
	wa = 2.d0*wtp*wtp*wtp
	phils1=0.d0
	phils2=0.d0
	phils=0.d0
	philc1=0.d0
	philc2=0.d0
	philc=0.d0
	
	print *, qr0,qr,qtp
	call gaulag(rx0,rw0,qr0,nfi)
	call gaulag(rx,rw,qr,nfi)

	!ANGULAR QUADRATURE POINTS CALCULATIONS
	do it1=1,qtp
		costp(it1)=Cos((2.d0*it1-1.d0)*pi*0.5d0*qtpinv)
		sintp(it1)=Sqrt(1.d0-costp(it1)**2.d0)
	end do!it1

	!ANGULAR CALCULATIONS
	do it1=1,qtp
		do it2=1,qtp
			do iphi=1,qtp
				ang(it1,it2,iphi)=costp(it1)*costp(it2)+sintp(it1)*sintp(it2)*costp(iphi)
			end do!iphi
			volelemt(it1,it2)=sintp(it1)*sintp(it2)
		end do!it2
	end do!it1
	
	!RADIAL & ANGULAR CALCULATION
	do ir1=1,qr
		rt1 = rx(ir1)/(1+lam)
		do it1=1,qtp
			r1c1=rt1*costp(it1)
			do ir2=1,qr
				rt2 = rx(ir2)/(1+lam)
				do it2=1,qtp
					r2c2=rt2*costp(it2)
					rcp(ir1,ir2,it1,it2)=r1c1+r2c2
					rcm(ir1,ir2,it1,it2)=r1c1-r2c2
				end do!qtp
			end do!ir1
		end do!it1
	end do!ir0

	!RADIAL
	do ir2=1,qr
		rt2 = rx(ir2)/(1+lam)
		do ir1=1,qr
			rt1 = rx(ir1)/(1+lam)
			do ir0=1,qr
				r0t = rx0(ir0)/(1+lam)
				rij(ir0,ir1,ir2)=r0t**(2.d0)+0.25d0*(rt1**(2.d0)+rt2**(2.d0))
			end do!ir2
		end do!ir1
	end do!ir0
	
	print *, "Beginning Integral Calculations"
	!Long-Long range integrals calculated here.
	kk=0.d0
	!$OMP PARALLEL DEFAULT(SHARED)
	!$OMP DO PRIVATE(kk,i)
	do i=1,24
		kk = i*0.025d0
		do ie=1,nt
			t12 = real(j12(ie),8)
			t13 = real(j13(ie),8)
			t14 = real(j14(ie),8)
			t23 = real(j23(ie),8)
			t24 = real(j24(ie),8)
			t34 = real(j34(ie),8)
			do ir2=1,qr
				rt2 = rx(ir2)/(1+lam)
				w2t = rw(ir2)
				do ir1=1,qr
					rt1 = rx(ir1)/(1+lam)
					w1t = rw(ir1)
					do ir0=1,qr
						r0t = rx0(ir0)/(1+lam)
						w0t = rw(ir0)
						rijt = rij(ir0,ir1,ir2)
						wtt=w0t*w1t*w2t*wa
						expqv=Exp((1.d0+lam-0.5d0)*(rt1+rt2))*Exp((1.d0+lam)*r0t)
						volrt=r0t*(rt1**2.d0)*(rt2**2.d0)
						do it1=1,qtp
							do it2=1,qtp
								r0rcpt=r0t*rcp(ir1,ir2,it1,it2)
								r0rcmt=r0t*rcm(ir1,ir2,it1,it2)
								voltt=volelemt(it1,it2)
								do iphi=1,qtp
									sint = Sin(kk*r0t)
									cost = Cos(kk*r0t)
									r12angt=0.5d0*rt1*rt2*ang(it1,it2,iphi)
									r12=Sqrt(rijt-r0rcmt-r12angt)
									r14=Sqrt(rijt+r0rcpt+r12angt)
									r23=Sqrt(rijt-r0rcpt+r12angt)
									r34=Sqrt(rijt+r0rcmt-r12angt)
									pot=1.d0/r12+1.d0/r34-1.d0/r14-1.d0/r23
									vol=wtt*voltt*volrt
									shf=1.d0-Exp(-mu*r0t)*(1.d0+0.5d0*mu*r0t)
									LC=2.d0*pot-mu*(r0t*mu**2.d0+2.d0*kk*(1.d0+mu*r0t)*Tan(kk*r0t))/(2.d0-2.d0*Exp(mu*r0t)+mu*r0t)
									lin=(r12**t12)*(rt2**t13)*(r14**t14)*(r23**t23)*(rt1**t24)*(r34**t34)
									exp2=Exp(-alp*rt2-bet*r23-gam*r34)
									
									if(philsi) then
										!Direct (phiLS)
										f=pot*vol*sint*lin*expqv*exp2
										phils1(ie,i)=phils1(ie,i)+f
									end if
									
									if(philci) then
										!Direct (phiLC)
										f=vol*cost*lin*shf*LC*expqv*exp2
										philc1(ie,i)=philc1(ie,i)+f
									end if
									
									lin=(r12**t12)*(rt2**t23)*(r14**t24)*(r23**t13)*(rt1**t14)*(r34**t34)
									exp2=Exp(-alp*r23-bet*rt2-gam*r34)
									
									if(philsi) then
										!P12 (phiLS)
										f=pot*vol*sint*lin*expqv*exp2
										phils2(ie,i)=phils2(ie,i)+f
									end if
									
									if(philci) then
										!P12 (phiLC)
										f=vol*cost*lin*shf*LC*expqv*exp2
										philc2(ie,i)=philc2(ie,i)+f
									end if
									
									lin=(r12**t12)*(rt2**t14)*(r14**t13)*(r23**t24)*(rt1**t23)*(r34**t34)
									exp2=Exp(-alp*r14-bet*rt1-gam*r34)
									
									if(philsi) then
										!P34 (phiLS)
										f=pot*vol*sint*lin*expqv*exp2
										phils3(ie,i)=phils3(ie,i)+f
									end if
									
									if(philci) then
										!P34 (phiLC)
										f=vol*cost*lin*shf*LC*expqv*exp2
										philc3(ie,i)=philc3(ie,i)+f
									end if
									
									lin=(r12**t12)*(rt2**t24)*(r14**t23)*(r23**t14)*(rt1**t13)*(r34**t34)
									exp2=Exp(-alp*rt1-bet*r14-gam*r34)
									
									if(philsi) then
										!P1234 (phiLS)
										f=pot*vol*sint*lin*expqv*exp2
										phils4(ie,i)=phils4(ie,i)+f
									end if
									
									if(philci) then
										!P1234 (phiLC)
										f=vol*cost*lin*shf*LC*expqv*exp2
										philc4(ie,i)=philc4(ie,i)+f
									end if
									
								end do
							end do!iphi
						end do!it2
					end do!it1
				end do!ir2
			end do!ir1
			print *, 'Finished ',real(kk,4),ie
		end do
	end do!ir0
	!$OMP END DO
	!$OMP END PARALLEL

	kk = 0.d0
	write(11,*) mega,nt
	write(13,*) mega,nt
	write(14,*) mega,nt
	write(15,*) mega,nt
	write(16,*) mega,nt
	!$OMP SINGLE
	do i=1,24
		do ie=1,nt
			kk = i*0.025d0
			fac=1.d0/(Sqrt(2.d0*kk)*((1+lam)**3.d0))
			
			!Singlet is (+) between 1 and 2 integrals
			!Triplet is (-) between 1 and 2 integrals
			!Change as needed
			phils1(ie,i) = phils1(ie,i)*fac
			phils2(ie,i) = phils2(ie,i)*fac
			phils3(ie,i) = phils3(ie,i)*fac
			phils4(ie,i) = phils4(ie,i)*fac
			phils(ie,i) = 2.d0*(phils1(ie,i)+phils2(ie,i)+phils3(ie,i)+phils4(ie,i))
			philc1(ie,i) = philc1(ie,i)*fac*0.5d0
			philc2(ie,i) = philc2(ie,i)*fac*0.5d0
			philc3(ie,i) = philc3(ie,i)*fac*0.5d0
			philc4(ie,i) = philc4(ie,i)*fac*0.5d0
			philc(ie,i) = 2.d0*(philc1(ie,i)+philc2(ie,i)+philc3(ie,i)+philc4(ie,i))
			print *, 'Kappa Value',real(kk,4),ie
			print *, "phils:",phils1(ie,i),phils2(ie,i),phils3(ie,i),phils4(ie,i)
			print *, "philc:",philc1(ie,i),philc2(ie,i),philc3(ie,i),philc4(ie,i)
			print *, "****************************************************"
			write(13,*) real(kk,4),ie,phils1(ie,i),philc1(ie,i)
			write(14,*) real(kk,4),ie,phils2(ie,i),philc2(ie,i)
			write(15,*) real(kk,4),ie,phils3(ie,i),philc3(ie,i)
			write(16,*) real(kk,4),ie,phils4(ie,i),philc4(ie,i)
		end do
	end do!i
	!$OMP END SINGLE

	kk=0.d0
	do i=1,24
		kk=i*0.025d0
		do ie=1,nt
			write(11,*) real(kk,4),ie,phils(ie,i),philc(ie,i)
		end do
	end do!i

	call cpu_time(finish)
	diff = int((finish-start))
	seconds = int(mod(diff,60))
	minutes = int((diff-seconds)/60.0)
	time = finish - start
	write(12,*) 'Time:',time,'Seconds'
	write(12,*) 'Total Time:',minutes,'Minutes and',seconds,'Seconds'
	print *, "Time:",time,"Seconds"

End Program PsSINGLETnew

Subroutine const(a1,a2,a3,a4,a5,a6,mega,nt,nfi) ! Obtained from Peter Van Reeth
	implicit none
	integer nfi
	integer a1(nfi),a2(nfi),a3(nfi),a4(nfi),a5(nfi),a6(nfi) !exponential constants!
	integer nt,mega,inx,nt2
	integer megap,i,i12,i13,i14,i23,i24,i34
	integer i12p1,i13p1,i14p1,i23p1,i24p1,i34p1
	integer i14p1m,i13p1m,i24p1m,i23p1m,j
	logical track

	megap = mega + 1
	inx = 0

	do i = 1,megap
		do i12p1 = 1,i,1
			i12 = i12p1 - 1
			i14p1m = i - i12
			do i14p1 = 1,i14p1m
				i14 = i14p1 - 1
				i13p1m = i14p1m - i14
				do i13p1 = 1,i13p1m
					i13 = i13p1 - 1
					i24p1m = i13p1m - i13
					do i24p1 = 1,i24p1m
						i24 = i24p1 - 1
						i23p1m = i24p1m - i24
						do i23p1 = 1,i23p1m
							i23 = i23p1 - 1
							i34 = i23p1m - i23p1
							track = .true.
							
							inx = inx + 1
							a1(inx) = i12
							a2(inx) = i13
							a3(inx) = i14
							a4(inx) = i23
							a5(inx) = i24
							a6(inx) = i34
						end do
					end do
				end do
			end do
		end do
		write(5,*) i,inx
	end do
	nt = inx
	write(5,*) ' N    r12    r13    r14    r23    r24    r34'
	write(5,"(7I7)") (j,a1(j),a2(j),a3(j),a4(j),a5(j),a6(j),j=1,nt)
End Subroutine

Subroutine gaulag(x,w,n,nfi)
	implicit none
	integer nfi,n
	real*8 x(nfi),w(nfi)
	real*8 anu(n)
	real*8, parameter :: eps=3.0d-13
	real*8, parameter :: pi=4.d0*atan(1.d0)
	integer j,its,i
	integer, parameter :: maxit=10
	real*8, parameter :: c1=9.084064d-1,c2=5.214976d-2
	real*8, parameter :: c3=2.579930d-3,c4=3.986126d-3
	real*8 rhs(n),r2(n),r3(n),theta(n),p1(n),p2(n),p3(n)
	real*8 pp(n),z(n),z1(n)
	logical unfinished(n)
	x=0.d0
	w=0.d0
	anu = 4.d0*n+2.d0
	rhs(1) = (4.d0*n-1.d0)*pi/anu(1)
	do i=2,n
		rhs(i)=(4.d0*n-4.d0*i+3.d0)*pi/anu(i)
	end do
	r3 = rhs**(1.d0/3.d0)
	r2 = r3**2.d0
	theta = r3*(c1+r2*(c2+r2*(c3+r2*c4)))
	z = anu*(Cos(theta)**2.d0)
	unfinished = .true.
	do its=1,maxit
		where (unfinished)
			p1=1.d0
			p2=0.d0
		end where
		do j=1,n
			where (unfinished)
				p3=p2
				p2=p1
				p1=((2.d0*real(j,8)-1.d0-z)*p2-(real(j,8)-1.d0)*p3)/real(j,8)
			end where
		end do
		where (unfinished)
			pp=(n*p1-n*p2)/z
			z1=z
			z=z1-p1/pp
			unfinished = (abs(z-z1) > eps*z)
		end where
		if (.not. any(unfinished)) exit
	end do
	if (its == maxit+1) print *, 'too many iterations in gaulag'
	x=z
	w=-1.d0/(pp*real(n,8)*p2)
End Subroutine