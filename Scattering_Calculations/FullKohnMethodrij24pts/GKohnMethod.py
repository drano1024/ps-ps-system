import numpy as np
from numpy import linalg as lin
import matplotlib.pyplot as plt
import ipywidgets as widgets
import unicodedata
import fractions
import sys

def calc(ax,stmega,P12,P34,tau,state,quad,fitdata=False):

    def tildeintegrals(sls,clc,cls,slc,pls,plc,u):
        nsls = []
        ncls = []
        nclc = []
        nslc = []
        npls = np.zeros([len(pls),2])
        nplc = np.zeros([len(pls),2])
        u00 = u[0][0]
        u01 = u[0][1]
        u10 = u[1][0]
        u11 = u[1][1]
        for i in range(len(sls)):
            nsls.append((u00**2)*sls[i]+2*u00*u01*cls[i]+(u01**2)*clc[i]+2*u00*u01)
            nclc.append((u10**2)*sls[i]+2*u10*u11*cls[i]+(u11**2)*clc[i]+2*u10*u11)
            ncls.append(u10*u00*sls[i]+(u10*u01+u11*u00)*cls[i]+(u11*u01)*clc[i]+2*u10*u01)
            nslc.append(u00*u10*sls[i]+(u11*u00+u10*u01)*cls[i]+(u11*u01)*clc[i]+2*u00*u11)
        for i in range(len(pls)):
            npls[i][0] = pls[i][0]
            npls[i][1] = u00*pls[i][1]+u01*plc[i][1]
            nplc[i][0] = plc[i][0]
            nplc[i][1] = u10*pls[i][1]+u11*plc[i][1]
        return nsls,ncls,nslc,nclc,npls,nplc
    
    def matcreate(cls,pls,clc,plc,pp,php,kk,ki,nt):
        B = np.zeros([nt+1])
        A = np.zeros([nt+1,nt+1])
        k = kk[ki]
        for i in range(nt+1):
            if i == 0:
                B[i] = (cls[ki]+0.5)
            else:
                temps = pls[nt*ki+i-1]
                B[i] = temps[1]
            for j in range(nt+1):
                if i == 0 and j == 0:
                    A[i,j] = clc[ki]
                elif i == 0 or j == 0 and j>i:
                    tempc = plc[ki*nt+j-1]
                    A[i,j] = tempc[1]
                elif i == 0 or j == 0 and j<i:
                    tempc = plc[ki*nt+i-1]
                    A[i,j] = tempc[1]
                else:
                    A[i,j] = 2*(php[i-1,j-1]-(-0.25-0.25+0.5*k**2.0)*pp[i-1,j-1])
        #print(A)
        #print("-------------------------------------")
        #print(B)
        #print("*************************************")
        return A,B
    
    def test(slc,cls):
        for i in range(len(slc)):
            print(slc[i]-cls[i])
    
    def print_inte(kk,inte):
        for i, k in enumerate(kk):
            print(k,inte[i])
    
    def read_ll(file):
        kk = []
        sls = []
        cls = []
        clc = []
        slc = []
        with open(file) as f:
            for line in f:
                l = line.strip().split()
                if l[0] == 'Kappa':
                    kk.append(float(l[-1]))
                elif l[0] == 'sls:':
                    sls.append(float(l[-1]))
                elif l[0] == 'cls:':
                    cls.append(float(l[-1]))
                elif l[0] == 'clc:':
                    clc.append(float(l[-1]))
                elif l[0] == 'slc:':
                    slc.append(float(l[-1]))
        return kk, sls, cls, clc, slc
    
    def read_ls(file):
        pls = []
        plc = []
        with open(file) as f:
            for idx, line in enumerate(f):
                l = line.strip().split()
                if idx == 0:
                    mega = int(l[0])
                    nt = int(l[1])
                else:
                    pls.append([int(l[1]),float(l[2])])
                    plc.append([int(l[1]),float(l[3])])
        return nt, pls, plc
    
    def read_ss(file):
        with open(file, 'r') as f:
            while True:
                try:
                    fline = f.readline().strip().split()
                    Omega = int(fline[0])
                    fline = f.readline().strip().split()
                    nt = int(fline[0])
                    ppintegrals = np.zeros((nt,nt))
                    phpintegrals = np.zeros((nt,nt))
                    for n in range(nt**2):
                        fline = f.readline().strip().split()
                        i = int(fline[0]) - 1
                        j = int(fline[1]) - 1
                        pp = float(fline[2])
                        php = float(fline[3])
                        ppintegrals[i][j] = pp
                        phpintegrals[i][j] = php
                except IndexError:
                    break
        return ppintegrals, phpintegrals
    
    ll_Direct = quad+'llDirect'
    ll_Exchange = quad+'llExchange'
    Full = P12 and P34
    u = np.array([[np.cos(tau),np.sin(tau)],[-np.sin(tau),np.cos(tau)]])
    sls = []
    cls = []
    clc = []
    slc = []
    pls = []
    plc = []
    #Read in long-long integrals
    kk, slsD, clsD, clcD, slcD = read_ll(ll_Direct)
    if P12 or P34:
        kk, slsE, clsE, clcE, slcE = read_ll(ll_Exchange)
        for i in range(len(kk)):
            sls.append(slsD[i] + state*slsE[i])
            cls.append(clsD[i] + state*clsE[i])
            clc.append(clcD[i] + state*clcE[i])
            slc.append(slcD[i] + state*slcE[i])
    else:
        for i in range(len(kk)):
            sls.append(slsD[i]/2)
            cls.append(clsD[i]/2)
            clc.append(clcD[i]/2)
            slc.append(slcD[i]/2)
    if not stmega == '-1':
        #Read in short-long and short-short integrals
        plc = []
        pls = []
        nt, plsD, plcD = read_ls(quad+'slDirect,'+stmega)
        php = np.zeros([nt,nt])
        pp = np.zeros([nt,nt])
        ppD, phpD = read_ss('ssDirect,'+stmega)
        if P12 and not Full:
            nt, pls12, plc12 = read_ls(quad+'slP12,'+stmega)
            pp12, php12 = read_ss('ssP12,'+stmega)
            for i in range(len(plsD)):
                pls.append([plsD[i][0],np.sqrt(2)*(plsD[i][1]+state*pls12[i][1])])
                plc.append([plcD[i][0],np.sqrt(2)*(plcD[i][1]+state*plc12[i][1])])
            for i in range(nt):
                for j in range(nt):
                    pp[i][j] = ppD[i][j]+state*pp12[i][j]
                    php[i][j] = phpD[i][j]+state*php12[i][j]
        elif P34 and not Full:
            nt, pls34, plc34 = read_ls(quad+'slP34,'+stmega)
            pp34, php34 = read_ss(quad+'ssP34,'+stmega)
            for i in range(len(plsD)):
                pls.append([plsD[i][0],np.sqrt(2)*(plsD[i][1]+state*pls34[i][1])])
                plc.append([plcD[i][0],np.sqrt(2)*(plcD[i][1]+state*plc34[i][1])])
            for i in range(nt):
                for j in range(nt):
                    pp[i][j] = ppD[i][j]+state*pp34[i][j]
                    php[i][j] = phpD[i][j]+state*php34[i][j]
        elif Full:
            nt, pls12, plc12 = read_ls(quad+'slP12,'+stmega)
            nt, pls34, plc34 = read_ls(quad+'slP34,'+stmega)
            nt, pls1234, plc1234 = read_ls(quad+'slP1234,'+stmega)
            pp12, php12 = read_ss('ssP12,'+stmega)
            pp34, php34 = read_ss(quad+'ssP34,'+stmega)
            pp1234, php1234 = read_ss(quad+'ssP12P34,'+stmega)
            for i in range(len(plsD)):
                pls.append([plsD[i][0],np.sqrt(2)*(plsD[i][1]+state*pls12[i][1]+state*pls34[i][1]+pls1234[i][1])])
                plc.append([plcD[i][0],np.sqrt(2)*(plcD[i][1]+state*plc12[i][1]+state*plc34[i][1]+plc1234[i][1])])
            for i in range(nt):
                for j in range(nt):
                    pp[i][j] = ppD[i][j]+state*pp12[i][j]+state*pp34[i][j]+pp1234[i][j]
                    php[i][j] = phpD[i][j]+state*php12[i][j]+state*php34[i][j]+php1234[i][j]
        else:
            #Direct Calcuations only
            for i in range(len(plsD)):
                pls.append([plsD[i][0],plsD[i][1]/np.sqrt(2)])
                plc.append([plcD[i][0],plcD[i][1]/np.sqrt(2)])
            pp = np.copy(ppD)
            php = np.copy(phpD)
    #Tau rotation here for long integrals
    sls, cls, slc, clc, pls, plc = tildeintegrals(sls, clc, cls, slc, pls, plc, u)
    phase = []
    phasef = []
    tphase = []
    if stmega == '-1':
        #Long-Range Scattering calculations only
        for i in range(len(kk)):
            phase.append(-(cls[i]+0.5)/clc[i])
            phasef.append(-(sls[i]+phase[i]*(cls[i]+0.5)))
            tphase.append(phasef[i]/lin.det(u))
            phasef[i] = (np.arctan(phasef[i]/lin.det(u))+tau)
    else:
        #Scattering Calculations with Short-range terms included.
        for i in range(len(kk)):
            A,B = matcreate(cls,pls,clc,plc,pp,php,kk,i,nt)
            #print(kk[i]," ",lin.det(A))
            if False:
                print('Kappa=',kk[i])
                for A0 in A:
                    print(' '.join(['%.3f'%a for a in A0]))
                print('---------------------------------------------------------')
                print(' '.join(['%.3f'%b for b in B]))
                print('*********************************************************')
            X = lin.solve(A,-B)
            Bt = B.transpose()
            phase.append(X[0])
            phasef.append(-Bt.dot(X)-sls[i])
            tphase.append(phasef[i]/lin.det(u))
            phasef[i] = (np.arctan(phasef[i]/lin.det(u))+tau)#%np.pi
    
    if P12 and not Full:
        ex = 'P12'
    elif P34 and not Full:
        ex = 'P34'
    elif Full:
        ex = 'Full'
    else:
        ex = 'Direct'
    
    if tau == 0:
        taustring = ', 0'
    else:
        taustring = ', '+'{:.2f}'.format(tau/np.pi)+unicodedata.lookup("GREEK SMALL LETTER PI")
    
    ax.plot(kk,phasef,label= ex+', '+stmega+', '+quad+taustring)
    if fitdata:
        poly = np.polyfit(kk,np.array(phasef),3)
        func = []
        for k in kk:
            func.append(np.polyval(poly,k))
        ax.plot(kk,func,label='Fitted Data')
        print('k=0 value: ',np.polyval(poly,0))

def plot(t,mega,sta,P12,P34):
    fig, ax = plt.subplots(figsize=(7,5), dpi=125)
    meg=str(int(mega))
    calc(ax,meg,P12,P34,t,sta,quad="70",fitdata=False)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',ncol=2, mode="expand", borderaxespad=0.)
    major_ticks = np.arange(0, 0.6, 0.025)
    ax.set_xticks(major_ticks,minor=True)
    ax.grid(which='major')
    ax.grid(which='minor')

if __name__ == "__main__":
    #ll_file = sys.argv[1]
    P12 = True
    P34 = False
    tau = 0
    mega = 0
    state = 1
    for m in [0,1,2]:
        for t in [0,np.pi/4,np.pi/2]:
            plot(t,m,state,P12,P34)
    #plot(0,mega,state,P12,P34)
    plt.show()
    #calc(ax,stmega,P12,P34,tau,state)