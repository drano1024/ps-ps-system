Program PsSINGLETnew
#ifdef _OPENMP
	use omp_lib
#endif
	implicit none
	integer, parameter :: nfi=100
	real*8 rx(nfi),rw(nfi),rx0(nfi),rw0(nfi),pi
	real*8 start,finish,time,alp0,alp1,alp2
	real*8 costp(nfi),sintp(nfi),ang(nfi,nfi,nfi)
	real*8 rcm(nfi,nfi,nfi,nfi),rcp(nfi,nfi,nfi,nfi)
	real*8 rij(nfi,nfi,nfi),volelemt(nfi,nfi),test,alp,bet,gam
	real*8 expqt,f,fac,pinv,pot,qtpinv,r0rcmt,r0rcpt,expqv
	real*8 r0t,r12,r12andt,r14,r1c1,r23,r2c2,r34,rijt,rt1,rt2
	real*8 sint,vol,voltt,w0t,w1t,w2t,wa,wtp,wtt,r12angt,lin
	real*8 mu,kk,exppv,sinfac,cost,shf,LC
	real*8 phils1(nfi,nfi),phils2(nfi,nfi),phils(nfi,nfi),philc1(nfi,nfi)
	real*8 philc2(nfi,nfi),philc(nfi,nfi)
	real*8 volrt,R1p,R2p,R0p,linp,expp,exp2,lam
	integer iphi,ir0,ir1,ir2,it1,it2,qr0,i,j,ie
	integer qr,qtp,diff,seconds,minutes
	integer mega,nt,j0(nfi),j1(nfi),j2(nfi)
	logical philsi,philci
	pi = 4.d0*ATAN(1.d0)
	pinv = 1.d0/pi
	call cpu_time(start)
	!Long-Long Range Integrals
	philsi = .true.
	philci = .true.
	
#ifdef _OPENMP
	print *, 'We are using',OMP_GET_NUM_THREADS(),' threads'
#endif

	open(unit=1,file='fort.new',status='old')
	open(unit=5,file='constnew',status='unknown') !linear powers for phi_i
	open(unit=5,file='constnew',status='unknown') !linear powers for phi_i
	open(unit=12,file='outonenew',status='unknown') !General information output
	open(unit=11,file='Plot',status='unknown') !output file for plot of phase-shifts
	open(unit=13,file='integrals',status='unknown')
	
	read(1,*) mega
	call const(j0,j1,j2,mega,nt,nfi)
	write(12,*) 'Nt=',nt
	print *, 'nt = ',nt
	read(1,*) alp1
	read(1,*) alp2
	read(1,*) alp0
	read(1,*) mu
	read(1,*) lam
	read(1,*) qr0,qr,qtp
	qtpinv = 1.d0/real(qtp,8)
	wtp = pi*qtpinv
	wa = 2.d0*wtp*wtp*wtp
	phils1=0.d0
	phils2=0.d0
	phils=0.d0
	philc1=0.d0
	philc2=0.d0
	philc=0.d0
	
	print *, qr0,qr,qtp
	call gaulag(rx0,rw0,qr0,nfi)
	call gaulag(rx,rw,qr,nfi)

	!ANGULAR QUADRATURE POINTS CALCULATIONS
	do it1=1,qtp
		costp(it1)=Cos((2.d0*it1-1.d0)*pi*0.5d0*qtpinv)
		sintp(it1)=Sqrt(1.d0-costp(it1)**2.d0)
	end do!it1

	!ANGULAR CALCULATIONS
	do it1=1,qtp
		do it2=1,qtp
			do iphi=1,qtp
				ang(it1,it2,iphi)=costp(it1)*costp(it2)+sintp(it1)*sintp(it2)*costp(iphi)
			end do!iphi
			volelemt(it1,it2)=sintp(it1)*sintp(it2)
		end do!it2
	end do!it1
	
	!RADIAL & ANGULAR CALCULATION
	do ir1=1,qr
		rt1 = rx(ir1)/(1+lam)
		do it1=1,qtp
			r1c1=rt1*costp(it1)
			do ir2=1,qr
				rt2 = rx(ir2)/(1+lam)
				do it2=1,qtp
					r2c2=rt2*costp(it2)
					rcp(ir1,ir2,it1,it2)=r1c1+r2c2
					rcm(ir1,ir2,it1,it2)=r1c1-r2c2
				end do!qtp
			end do!ir1
		end do!it1
	end do!ir0

	!RADIAL
	do ir2=1,qr
		rt2 = rx(ir2)/(1+lam)
		do ir1=1,qr
			rt1 = rx(ir1)/(1+lam)
			do ir0=1,qr
				r0t = rx0(ir0)/(1+lam)
				rij(ir0,ir1,ir2)=r0t**(2.d0)+0.25d0*(rt1**(2.d0)+rt2**(2.d0))
			end do!ir2
		end do!ir1
	end do!ir0
	
	print *, "Beginning Integral Calculations"
	!Long-Long range integrals calculated here.
	kk=0.d0
	!$OMP PARALLEL DEFAULT(SHARED)
	!$OMP DO PRIVATE(kk,i)
	do i=1,12
		kk = i*0.05d0
		do ie=1,nt
			do ir2=1,qr
				rt2 = rx(ir2)/(1+lam)
				w2t = rw(ir2)
				do ir1=1,qr
					rt1 = rx(ir1)/(1+lam)
					w1t = rw(ir1)
					do ir0=1,qr
						r0t = rx0(ir0)/(1+lam)
						w0t = rw(ir0)
						rijt = rij(ir0,ir1,ir2)
						expqt = Exp(r0t)
						wtt=w0t*w1t*w2t*wa*expqt
						expqv=Exp((lam-alp0)*r0t)*Exp((lam-alp1+0.5d0)*rt1)*Exp((lam-alp2+0.5d0)*rt2)
						lin=(r0t**(real(j0(ie),8)+1))*(rt1**(real(j1(ie),8)+2))*(rt2**(real(j2(ie),8)+2))
						volrt=r0t*(rt1**2.d0)*(rt2**2.d0)
						exp2 = Exp(lam*r0t+(0.5d0+lam)*(rt1+rt2))
						do it1=1,qtp
							do it2=1,qtp
								r0rcpt=r0t*rcp(ir1,ir2,it1,it2)
								r0rcmt=r0t*rcm(ir1,ir2,it1,it2)
								voltt=volelemt(it1,it2)
								do iphi=1,qtp
									sint = Sin(kk*r0t)
									cost = Cos(kk*r0t)
									r12angt=0.5d0*rt1*rt2*ang(it1,it2,iphi)
									r12=Sqrt(rijt-r0rcmt-r12angt)
									r14=Sqrt(rijt+r0rcpt+r12angt)
									r23=Sqrt(rijt-r0rcpt+r12angt)
									r34=Sqrt(rijt+r0rcmt-r12angt)
									pot=1.d0/r12+1.d0/r34-1.d0/r14-1.d0/r23
									vol=wtt*voltt
									shf=1.d0-Exp(-mu*r0t)*(1.d0+0.5d0*mu*r0t)
									LC=2.d0*pot-mu*(r0t*mu**2.d0+2.d0*kk*(1.d0+mu*r0t)*Tan(kk*r0t))/(2.d0-2.d0*Exp(mu*r0t)+mu*r0t)
									
									if(philsi) then
										!Direct (phiLS)
										f=pot*lin*sint*vol*expqv
										phils1(ie,i)=phils1(ie,i)+f
									end if
									
									if(philci) then
										!Direct (phiLC)
										f=lin*cost*vol*shf*LC*expqv
										philc1(ie,i)=philc1(ie,i)+f
									end if
									
									R1p = r14
									R2p = r23
									R0p=0.5d0*Sqrt(rt1**2.d0+rt2**2.d0-4.d0*r12angt)
									linp=(R0p**(real(j0(ie),8)))*(R1p**(real(j1(ie),8)))*(R2p**(real(j2(ie),8)))
									expp=Exp(-alp0*R0p-alp1*R1p-alp2*R2p)
									
									if(philsi) then
										!Exchange (phiLS)
										f=pot*volrt*linp*sint*vol*expp*exp2
										phils2(ie,i)=phils2(ie,i)+f
									end if
									
									if(philci) then
										!Exchange (phiLC)
										f=volrt*linp*cost*vol*shf*LC*exp2*expp
										philc2(ie,i)=philc2(ie,i)+f
									end if
								end do
							end do!iphi
						end do!it2
					end do!it1
				end do!ir2
			end do!ir1
		end do
		print *, 'Finished ',kk
	end do!ir0
	!$OMP END DO
	!$OMP END PARALLEL

	kk = 0.d0
	write(11,*) mega,nt
	!$OMP SINGLE
	do i=1,12
		do ie=1,nt
			kk = i*0.05d0
			fac=1.d0/(Sqrt(2.d0*kk)*((1+lam)**3.d0))
			
			!Singlet is (+) between 1 and 2 integrals
			!Triplet is (-) between 1 and 2 integrals
			!Change as needed
			phils1(ie,i) = phils1(ie,i)*fac
			phils2(ie,i) = phils2(ie,i)*fac
			phils(ie,i) = 2.d0*(phils1(ie,i)+phils2(ie,i))
			philc1(ie,i) = philc1(ie,i)*fac*0.5d0
			philc2(ie,i) = philc2(ie,i)*fac*0.5d0
			philc(ie,i) = 2.d0*(philc1(ie,i)+philc2(ie,i))
			print *, 'Kappa Value',real(kk,4)
			print *, "phils:",phils1(ie,i),phils2(ie,i)
			print *, "philc:",philc1(ie,i),philc2(ie,i)
			print *, "****************************************************"
		end do
	end do!i
	!$OMP END SINGLE

	kk=0.d0
	do i=1,12
		kk=i*0.05d0
		do ie=1,nt
			write(11,*) real(kk,4),ie,phils(ie,i),philc(ie,i)
		end do
	end do!i

	call cpu_time(finish)
	diff = int((finish-start))
	seconds = int(mod(diff,60))
	minutes = int((diff-seconds)/60.0)
	time = finish - start
	write(12,*) 'Time:',time,'Seconds'
	write(12,*) 'Total Time:',minutes,'Minutes and',seconds,'Seconds'
	print *, "Time:",time,"Seconds"

End Program PsSINGLETnew

Subroutine const(a1,a2,a3,mega,nt,nfi) ! Obtained from Peter Van Reeth
	implicit none
	integer nfi
	integer a1(nfi),a2(nfi),a3(nfi)!exponential constants!
	integer nt,mega,inx,nt2
	integer megap,i,i12,i13,i14,i23,i24,i34
	integer i12p1,i13p1,i14p1,i23p1,i24p1,i34p1
	integer i14p1m,i13p1m,i24p1m,i23p1m,j
	logical track

	megap = mega + 1
	inx = 0

	do i = 1,megap
		do i12p1 = 1,i,1
			i12 = i12p1 - 1
			i13p1m = i - i12
			do i13p1 = 1,i13p1m
				i13 = i13p1 - 1
				i14 = i13p1m - i13p1
							
				inx = inx + 1
				a1(inx) = i12
				a2(inx) = i13
				a3(inx) = i14
			end do
		end do
		write(5,*) i,inx
	end do
	nt = inx
	write(5,*) ' N    R0    R1    R2'
	write(5,"(4I3)") (j,a1(j),a2(j),a3(j),j=1,nt)
End Subroutine

Subroutine gaulag(x,w,n,nfi)
	implicit none
	integer nfi,n
	real*8 x(nfi),w(nfi)
	real*8 anu(n)
	real*8, parameter :: eps=3.0d-13
	real*8, parameter :: pi=4.d0*atan(1.d0)
	integer j,its,i
	integer, parameter :: maxit=10
	real*8, parameter :: c1=9.084064d-1,c2=5.214976d-2
	real*8, parameter :: c3=2.579930d-3,c4=3.986126d-3
	real*8 rhs(n),r2(n),r3(n),theta(n),p1(n),p2(n),p3(n)
	real*8 pp(n),z(n),z1(n)
	logical unfinished(n)
	x=0.d0
	w=0.d0
	anu = 4.d0*n+2.d0
	rhs(1) = (4.d0*n-1.d0)*pi/anu(1)
	do i=2,n
		rhs(i)=(4.d0*n-4.d0*i+3.d0)*pi/anu(i)
	end do
	r3 = rhs**(1.d0/3.d0)
	r2 = r3**2.d0
	theta = r3*(c1+r2*(c2+r2*(c3+r2*c4)))
	z = anu*(Cos(theta)**2.d0)
	unfinished = .true.
	do its=1,maxit
		where (unfinished)
			p1=1.d0
			p2=0.d0
		end where
		do j=1,n
			where (unfinished)
				p3=p2
				p2=p1
				p1=((2.d0*real(j,8)-1.d0-z)*p2-(real(j,8)-1.d0)*p3)/real(j,8)
			end where
		end do
		where (unfinished)
			pp=(n*p1-n*p2)/z
			z1=z
			z=z1-p1/pp
			unfinished = (abs(z-z1) > eps*z)
		end where
		if (.not. any(unfinished)) exit
	end do
	if (its == maxit+1) print *, 'too many iterations in gaulag'
	x=z
	w=-1.d0/(pp*real(n,8)*p2)
End Subroutine