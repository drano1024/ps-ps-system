# Ps-Ps System
This repository contains the codes used to calculate the s-wave ground state energy of the Ps-Ps system (DiPositronium). There are 3 calculations done in this repository: integrals, energies, and average values.

To run the code simply type:

```bash
python run_calculations.py --input input_file.yaml
```

## Integrals
These integrals are calculated using the Drake & Yan procedure described in (https://doi.org/10.1103/PhysRevA.52.3681)

Below is an example input yaml file for this type of calculation:

```yaml
integrals:
    compiler: "ifort or gfortran"
    omega: "list or single integer value. This controls the number of terms in the wavefunction."
```

Quadrature codes are available as well, but are not included in the run_calculations.py script since they require a supercomputer due to the amount of time required for the caluclations to finish.

## energies
These energies are calculated from integral files created from running an integrals job. These energies are calculated using the Rayleigh-Ritz variational method (https://en.wikipedia.org/wiki/Rayleigh%E2%80%93Ritz_method). These calculations also give the wave function coefficients that can be used for different calculations.

Below is an example input yaml file:

```yaml
energy:
    energy_type: "Todd or Lowden"
    spin_state: "singlet or triplet"
    integrals_dir: "path to the integral files from the integrals calculation."
    compiler: "ifort or gfortran. Only used for Todd method."
    omega: "list or single integer value."
    exchanges: "Direct, P12, P34, or P12P34."
```

## Average Values
The average values (expectation values) are calculated for the 6 interparticle distances in the system. More basic information on average values can be found here (https://en.wikipedia.org/wiki/Expectation_value_(quantum_mechanics)).

Below is an example input yaml file:

```yaml
averages:
    ci_dir: "path to the files containing the wave function coefficients obtained from the energies calculation."
    compiler: "ifort or gfortran"
    omega: "list or single integer value."
    exchanges: "Direct, P12, P34, or P12P34"
```