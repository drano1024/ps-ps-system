      Program NumberofTerms
      	parameter(nfi=1000000)
        real*8 alpk(5),betk(5),gamk(5)
        real*8 alpkd(5),betkd(5),gamkd(5)
        integer i,j,nt,mega,nt2,nphi,nqs,nq
        integer ii(nfi),ij(nfi),ik(nfi),il(nfi),im(nfi),in2(nfi)

        open(unit=2,file='Number',status='unknown')
		open(unit=4,file='Powers',status='unknown')
		
		mega = 15
		
        call const(ii,ij,ik,il,im,in2,mega,nt,nt2,nfi)
		
      End Program NumberofTerms

      Subroutine const(ii,ij,ik,il,im,in2,mega,nt,nt2,nfi) ! Obtained from Peter Van Reeth
      	integer ii(nfi),ij(nfi),ik(nfi),il(nfi),im(nfi),in2(nfi)
      	integer nt,mega,inx
        integer megap,i,i12,i13,i14,i23,i24,i34
        integer i12p1,i13p1,i14p1,i23p1,i24p1,i34p1
        integer i14p1m,i13p1m,i24p1m,i23p1m

        megap = mega + 1
        inx = 0

        do i = 1,megap
          do i12p1 = 1,i,1
            i12 = i12p1 - 1
            i14p1m = i - i12
            do i14p1 = 1,i14p1m
              i14 = i14p1 - 1
              i13p1m = i14p1m - i14
              do i13p1 = 1,i13p1m
                i13 = i13p1 - 1
                i24p1m = i13p1m - i13
                do i24p1 = 1,i24p1m
                  i24 = i24p1 - 1
                  i23p1m = i24p1m - i24
                  do i23p1 = 1,i23p1m
                  	i23 = i23p1 - 1
                    i34 = i23p1m - i23p1

                    inx = inx + 1
                    ii(inx) = i12
                    ij(inx) = i13
                    ik(inx) = i14
                    il(inx) = i23
                    im(inx) = i24
                    in2(inx) = i34

                  end do
                end do
              end do
            end do
          end do
          write(4,*) (i-1),inx
		  write(2,*) (i-1),inx
        end do
        
        nt = inx
        nt2 = inx
        write(4,*) ' N    r12    r13    r14    r23    r24    r34'
        write(4,"(7I7)")(j,ii(j),ij(j),ik(j),il(j),im(j),in2(j),j=1,nt2)
      End Subroutine
