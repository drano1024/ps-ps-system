import numpy as np

nt_lib = {
    0:1,
    1:7,
    2:28,
    3:84,
    4:210,
    5:462
}

def load_file(file):
    skip_line = ['quad', 'Beginning', 'Start', 'Calculating']
    integrals = []
    with open(file, 'r') as f:
        while True:
            try:
                fline = f.readline().strip().split()
                if fline[0] == 'Omega:':
                    Omega = int(fline[1])
                    nt = nt_lib[Omega]
                elif fline[0] in skip_line:
                    continue
                elif fline[1] == 'Finished':
                    continue
                elif not fline[0] == []:
                    i = int(fline[0]) -1
                    j = int(fline[1]) -1
                    pp = float(fline[2])
                    php = float(fline[3])
                    integrals.append([i,j,pp,php])
                else:
                    break
            except IndexError:
                break
        return Omega, nt , integrals

def remove_doubles(n, integrals):
    pp = np.zeros([n,n])
    php = np.zeros([n,n])
    for i in range(0, len(integrals), 2):
        pp1 = integrals[i][2]
        pp2 = integrals[i+1][2]
        php1 = integrals[i][3]
        php2 = integrals[i+1][3]
        pp[integrals[i][0], integrals[i][1]] = max(pp1,pp2)
        php[integrals[i][0], integrals[i][1]] = min(php1,php2)
    return pp, php

def clean_ints(n, integrals):
    pp = np.zeros([n,n])
    php = np.zeros([n,n])
    for i in range(0, len(integrals)):
        pp1 = integrals[i][2]
        php1 = integrals[i][3]
        pp[integrals[i][0], integrals[i][1]] = pp1
        php[integrals[i][0], integrals[i][1]] = php1
    return pp, php

if __name__ == "__main__":
    import sys
    w, n, ints = load_file(sys.argv[1])
    try:
        single = sys.argv[2]
    except IndexError:
        single = False
    sorted_ints = sorted(sorted(ints, key=lambda l:l[1]), key=lambda l:l[0])
    if not single:
        pp, php = remove_doubles(n, sorted_ints)
    else:
        pp, php = clean_ints(n, sorted_ints)
    print(w)
    print(n)
    for i in range(n):
        for j in range(n):
            print(i+1,j+1,pp[i,j],php[i,j])
