import numpy as np
import sys

nt_lib = {
    0:1,
    1:7,
    2:28,
    3:84,
    4:210,
    5:462
}

def load_file(file):
    skip_line = ['quad', 'Beginning', 'Start', 'Calculating']
    integrals = []
    with open(file, 'r') as f:
        while True:
            try:
                fline = f.readline().strip().split()
                if fline[0] == 'Omega:':
                    Omega = int(fline[1])
                    nt = nt_lib[Omega]
                elif fline[0] in skip_line:
                    continue
                elif fline[1] == 'Finished':
                    continue
                elif not fline[0] == []:
                    i = int(fline[0]) -1
                    j = int(fline[1]) -1
                    pp = float(fline[2])
                    php = float(fline[3])
                    integrals.append([i,j,pp,php])
                else:
                    break
            except IndexError:
                break
        return Omega, nt , integrals

if __name__ == "__main__":
    ints = []
    files = [ 'old_ints_'+x+'_run_1234' for x in ['first', 'second','third','fourth']]
    nprocs = 168
    for i, fil in enumerate( files ):
        if i > 0:
            count = 0
            with open( fil, 'r' ) as f:
                for line in f:
                    l = line.strip().split()
                    if l[-1] == '0.0':
                        continue
                    else:
                        count += 1
            ints.append( count )
            #print( count, ' integrals completed in 21 days for ', fil)
    total_ints = 210**2-84**2
    #print( total_ints, ' integrals to complete.')
    diff_l = []
    for i in range( len( ints ) ):
        if i > 0:
            diff_l.append( ints[i] - ints[i-1] )
            #print( ints[i] - ints[i-1], 'integrals calculated in run', i )
    diff = sum( diff_l )/len( diff_l )
    print( np.floor(diff/nprocs), 'integrals completed per processor')
    cycles = int( np.ceil( total_ints/diff ) )
    #print( cycles, 'cycles to complete' )
    time = cycles*21/30
    print( time, 'months to finish all calculations on Talon' )
    node_hours = 21*24*nprocs #time*nodes
    print( node_hours, 'node hours per cycle for ', nprocs,' processors on Talon')
    tac_speedup = 4
    new_diff = tac_speedup*diff
    print( 'TACC can calculate ', new_diff, ' integrals in 21 days with ', nprocs, ' processors')
    print( 'TACC would take', int( np.ceil( total_ints/new_diff ) ), 'months to finish')
    tacc_nodes = 6
    tacc_diff = new_diff/nprocs*128*tacc_nodes
    print( total_ints/tacc_diff, 'cycles to complete on TACC with', tacc_nodes, 'nodes' )
    print( total_ints/tacc_diff*21, 'days to finish' )
    print( total_ints/tacc_diff*21*24*tacc_nodes*128, 'node-hours on TACC' )