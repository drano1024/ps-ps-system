import numpy as np

def load_file(file):
    with open(file, 'r') as f:
        while True:
            try:
                fline = f.readline().strip().split()
                Omega = int(fline[0])
                fline = f.readline().strip().split()
                nt = int(fline[0])
                ppintegrals = np.zeros((nt,nt))
                phpintegrals = np.zeros((nt,nt))
                for n in range(nt**2):
                    fline = f.readline().strip().split()
                    i = int(fline[0]) - 1
                    j = int(fline[1]) - 1
                    pp = float(fline[2])
                    php = float(fline[3])
                    ppintegrals[i][j] = pp
                    phpintegrals[i][j] = php
            except IndexError:
                break
        return Omega, nt , ppintegrals, phpintegrals

def merge(ints1,ints2):
    length = max(len(ints1),len(ints2))
    intmerged = np.zeros((length,length))
    for i in range(length):
        for j in range(length):
            try:
                temp1 = ints1[i][j]
                temp2 = ints2[i][j]
                if temp1 == 0.0:
                    intmerged[i][j] = temp2
                elif temp2 == 0.0:
                    intmerged[i][j] = temp1
                elif temp1 == temp2:
                    intmerged[i][j] = 0.5*(temp1+temp2)
                else:
                    intmerged[i][j] = temp2
                #print(i,j,intmerged[i][j])
            except IndexError:
                intmerged[i][j] = temp1
                #print(i,j,intmerged[i][j])
                continue
    return intmerged

def print_mat(mat):
    for i in range(len(mat)):
        for j in range(len(mat)):
            print(i,j,mat[i][j])

def print_final(mat1,mat2,w,n):
    print(w)
    print(n)
    for i in range(len(mat1)):
        for j in range(len(mat2)):
            print(i+1,j+1,mat1[i][j],mat2[i][j])

def remove_fac(old_ints,ints,fac):
    new_ints = np.copy(ints)
    for i in range(len(old_ints)):
        for j in range(len(old_ints)):
            if old_ints[i][j] == 0:
                continue
            else:
                new_ints[i][j] = old_ints[i][j]
    return new_ints

if __name__ == "__main__":
    import sys
    #Must put larger file first
    w1, n1, ppints1, phpints1 = load_file(sys.argv[1])
    w2, n2, ppints2, phpints2 = load_file(sys.argv[2])
    alp = 0.385
    bet = 0.48
    gam = 0.37
    lam = 1.0
    if sys.argv[3] == 'merge':
        ppints = merge(ppints1,ppints2)
        phpints = merge(phpints1,phpints2)
        print_final(ppints,phpints,w1,n1)
    elif sys.argv[3] =='P34':
        fac = 2*np.pi/((alp+lam)*(bet+lam)*(1+lam))
        pp_new = remove_fac(ppints2,ppints1,fac)
        php_new = remove_fac(phpints2,phpints1,fac)
        print_final(pp_new,php_new,w1,n1)
    elif sys.argv[3] == 'P12P34':
        fac = 2*np.pi/((alp+lam)*(alp+lam)*(1+lam))
        pp_new = remove_fac(ppints2,ppints1,fac)
        php_new = remove_fac(phpints2,phpints1,fac)
        print_final(pp_new,php_new,w1,n1)