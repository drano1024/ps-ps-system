      Program AvgValues
      	parameter(nfi=2000)
        real*8, dimension(-2:90,-2:90,-2:90,6):: resus
        real*8, dimension(-2:90,-2:90,-2:90,6):: resusd
        real*8, dimension(-2:90,-2:90,-2:90,6):: resusdd
        real*8 rcterm(130,130,130),denom
        real*8 phi1,phid1,avg(36),ci(nfi)
        real*8 alpk(5),betk(5),gamk(5)
        real*8 alpkd(5),betkd(5),gamkd(5)
        integer jp(6,6), ints(nfi), tmp
        integer i,j,nt,mega,nt2,nphi
        integer ii(nfi),ij(nfi),ik(nfi),il(nfi),im(nfi),in2(nfi)
        logical P12
        pi = 4.0*ATAN(1.0)
        avg2 = 0.0
        avg = 0.0
		    denom = 0.0
        P12=.true.
        ints = 0
        ci = 0.d0
      	!Using Ho's Paper Phys Rev A 55 (1986)
        !and Frolov and Smith Phys Rev A 55 (1997)

        open(unit=1,file='fort.95',status='old')
        open(unit=4,file='const',status='unknown') !a_ij values for phi_i
        open(unit=9,file='outone',status='unknown') !General information output
		    !constantsnewDirect is for direct calculations only
		    !constantsnewexchange is for direct and electron exchange
        open(unit=8,file='ci.P12.0',status='old')
        open(unit=15,file='average.P12.0',status='unknown')
        open(unit=16,file='jpmatrixavg',status='unknown')
        open(unit=17,file='ToddssP12,0',status='old')

        read(1,*) nphi !number of short range terms with different exp

        do i=1,nphi
          read(1,*) alpk(i)
          read(1,*) betk(i)
          read(1,*) gamk(i)

          !P12
          if ( P12 ) then
            alpkd(i) = betk(i)
            betkd(i) = alpk(i)
            gamkd(i) = gamk(i)
          else
            alpkd(i) = 0.0
            betkd(i) = 0.0
            gamkd(i) = 0.0
          end if

          write(9,*) 'alpha',i,alpk(i)
          write(9,*) 'beta',i,betk(i)
          write(9,*) 'gamma',i,gamk(i)
        end do

        !max q value in eq. 5 Drake&Yan
        read(1,*) nqs
        nq = nqs

        read(1,*) mega
        call const(ii,ij,ik,il,im,in2,mega,nt,nt2,nfi)
        write(9,*) 'N =',nt

        read(8,*) numb
        do i=1,numb
          ! Read the integral number
          ! Read the ci value and assign it to the integral number
          read(17,*) tmp
          if ( .NOT. tmp .eq. 0 ) then
            ints(i) = tmp
            read(8,*) ci(ints(i))
          end if
        end do

        do kc=1,nphi
          do lc=kc,nphi
            a1 = alpk(kc)+alpk(lc)
            a2 = betk(kc)+betk(lc)
            a3 = gamk(kc)+gamk(lc)

            !Here are the exchange exponential fall offs
            !P12
            if ( P12 ) then
              a1d = alpk(kc)+alpkd(lc)
              a2d = betk(kc)+betkd(lc)
              a3d = gamk(kc)+gamkd(lc)

              !Here are the exchange exponential fall offs
              !P12
              a1dd = alpkd(kc)+alpkd(lc)
              a2dd = betkd(kc)+betkd(lc)
              a3dd = gamkd(kc)+gamkd(lc)
            end if

            !Here we calculate the W function from Drake&Yan for all
            !combinations. To be called later.

            lmax = 2*mega+2*nqs+10
            mmax = 2*mega+10
            nmax = 2*mega+2*nqs+10

            do lh=0,lmax
              do mh=0,mmax
                do nh=0,nmax

                  lht=lh
                  mht=mh
                  nht=nh-2*nq-1
                  
                  if((lht+mht+nht+2).gt.0)then
                    call wfunction(lht,mht,nht,a1,a2,a3,res1)
                    resus(lh,mh,nh,1) = res1
                    call wfunction(lht,mht,nht,a1,a3,a2,res2)
                    resus(lh,mh,nh,2) = res2
                    call wfunction(lht,mht,nht,a2,a1,a3,res3)
                    resus(lh,mh,nh,3) = res3
                    call wfunction(lht,mht,nht,a2,a3,a1,res4)
                    resus(lh,mh,nh,4) = res4
                    call wfunction(lht,mht,nht,a3,a1,a2,res5)
                    resus(lh,mh,nh,5) = res5
                    call wfunction(lht,mht,nht,a3,a2,a1,res6)
                    resus(lh,mh,nh,6) = res6

                    !P12 exchange terms
                    if ( P12 ) then
                      call wfunction(lht,mht,nht,a1d,a2d,a3d,res1d)
                      resusd(lh,mh,nh,1) = res1d
                      call wfunction(lht,mht,nht,a1d,a3d,a2d,res2d)
                      resusd(lh,mh,nh,2) = res2d
                      call wfunction(lht,mht,nht,a2d,a1d,a3d,res3d)
                      resusd(lh,mh,nh,3) = res3d
                      call wfunction(lht,mht,nht,a2d,a3d,a1d,res4d)
                      resusd(lh,mh,nh,4) = res4d
                      call wfunction(lht,mht,nht,a3d,a1d,a2d,res5d)
                      resusd(lh,mh,nh,5) = res5d
                      call wfunction(lht,mht,nht,a3d,a2d,a1d,res6d)
                      resusd(lh,mh,nh,6) = res6d

                      !P12 exchange terms
                      call wfunction(lht,mht,nht,a1dd,a2dd,a3dd,res1dd)
                      resusdd(lh,mh,nh,1) = res1dd
                      call wfunction(lht,mht,nht,a1dd,a3dd,a2dd,res2dd)
                      resusdd(lh,mh,nh,2) = res2dd
                      call wfunction(lht,mht,nht,a2dd,a1dd,a3dd,res3dd)
                      resusdd(lh,mh,nh,3) = res3dd
                      call wfunction(lht,mht,nht,a2dd,a3dd,a1dd,res4dd)
                      resusdd(lh,mh,nh,4) = res4dd
                      call wfunction(lht,mht,nht,a3dd,a1dd,a2dd,res5dd)
                      resusdd(lh,mh,nh,5) = res5dd
                      call wfunction(lht,mht,nht,a3dd,a2dd,a1dd,res6dd)
                      resusdd(lh,mh,nh,6) = res6dd
                    end if
                    
                  end if
                end do
              end do
            end do

            !Creating the Cjqk terms from Drake & Yan, equation 4
            jx = 2*mega+12
            call ccoeff(jx,rcterm)

            !Create the matrix for the extra powers in the linear terms
            !that come from the Hamiltonian acting on the wavefunction

            call jpmatrix(jp)

            !Here begins the integration loops
            pihalf=0.5/pi

            do ie=1,nt
              if ( .NOT. ANY( ints == ie ) ) cycle
              do je=1,nt
                if ( .NOT. ANY( ints == je ) ) cycle

     			      j12e = ii(ie)+ii(je)
                j13e = ij(ie)+ij(je)
                j14e = ik(ie)+ik(je)
                j23e = il(ie)+il(je)
                j24e = im(ie)+im(je)
                j34e = in2(ie)+in2(je)
                con = ci(ie)*ci(je)
				
				        call integration(nq,j13e,j23e,j34e,j12e,j14e,j24e,
     &	rcterm,resus,phires)
	 
				        if( P12 ) then
                  !First Exchange
				          j12d = ii(ie)+ii(je)
                  j13d = ij(ie)+il(je)
                  j14d = ik(ie)+im(je)
                  j23d = il(ie)+ij(je)
                  j24d = im(ie)+ik(je)
                  j34d = in2(ie)+in2(je)
                  nq = nqs

                  call integration(nq,j13d,j23d,j34d,j12d,j14d,j24d,
     &  rcterm,resusd,phiresd)

     			        !Second Exchange
                  j12dd = ii(ie)+ii(je)
                  j13dd = il(ie)+ij(je)
                  j14dd = im(ie)+ik(je)
                  j23dd = ij(ie)+il(je)
                  j24dd = ik(ie)+im(je)
                  j34dd = in2(ie)+in2(je)
                  nq = nqs

                  call integration(nq,j13dd,j23dd,j34dd,j12dd,j14dd,
     &  j24dd,rcterm,resusd,phiresdd)

     			        !Third Exchange
                  j12ddd = ii(ie)+ii(je)
                  j13ddd = il(ie)+il(je)
                  j14ddd = im(ie)+im(je)
                  j23ddd = ij(ie)+ij(je)
                  j24ddd = ik(ie)+ik(je)
                  j34ddd = in2(ie)+in2(je)
                  nq = nqs

                  call integration(nq,j13ddd,j23ddd,j34ddd,j12ddd,
     &  j14ddd,j24ddd,rcterm,resusdd,phiresddd)

                  phi1 = phires*pihalf
                  phid1 = phiresd*pihalf
                  phid2 = phiresdd*pihalf
                  phid3 = phiresddd*pihalf
                  denom = denom + (phi1+phid1+phid2+phid3)*con
                else
                  phi1 = phires*pihalf
                  denom = denom + phi1*con !Used for Direct Calcuation only
                end if

                do lk=1,6 !6 is the number of different average values to be calculated
                  
                  j12 = j12e+jp(lk,1)
                  j13 = j13e+jp(lk,2)
                  j14 = j14e+jp(lk,3)
                  j23 = j23e+jp(lk,4)
                  j24 = j24e+jp(lk,5)
                  j34 = j34e+jp(lk,6)
                  nq = nqs

                  !Calculate the general result for the integration of a
                  !given term combining the Wfunction and C terms
                  call integration(nq,j13,j23,j34,j12,j14,j24,
     &	rcterm,resus,phires)
	 
     			        if( P12 ) then
				            !First Exchange
                    j12d = ii(ie)+ii(je)+jp(lk,1)
                    j13d = ij(ie)+il(je)+jp(lk,2)
                    j14d = ik(ie)+im(je)+jp(lk,3)
                    j23d = il(ie)+ij(je)+jp(lk,4)
                    j24d = im(ie)+ik(je)+jp(lk,5)
                    j34d = in2(ie)+in2(je)+jp(lk,6)
                    nq = nqs

                    call integration(nq,j13d,j23d,j34d,j12d,j14d,j24d,
     &  rcterm,resusd,phiresd)

     			          !Second Exchange
                    j12dd = ii(ie)+ii(je)+jp(lk,1)
                    j13dd = il(ie)+ij(je)+jp(lk,2)
                    j14dd = im(ie)+ik(je)+jp(lk,3)
                    j23dd = ij(ie)+il(je)+jp(lk,4)
                    j24dd = ik(ie)+im(je)+jp(lk,5)
                    j34dd = in2(ie)+in2(je)+jp(lk,6)
                    nq = nqs

                    call integration(nq,j13dd,j23dd,j34dd,j12dd,j14dd,
     &  j24dd,rcterm,resusd,phiresdd)

     			          !Third Exchange
                    j12ddd = ii(ie)+ii(je)+jp(lk,1)
                    j13ddd = il(ie)+il(je)+jp(lk,2)
                    j14ddd = im(ie)+im(je)+jp(lk,3)
                    j23ddd = ij(ie)+ij(je)+jp(lk,4)
                    j24ddd = ik(ie)+ik(je)+jp(lk,5)
                    j34ddd = in2(ie)+in2(je)+jp(lk,6)
                    nq = nqs

                    call integration(nq,j13ddd,j23ddd,j34ddd,j12ddd,
     &  j14ddd,j24ddd,rcterm,resusdd,phiresddd)

                    phi1 = phires*pihalf
                    phid1 = phiresd*pihalf
                    phid2 = phiresdd*pihalf
                    phid3 = phiresddd*pihalf
                    avg(lk) = avg(lk)+(phi1+phid1+phid2+phid3)*con
                  else
                    phi1 = phires*pihalf
				            avg(lk) = avg(lk) + phi1*con
				          end if
                  
                end do
              end do
            end do
          end do
        end do
		
		do i=1,36
			avg(i) = avg(i)/denom
		end do
		
		write(9,*) denom
		
		!Expectation Values obtained from Frolov Phys. Rev. A volume 55, number 4 pg. 2662 April 1997
        write(15,*) 'Distance Calculated   ','Value             '
     & ,'        Percent Difference'
        p12v = pdiff(avg(1),6.032613)
        p13 = pdiff(avg(2),4.486844)
        p23 = pdiff(avg(4),4.486844)
        p34 = pdiff(avg(6),6.032613)
        write(15,*) 'r12                ',avg(1),p12v
        write(15,*) 'r13                ',avg(2),p13
        write(15,*) 'r23                ',avg(4),p23
        write(15,*) 'r14                ',avg(3)
        write(15,*) 'r24                ',avg(5)
        write(15,*) 'r34                ',avg(6),p34

      End Program AvgValues

      Function pdiff(x,y)
      	real*8 x,y,pdiff

        top = ABS(x-y)
        bot = x+y
        botinv = 1.0/bot
        pdiff = top*botinv*200.0
      End Function

      !
      ! This subroutine creates the constants a1,a2,a3,a4,a5, and a6
      ! for the exponential terms. Used const routine from "Codes for Gabriels Values"
      !

      Subroutine const(ii,ij,ik,il,im,in2,mega,nt,nt2,nfi) ! Obtained from Peter Van Reeth
      	integer ii(nfi),ij(nfi),ik(nfi),il(nfi),im(nfi),in2(nfi)
      	integer nt,mega,inx
        integer megap,i,i12,i13,i14,i23,i24,i34
        integer i12p1,i13p1,i14p1,i23p1,i24p1,i34p1
        integer i14p1m,i13p1m,i24p1m,i23p1m

        megap = mega + 1
        inx = 0

        do i = 1,megap
          do i12p1 = 1,i,1
            i12 = i12p1 - 1
            i14p1m = i - i12
            do i14p1 = 1,i14p1m
              i14 = i14p1 - 1
              i13p1m = i14p1m - i14
              do i13p1 = 1,i13p1m
                i13 = i13p1 - 1
                i24p1m = i13p1m - i13
                do i24p1 = 1,i24p1m
                  i24 = i24p1 - 1
                  i23p1m = i24p1m - i24
                  do i23p1 = 1,i23p1m
                  	i23 = i23p1 - 1
                    i34 = i23p1m - i23p1
                    
                    inx = inx + 1
                    ii(inx) = i12
                    ij(inx) = i13
                    ik(inx) = i14
                    il(inx) = i23
                    im(inx) = i24
                    in2(inx) = i34

                  end do
                end do
              end do
            end do
          end do
          write(4,*) (i-1),inx
        end do
        nt = inx
        nt2 = inx
        write(4,*) ' N    r12    r13    r14    r23    r24    r34'
        write(4,"(7I7)")(j,ii(j),ij(j),ik(j),il(j),im(j),in2(j),j=1,nt2)
      End Subroutine

      Subroutine integration(nq,j1,j2,j3,j12,j13,j23,rcterm,resus,phi)
      	real*8 rcterm(130,130,130)
        real*8 ,dimension(-2:90,-2:90,-2:90,6)::resus
        real*8 pi,piterm,phi
        real*8 cterm,rqt
        integer lht,mht,nht,j1,j2,j3,j12,j13,j23
        integer nq

        phi = 0.0
        cterm = 0.0

        pi = 4.0*atan(1.0)
        piterm = (4.0*pi)**3.0

        L12 = int((j12+1)/2)
        L13 = int((j13+1)/2)
        L23 = int((j23+1)/2)

        j1j12 = j1+2+j12
        j1j13 = j1+2+j13
        j2j12 = j2+2+j12
        j2j23 = j2+2+j23
        j3j13 = j3+2+j13
        j3j23 = j3+2+j23

        do jq=0,nq
          
          j1q = j1+2+2*jq
          j2q = j2+2+2*jq
          j3q = j3+2+2*jq
          j1j12q = j1j12+j13-2*jq
          j2j12q = j2j12+j23-2*jq
          j3j13q = j3j13+j23-2*jq
          
          rqt = piterm/((2.0*jq+1.0)**2.0)

          do k12=0,L12
            do k13=0,L13
              do k23=0,L23
                nq2 = 2*nq+1
                rcoeff=rcterm(j12+2,jq+2,k12+2)*rcterm(j13+2,jq+2,k13+2)
     &  *rcterm(j23+2,jq+2,k23+2)*rqt
              
              !First W(...) function integral
              lht = j1q+2*k12+2*k13
              mht = j2j12-2*k12+2*k23
              nht = j3j13q-2*k23-2*k13+nq2
              res1 = resus(lht,mht,nht,1)

              !Second W(...) function integral
              lht = j1q+2*k12+2*k13
              mht = j3j13-2*k13+2*k23
              nht = j2j12q-2*k23-2*k12+nq2
              res2 = resus(lht,mht,nht,2)

              !Third W(...) function integral
              lht = j2q+2*k12+2*k23
              mht = j1j12-2*k12+2*k13
              nht = j3j13q-2*k23-2*k13+nq2
              res3 = resus(lht,mht,nht,3)

              !Fourth W(...) function integral
              lht = j2q+2*k12+2*k23
              mht = j3j23-2*k23+2*k13
              nht = j1j12q-2*k12-2*k13+nq2
              res4 = resus(lht,mht,nht,4)

              !Fifth W(...) function integral
              lht = j3q+2*k13+2*k23
              mht = j1j13-2*k13+2*k12
              nht = j2j12q-2*k12-2*k23+nq2
              res5 = resus(lht,mht,nht,5)

              !Sixth W(...) function integral
              lht = j3q+2*k13+2*k23
              mht = j2j23-2*k23+2*k12
              nht = j1j12q-2*k12-2*k13+nq2
              res6 = resus(lht,mht,nht,6)

              phi = phi + rcoeff*(res1+res2+res3+res4+res5+res6)
              end do
            end do
          end do
        end do
      End Subroutine

      Subroutine ccoeff(jx,rcterm)
      	real*8 rcterm(130,130,130)
        real*8 rkterm,rkterm2,rterm,binom,facnq
        integer n,nq,nk,nt,nqj,nq2,n2,jx

        do n=0,jx,2
          n2 = int(n/2)
          rnp2 = real(n+2,8)
          np2 = n+2

          do nq=0,n2
            facnq = real(2*nq+1,8)/rnp2
            nq2 = int(n2-nq)
            nqj = int(nq-1)
            do nk=0,nq2
              nk2 = 2*nk+1

              call binomcoeff(np2,nk2,binom)
              rterm=1.0

              if(nq .ne. 0) then
                do nt=0,nqj
                  rkterm = real(2*nk+2*nt-n,8)
                  rkterm2 = real(2*nk+2*nq-2*nt+1,8)
                  rterm = rterm*rkterm/rkterm2
                end do
              end if

              rcterm(n+2,nq+2,nk+2) = rterm*binom*facnq
              rcterm(1,nq+2,2)=1.0
            end do
          end do
        end do

        do n=1,25,2 !Change the 25 to improve accuracy, if needed.
          n2 = 120
          rnp2 = real(n+2,8)
          np2 = n+2
          
          do nq=0,n2
            facnq = real(2*nq+1,8)/rnp2
            nq2 = int((n+1)/2)
            nqj = int(nq-1)

            do nk=0,nq2
              if(nq2 .lt. int(nq-1))then
                nqj = nq2
              end if
              nk2 = 2*nk+1
              call binomcoeff(np2,nk2,binom)
              rterm=1.0

              if (nq .ne. 0) then
                do nt=0,nqj
                  rkterm = real(2*nk+2*nt-n,8)
                  rkterm2 = real(2*nk+2*nq-2*nt+1,8)
                  rterm = rterm*rkterm/rkterm2
                end do
              end if

              rcterm(n+2,nq+2,nk+2) = rterm*binom*facnq
              rcterm(1,nq+2,2) = 1.0
              
            end do
          end do
        end do

      End Subroutine

      Subroutine wfunction(lht,mht,nht,alpha,beta,gamma,res)
      	real*8 hyper(300,3)
        real*8 factl,expterm,exptermb,termb
        real*8 termc,res,zd
        integer lht,mht,nht,ip,ht1,ht2,ht3

        factl = exp(factln(lht))

        nn = 65
        ht1 = 1.0
        ht2 = lht+mht+nht+3+nn
        ht3 = lht+mht+3+nn
        zd = (alpha+beta)/(alpha+beta+gamma)
        call hypergeom(nn,ht1,ht2,ht3,zd,hyper)

        expterm = (alpha+beta+gamma)**(lht+mht+nht+3.0)
        exptermb = alpha/(alpha+beta+gamma)
        termb = 1.0
        res = 0.0
        do ip=0,nn-1
          htb = lht+mht+2+ip

          facta = exp(factln(lht+mht+nht+2+ip))
          factb = exp(factln(lht+1+ip))

          termb = exptermb**ip
          ip1 = lht+mht+nht+3+ip
          termc = hyper(ip1,1)*facta*termb/(factb*htb)

          res = res + factl/expterm*termc
        end do
      End Subroutine

      Subroutine hypergeom(nn,ht1,ht2,ht3,zd,hyper)
      	real*8 f1,f2,f3,ft1,ft2,rj,zd
        real*8 hyper(300,3)
        integer nn,ht1,ht2,ht3

        hyper = 0.0
        coeff = zd*ht2/ht3
        hyper11 = 1.0
        ft2 = 1.0

        do i=1,nn
          rj = real(i-1,8)
          f1 = ht1+rj
          f2 = ht2+rj
          f3 = (ht3+rj)*i

          ft1 = f1*f2*zd/f3
          hyper11 = hyper11+ft1*ft2
          ft2 = ft1*ft2
        end do

        nht2 = int(ht2)
        nht21 = int(ht2)-1
        hyper(nht2,1) = hyper11
        ss = ht3
        st = ht2
        do ik=nht21,1,-1
          ss=ss-1
          if(ss .ne. 0.0) then
            st = st-1
            hyper(ik,1) = st/ss*zd*hyper(ik+1,1)+1.0
          end if
        end do
      End Subroutine

      Function fact(n)
      	integer n
        real*8 fact

        if(n .eq. 0) then
          fact = 1.0
        else
          ns = 1
          do l=1,n
            ns=ns*l
          end do
          fact = real(ns,8)
        end if
        return
      End Function

      Subroutine binomcoeff(j,l,binom)
      	real*8 binom
        integer j,l
        external fact

        binom = exp(factln(j))/(exp(factln(l))*exp(factln(j-l)))

        return
      End Subroutine

      Function factln(n)
      	real*8 a(100)
        data a/100*-1.0/

        if(n .le. 99) then
          if(a(n+1).lt.0.0) then
            a(n+1) = gammaln(n+1.0)
          end if
          factln = a(n+1)
        else
          factln = gammaln(n+1.0)
        end if
        return
      End Function

      Function gammaln(xx)
      	real*8 cof(6),stp,half,one,fpf,x,tmp,ser

        data cof,stp/76.18009173E0,-86.50532033E0,24.01409822E0,
     &	-1.231739516E0,0.120858003E-02,-0.536382E-05,2.50662827465E0/
     	
        data half,one,fpf/0.5E0,1.0E0,5.5E0/

        x = xx-one
        tmp = x+fpf
        tmp = (x+half)*log(tmp)-tmp
        ser=one

        do j=1,6
          x = x+one
          ser=ser+cof(j)/x
        end do
        gammaln = tmp+log(stp*ser)
      End Function

      Subroutine jpmatrix(jp)
      	integer jp(6,6),jp2(6,6)
        integer ipp,jpp

        jp = 0
        jp2 = 0

        do ipp=1,6
          jp2(ipp,ipp) = 1
          !jp2(ipp+6,ipp) = -1
          !jp2(ipp+12,ipp) = -2
          !jp2(ipp+18,ipp) = 2
          !jp2(ipp+24,ipp) = 3
          !jp2(ipp+30,ipp) = 4
        end do

        do ipp = 1,6
          do jpp = 1,6
            jp(ipp,jpp) = jp2(ipp,jpp)
          end do
        end do

        do ipp=1,6
          write(16,*) ipp,jp(ipp,1),jp(ipp,2),jp(ipp,3)
     & ,jp(ipp,4),jp(ipp,5),jp(ipp,6)
        end do

      End Subroutine