#!/bin/bash

write_fort() {
    # Writes the fort.95 input file used for DrakePsPsBound.f
    # $1 is the omega value
    # $2 is used to include the electron exchange
    echo $1
    echo "1" > fort.95
    echo "0.385" >> fort.95
    echo "0.48" >> fort.95
    echo "0.37" >> fort.95
    echo "15" >> fort.95
    echo $1 >> fort.95
}

change_avgvals () {
    # Changed the input/output files used in AvgValues.f
    # with the correct omega value and exchange
    if [[ "$2" -eq 0 ]]
    then
        exc='Direct'
        sed -i "/P12=/c\        P12=.false." AvgValues.f
    else
        exc='P12'
        sed -i "/P12=/c\        P12=.true." AvgValues.f
    fi
    sed -i "/unit=8/c\        open(unit=8,file='ci.$exc,$1',status='old')" AvgValues.f
    sed -i "/unit=15/c\        open(unit=15,file='average.$exc,$1',status='unknown')" AvgValues.f
    sed -i "/unit=17/c\        open(unit=17,file='Toddss$exc,$1',status='old')" AvgValues.f
}

for omega in 0 1
do
    write_fort $omega
    for e in 0
    do
        change_avgvals $omega $e
        make
        ./avgval
        make clean
    done
done
