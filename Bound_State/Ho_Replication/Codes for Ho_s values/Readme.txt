The files of interest are:
	1. DrakePsPsBound.f
	2. fort.95
	3. omega2PsPs
	4. RRmethod.f95
	5. RRconvergencePsPsDrake
	6. run.sh

fort.95 is the input file for DrakePsPsBound.f . omega2PsPs
is one of the output files from this program and contains all
of the calculated integrals. This is also the input file for 
RRmethod.f95. The eigenvalues are given in the output file
RRconvergencePsPsDrake. The run.sh file must be made into an
executable file by running the following command "chmod u+x run.sh".
Then you run it the same as any other executable file "./run.sh".
The run.sh file runs the make files for both codes and runs both codes.
First it runs DrakePsPsBound.f then it runs RRmethod.f95.
