!	Rayeighly Ritz method to calculate energy eigenvalues of Ps_H system
!	with input from fort.48 created by Square Hamiltonian program(BoundstateCalc.f).

Program RRmethod
	implicit double precision(a-h, o-z)

    parameter(nn=1000, int=100, ep=-0.0001d0, lwork=64*nn)
    dimension h(nn,nn), p(nn,nn)
    dimension h2(nn,nn), p2(nn,nn)
    dimension eigv(nn), vec(nn,nn), dl(nn), e(nn)
    dimension work(lwork), numb(15)
    character U, uplo, jobz
    character(len=37) fmt24
    character(len=16) fmt25
    integer ifl, ITYPE, info, ni
    external dsygv

    fmt24 = "(1I9,I7,3f25.13,3f25.13,3f25.13,I7)"
    fmt25 = "(A8,A11,3A11,A8)"

    jobz='V'
    info=0
    ITYPE=1
    uplo='L'

    open(unit=48,file='MyIntegrals,7',status='old')
    open(unit=6,file='energy,7',status='unknown')
    open(unit=9,file='mycivals,7',status='unknown')
    open(unit=7,file='check',status='unknown')
    open(unit=8,file='constants2',status='unknown')

    read(48,*) mega
    write(7,*) mega

    do inp=1,mega+1!Changing to 3 to allow for Ho's omega values
      read(48,*) ihp, numb(inp)
    end do

    do inp=1,mega+1!Changing to 3 to allow for Ho's omega values
      write(7,*) ihp, numb(inp)
    end do

    read(48,*) nt
    write(7,*) nt
    
    do i=1,nt
      do j=1,i
        read(48,*) i2,j2,p2(i,j),h2(i,j)
        h(i,j) = h2(i,j)!*2.0!The factor of 2 is for Ry units. Remove to change to a.u.
        h(j,i) = h(i,j)
        p(i,j) = p2(i,j)
        p(j,i) = p(i,j)
      end do
    end do

    do i=1,nn
      eigv(i) = 0.0
    end do

    do nicount=1,mega+1,1!Changing to 3 to allow for Ho's omega values
      ni = numb(nicount)
      call dsygv(ITYPE, jobz, uplo, ni, h, nn, p, nn, eigv, work, lwork, info)
      if (nicount .eq. 1) then
        write(6,fmt25) 'omega','No terms','eigen 1','eigen 2','eigen 3'!,'INFO'
      end if
      write(6,*) nicount-1,ni,eigv(1),eigv(2),eigv(3)!,info
      if(info.gt.0)  then
        write(6,*) "Something's wrong."
      end if
      write(8,*) 'eigvector',nicount-1
      do i=1,numb(nicount)
        write(8,*) ( h(i,j),j=1,numb(nicount) )
      end do

      if(nicount.eq.(mega+1)) then
        write(9,*) ni
        do i = 1,ni
          write(9,*) h(i,1)
        end do
      end if
      
      write(7,*) numb(nicount)
      do i=1,numb(nicount)
        write(7,*) eigv(i)
      end do

      do i=1,nt
        do j=1,i
          h(i,j) = h2(i,j)!*2.0!The factor of 2 is for Ry units. Remove to change to a.u.
          h(j,i) = h(i,j)
          p(i,j) = p2(i,j)
          p(j,i) = p(i,j)
        end do
      end do
    end do

    write(6,*) 'Omega =',mega
    write(6,*) 'NT =',nt

    end