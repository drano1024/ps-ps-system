#!/bin/bash

write_fort() {
    # Writes the fort.95 input file used for DrakePsPsBound.f
    # $1 is the omega value
    # $2 is used to include the electron exchange
    echo $1 $2
    echo "1" > fort.95
    echo "0.385" >> fort.95
    echo "0.48" >> fort.95
    echo "0.37" >> fort.95
    echo "15" >> fort.95
    echo $1 >> fort.95
    echo $2 >> fort.95
}

change_drake () {
    # Changed the output files used in DrakePsPsBound.f
    # with the correct omega value
    if [[ "$2" -eq 0 ]]
    then
        exc='Direct'
    else
        exc='P12'
    fi
    sed -i "/unit=8/c\        open(unit=8,file='MyIntegrals_$exc,$1',status='unknown') !input file for RRmethod" DrakePsPsBound.f
}

change_rrmethod () {
    # Changes the RRmethod.f95 input/output file names
    if [[ "$2" -eq 0 ]]
    then
        exc='Direct'
    else
        exc='P12'
    fi
    sed -i "/unit=48/c\    open(unit=48,file='MyIntegrals_$exc,$1',status='old')" RRmethod.f95
    sed -i "/unit=6/c\    open(unit=6,file='energy_$exc,$1',status='unknown')" RRmethod.f95
    sed -i "/unit=9/c\    open(unit=9,file='mycivals_$exc,$1',status='unknown')" RRmethod.f95
}

for omega in 0 1 2 3 4 5 6 7
do
    for ex in 0 1
    do
        write_fort $omega $ex
        change_drake $omega $ex
        change_rrmethod $omega $ex

        make -f makedrake
        make -f makefile
        ./drake
        ./RRmethod

        echo "Finished omega=$omega with exchange $ex"
    done
done