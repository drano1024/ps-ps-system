#!/bin/bash

write_fort() {
    # Writes the fort.95 input file used for DrakePsPsBound.f
    # $1 is the omega value
    # $2 is used to include the electron exchange
    echo $1
    echo "1" > fort.95
    echo "0.385" >> fort.95
    echo "0.48" >> fort.95
    echo "0.37" >> fort.95
    echo "15" >> fort.95
    echo $1 >> fort.95
}

change_avgvals () {
    # Changed the input/output files used in AvgValues.f
    # with the correct omega value and exchange
    if [[ "$2" -eq 0 ]]
    then
        exc='Direct'
        sed -i "/P12=/c\        P12=.false." AvgValues.f
    else
        exc='P12'
        sed -i "/P12=/c\        P12=.true." AvgValues.f
    fi
    sed -i "/unit=8/c\        open(unit=8,file='mycivals_$exc,$1',status='old')" AvgValues.f
    sed -i "/unit=15/c\        open(unit=15,file='average_$exc,$1',status='unknown')" AvgValues.f
    ln -sf ../Codes\ for\ Gabriel_s\ Values/mycivals_$exc,$1 ./
}

for omega in 0 1 2 3 4 5 6 7
do
    write_fort $omega
    for exc in 0 1
    do
        change_avgvals $omega $exc
        make -f makeavg
        ./avgval
    done
done

rm mycivals_Direct,*
rm mycivals_P12,*