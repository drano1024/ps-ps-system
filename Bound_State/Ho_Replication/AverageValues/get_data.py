import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import json
import sys

base_files = ['average_Direct,','average_P12,']

avg_vals = {}
columns = ['Omega','Exchange','r12','r13','r23','r14','r24','r34']
df = pd.DataFrame( columns=columns )
for i in range( 8 ):
    avg_vals[i] = {}

    for bf in base_files:
        file = bf+str(i)
        ex = bf.strip().split('_')[-1].replace(',','')
        avg_vals[i][ex] = {}
        tmp_dict = {'Omega':i,'Exchange':ex}

        with open( file ) as f: lines = f.readlines()
        for j, line in enumerate( lines ):
            l = line.strip().split()
            if j == 0 or len( l ) == 0: continue
            avg_vals[i][ex][l[0]] = float( l[1] )
            tmp_dict[l[0]] = float( l[1] )
            if l[0] not in columns: columns.append( l[0] )
        df = df.append( tmp_dict, ignore_index=True )

omega = np.unique( df['Omega'] )
fig, ax = plt.subplots(ncols=2)

for bf in base_files:
    ex = bf.strip().split('_')[-1].replace(',','')
    col = 0
    if ex == 'P12':
        col = 1
    for rij_key in columns[2:]:
        rij = df.where( df['Exchange'] == ex )[rij_key].dropna().to_list()
        ax[col].plot( omega, rij, label=rij_key, marker='x' )
for axi in ax:
    axi.legend()
    axi.set_xlabel( 'Omega' )
    axi.set_ylabel( r'$r_{ij}$ Values' )
plt.tight_layout()
plt.show()
