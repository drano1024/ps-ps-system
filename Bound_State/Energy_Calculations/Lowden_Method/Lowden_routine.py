def make_hermitian(mat):
    return (mat+mat.T)/2

def checks(d,U):
    I = np.identity(len(d))
    diag = False
    Umat = False
    if not np.array_equal(np.round(d@lin.inv(d),6),I):
        diag = True
    if not np.array_equal(np.round(U.T.conjugate()@U,6),I):
        Umat = True
    if diag and not Umat:
        #print(np.round(d@lin.inv(d),6))
        print("Eigenvalue Matrix, d, is wrong!")
    elif Umat and not diag:
        print("Eigenvector Matrix, U, is wrong!")
    elif Umat and diag:
        print("Eigenvalue and Eigenvector Matrices, d and U, are wrong!")
        
    return

def load_files(file):
    with open(file, 'r') as f:
        while True:
            try:
                line = f.readline().strip().split()
                w = int(line[0])
                line = f.readline().strip().split()
                nt = int(line[0])
                phiphi = np.zeros([nt,nt])
                phihphi = np.zeros([nt,nt])
                for iter in range(nt**2):
                    line = f.readline().strip().split()
                    i = int(line[0])-1
                    j = int(line[1])-1
                    pp_temp = float(line[2])
                    phiphi[i][j] = pp_temp
                    # Change the commented section of the next line
                    # if the file has the data on two lines
                    try:
                        php_temp = float(line[3])
                    except IndexError:
                        php_temp = float(f.readline().strip().split()[0])
                    phihphi[i][j] = php_temp
            except IndexError:
                break
    return phiphi,phihphi

def basis_removal(eigD,U):
    delidx = []
    for i in range(len(eigD)):
        if eigD[i] < 1e-1:
            delidx.append(i)
            #U = np.delete(U,i,1)
    return np.delete(U,delidx,1)

def Lowdin(H,D):
    #H = make_hermitian(H)
    #D = make_hermitian(D)
    eigD,U = lin.eigh(D)
    #print("Minimum eigenvalue of phiphi: ",np.amin(abs(eigD)))
    U = basis_removal(eigD,U)
    print(np.shape(U))
    d = U.T.conjugate()@D@U
    checks(d,U)
    #Last A evaluation gives best results for Direct Calculations.
    #A = U@slin.sqrtm(lin.inv(d))
    A = U@lin.inv(slin.sqrtm(d))
    H_prime = (A.T.conjugate())@H@A
    eig,C_prime = lin.eigh(H_prime)
    C = A@C_prime
    return eig,C

if __name__ == "__main__":
    import sys
    import numpy as np
    from scipy import linalg as slin
    from numpy import linalg as lin
    # Load in the correct files
    # These if statements are for allowing different exchanges to be
    # implemented.
    if len(sys.argv) == 2:
        phiphi,phihphi = load_files(sys.argv[1])
    elif len(sys.argv) == 3:
        phiphi_D,phihphi_D = load_files(sys.argv[1])
        phiphi_E,phihphi_E = load_files(sys.argv[2])
        phiphi = 2*(phiphi_D+phiphi_E)
        phihphi = 2*(phihphi_D+phihphi_E)
    elif len(sys.argv) == 5:
        phiphi_D,phihphi_D = load_files(sys.argv[1])
        phiphi_E12,phihphi_E12 = load_files(sys.argv[2])
        phiphi_E34,phihphi_E34 = load_files(sys.argv[3])
        phiphi_E1234,phihphi_E1234 = load_files(sys.argv[4])
        phiphi = 4*(phiphi_D+phiphi_E12+phiphi_E34+phiphi_E1234)
        phihphi = 4*(phihphi_D+phihphi_E12+phihphi_E34+phihphi_E1234)
    else:
        print("Input file(s) needed!")
    #print(phihphi)
    #print(phiphi)
    energy,C = Lowdin(phihphi,phiphi)
    #print("{:<6f}".format(energy[0]))
    print(energy[0])
    #print(len(C[:, 0]))
    #for ci in C[:, 0]:
    #    print(ci.real)
