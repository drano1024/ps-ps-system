#gfortran -c Todd.f90
#gfortran -o todd Todd.o -llapack -lblas -O3 -fdefault-real-8 -fdefault-double-8
#./todd

change_files() {
    # $1 - omega value
    # $2 - exchange (0-Direct, 1-P12, 2-P34, 3-P1234)
    
    if [[ "$2" -eq 0 ]]
    then
        exc="Direct"
    elif [[ "$2" -eq 1 ]]
    then
        exc="P12"
        sed -i "/P12=/c\	P12=.true." Todd.f90
        sed -i "/P34=/c\	P34=.false." Todd.f90
        sed -i "/P1234=/c\    P1234=.false." Todd.f90
    elif [[ "$2" -eq 2 ]]
    then
        exc="P34"
        sed -i "/P12=/c\	P12=.false." Todd.f90
        sed -i "/P34=/c\	P34=.true." Todd.f90
        sed -i "/P1234=/c\    P1234=.false." Todd.f90
    elif [[ "$2" -eq 3 ]]
    then
        exc="P12P34"
        sed -i "/P12=/c\    P12=.true." Todd.f90
        sed -i "/P34=/c\    P34=.true." Todd.f90
        sed -i "/P1234=/c\    P1234=.true." Todd.f90
    fi
    sed -i "/unit=8/c\    open(unit=8,file='Todd.$exc,$1.out',status='unknown')" Todd.f90
    sed -i "/unit=10/c\    open(unit=10,file='ssDirect,$1',status='unknown')" Todd.f90
    sed -i "/unit=11/c\    if(P12) open(unit=11,file='ssP12,$1',status='old')" Todd.f90
    sed -i "/unit=12/c\    if(P34) open(unit=12,file='ssP34,$1',status='old')" Todd.f90
    sed -i "/unit=13/c\    if(P1234) open(unit=13,file='ssP12P34,$1',status='old')" Todd.f90
    sed -i "/unit=14/c\    open(unit=14,file='ci.$exc,$1',status='unknown')" Todd.f90
}

for omega in 0 1
do
    # 0 - Direct
    # 1 - P12 exchange
    # 2 - P34 exchange
    # 3 - P1234 exchange
    for e in 0 1
    do
        change_files $omega $e
        sleep 1
        make todd
        ./todd
        make clean
    done
done