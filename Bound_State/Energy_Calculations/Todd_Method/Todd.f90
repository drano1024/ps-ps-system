Program Todd
	implicit none
	real*8 phiphi(2000,2000),phihphi(2000,2000)
	real*8, allocatable :: eigv(:),work(:)
	real*8 emin,etemp1,etemp2,eavg,ebas(2000),tola,diff
	real*8, allocatable :: phpu(:,:),ppu(:,:),phpl(:,:),ppl(:,:)
	real*8 ci(2000,2000),tdir,t12,t34,t1234,tdir2,t122,t342,t12342,exfac
	integer ITYPE,info,infot,nt,ijmin(2000*2000,2),n,ndel,nbas,srtval, lwork
	integer counter,temp1,temp2,num(1500000),row,col,cdel,rdel,nttm,ie,je,iet,jet
	integer del(2000),deldel,i,j,k,mega,order(2000),trash
	character U,uplo,uplot,jobz
	character(len = 12) bas
	character(len = 8) ftm
	character(len=20) filename
	logical check,writeall,P12, P34, P1234
	external dsygv
	!For dsygv, lwork needs to be 3*N-1. So for 462 terms, lwork needs to be 1400. 
	!For Higher omega values, the lwork value needs to be changes as well as the work size.
	bas = 'Basis Size: '
	ftm = '(A13,I4)'
	ijmin = 0
	tola = 1.d-6
	srtval = 1
	del = 0
	deldel = 0
	order = 0
	P12=.true.
	P34=.false.
    P1234=.false.
	exfac = 1.d0
	if(P12 .OR. P34) then
		exfac = 2.d0
	else if(P1234) then
		exfac = 4.d0
	end if
	
    open(unit=8,file='Todd.P12,1.out',status='unknown')
    open(unit=10,file='ssDirect,1',status='unknown')
    if(P12) open(unit=11,file='ssP12,1',status='old')
    if(P34) open(unit=12,file='ssP34,1',status='old')
    if(P1234) open(unit=13,file='ssP12P34,1',status='old')
    open(unit=14,file='ci.P12,1',status='unknown')
	
	read(10,*) mega
	read(10,*) nt
	if(P12) read(11,*) trash
	if(P12) read(11,*) trash
	if(P34) read(12,*) trash
	if(P34) read(12,*) trash
	if(P1234) read(13,*) trash
	if(P1234) read(13,*) trash
	nttm = nt
	
	do ie=1,nt
		do je=1,nt
			read(10,*) iet,jet,tdir,tdir2
			if(P12) then
				read(11,*) iet,jet,t12,t122
			else
				t12 = 0.d0
				t122 = 0.d0
			end if

			if(P34) then
				read(12,*) iet,jet,t12,t122
			else
				t34 = 0.d0
				t342 = 0.d0
			end if

			if(P1234) then
				read(13,*) iet,jet,t12,t122
			else
				t1234 = 0.d0
				t12342 = 0.d0
			end if
			phiphi(ie,je) = (tdir+t12)*exfac
			phihphi(ie,je) = (tdir2+t122)*exfac
		end do
	end do

	if(P1234) then
		write(filename,'(A13,I1)') 'ToddssP12P34,',mega
	else if(P34) then
		write(filename,'(A10,I1)') 'ToddssP34,',mega
	else if(P12) then
		write(filename,'(A10,I1)') 'ToddssP12,',mega
	else
		write(filename,'(A13,I1)') 'ToddssDirect,',mega
	end if

	open(unit=15,file=filename,status="unknown")
	
	do i=0,nt-1
		if(i.eq.0) then
			num(i+1) = 1
		else
			num(i+1) = num(i) + i
		end if
	end do
	
	if(num(nt-1).gt.size( num )) then
		write(8,*) "ERROR: ijmin Matrix too small."
		write(8,*) "Current Size: ", size( num )
		write(8,*) "Suggested size greater than: ",num(nt)
		write(8,*) ""
		stop
	else
		continue
	end if
	
	
	jobz='V'
	info = 0
	ITYPE=1
	uplo='L'
	uplot='U'
	emin = 10.0
	
	do nbas=1,nt
		ALLOCATE( phpl( nbas, nbas ) )
		ALLOCATE( ppl( nbas, nbas ) )
		ALLOCATE( phpu( nbas, nbas ) )
		ALLOCATE( ppu( nbas, nbas ) )
		ALLOCATE( eigv( nbas ) )
		lwork = 3*nbas-1
		ALLOCATE( work( lwork ) )
		phpl = 0.0
		ppl = 0.0
		phpu = 0.0
		ppu = 0.0
		eigv = 0.0
		work = 0.0
		if(nbas.eq.1) then
			do i=1,nt
				phpl(1,1) = phihphi(i,i)
				ppl(1,1) = phiphi(i,i)
				
				phpu(1,1) = phihphi(i,i)
				ppu(1,1) = phiphi(i,i)
				
				call dsygv(ITYPE,jobz,uplo,nbas,phpl,nbas,ppl,nbas,eigv,work,lwork,info)
				etemp1 = eigv(1)
				
				call dsygv(ITYPE,jobz,uplot,nbas,phpu,nbas,ppu,nbas,eigv,work,lwork,infot)
				etemp2 = eigv(1)
				diff = abs(etemp1-etemp2)
				eavg = 0.5*(etemp1+etemp2)
				
				if((diff.gt.tola).or.(info.ne.0).or.(infot.ne.0)) then
					del(deldel) = i
					deldel = deldel + 1
					nttm = nttm -1
				else if(diff.lt.tola) then
					eavg = 0.5*(etemp1+etemp2)
					if(eavg.lt.emin) then
						emin = eigv(1)
						ijmin(1,1) = i
						ijmin(1,2) = i
						row = i
						ebas(nbas) = emin
						ci(1,1) = 0.5d0*(phpl(1,1)+phpu(1,1))
					end if
				end if
			end do
			srtval = srtval + nbas
			print *, "Finished Basis: ", nbas,"Energy: ",emin
		else
			ndel = 0
			do n=1,nt
				If(n.lt.ijmin(1,1)) then
					ndel = ndel - 1
				else if(n.eq.ijmin(1,1)) then
					ndel = -ndel - 1
				else if(n.gt.ijmin(1,1)) then 
					ndel = ndel + 1
				end if
				col = ijmin(1,1) + ndel
				
				check = .true.
				do i=1,srtval
					if(col.eq.ijmin(i,1)) then
						check = .false.
					end if
				end do
				do i=1,deldel
					if(col.eq.del(i)) then
						check = .false.
					end if
				end do
				
				if(check) then
					!Old Terms
					counter = 1
					do j=1,nbas-1
						do k=1,j
							phpl(j,k) = phihphi(ijmin(counter,1),ijmin(counter,2))
							phpl(k,j) = phpl(j,k)
							ppl(j,k) = phiphi(ijmin(counter,1),ijmin(counter,2))
							ppl(k,j) = ppl(j,k)
							
							phpu(j,k) = phihphi(ijmin(counter,1),ijmin(counter,2))
							phpu(k,j) = phpu(j,k)
							ppu(j,k) = phiphi(ijmin(counter,1),ijmin(counter,2))
							ppu(k,j) = ppu(j,k)
							counter = counter + 1
						end do
					end do
					
					!New Terms
					do j=1,nbas
						if(j.eq.1) then
							phpl(nbas,j) = phihphi(col,row)
							phpl(j,nbas) = phpl(nbas,j)
							ppl(nbas,j) = phiphi(col,row)
							ppl(j,nbas) = ppl(nbas,j)
							
							phpu(nbas,j) = phihphi(col,row)
							phpu(j,nbas) = phpu(nbas,j)
							ppu(nbas,j) = phiphi(col,row)
							ppu(j,nbas) = ppu(nbas,j)
						else if((j.eq.nbas)) then
							phpl(nbas,nbas) = phihphi(col,col)
							ppl(nbas,nbas) = phiphi(col,col)
							
							phpu(nbas,nbas) = phihphi(col,col)
							ppu(nbas,nbas) = phiphi(col,col)
						else
							phpl(nbas,j) = phihphi(ijmin(num(nbas-1)+j-1,1),col)
							phpl(j,nbas) = phpl(nbas,j)
							ppl(nbas,j) = phiphi(ijmin(num(nbas-1)+j-1,1),col)
							ppl(j,nbas) = ppl(nbas,j)
							
							phpu(nbas,j) = phihphi(ijmin(num(nbas-1)+j-1,1),col)
							phpu(j,nbas) = phpu(nbas,j)
							ppu(nbas,j) = phiphi(ijmin(num(nbas-1)+j-1,1),col)
							ppu(j,nbas) = ppu(nbas,j)
						end if
					end do
					
					call dsygv(ITYPE,jobz,uplo,nbas,phpl,nbas,ppl,nbas,eigv,work,lwork,info)
					etemp1 = eigv(1)
					
					call dsygv(ITYPE,jobz,uplot,nbas,phpu,nbas,ppu,nbas,eigv,work,lwork,infot)
					etemp2 = eigv(1)
					eavg = 0.5*(etemp1+etemp2)
					
					writeall = .false.
					do j=1,nbas
						if((phpl(j,1).gt.1.d4).or.(phpu(j,1).gt.1.d4)) then
							writeall=.true.
						end if
					end do
					
					!************************
					!COMPARISON
					!************************
					
					diff = abs(etemp1-etemp2)
					if((diff.gt.tola).or.(info.ne.0).or.(infot.ne.0).or.writeall) then
						del(deldel+1) = col
						deldel = deldel + 1
						nttm = nttm -1
					else if(diff.lt.tola) then
						if(eavg.lt.emin) then
							emin = eavg
							ebas(nbas) = emin
							counter = srtval
							do j=1,nbas
								if(j.eq.1) then
									ijmin(counter,1) = col
									ijmin(counter,2) = row
									counter = counter + 1
								else if(j.eq.nbas) then
									ijmin(counter,1) = col
									ijmin(counter,2) = col
									counter = counter + 1
								else
									ijmin(counter,1) = ijmin(num(nbas-1)+j-1,1)
									ijmin(counter,2) = col
									counter = counter + 1
								end if
								ci(nbas,j) = 0.5d0*(phpl(j,1)+phpu(j,1))
							end do
						end if
					end if
				end if
			end do
			srtval = srtval + nbas
			print *, "Finished Basis: ", nbas,"Energy: ",emin
		end if
		DEALLOCATE( phpl, ppl, phpu, ppu, eigv, work )
	end do
	
	do i=1,nttm
		write(8,*) "*******************"
		write(8,ftm) bas,i
		write(8,*) "*******************"
		write(8,*) ""
		write(8,*) "Min Energy:"
		write(8,*) ebas(i)
		write(8,*) ""
	end do
	
	write(8,*) "Deleted Basis: "
	write(8,*) (del(i) ,i=1,deldel)
	write(8,*) ""
	write(8,*) "Final Matrix Size: ",nttm
	write(8,*) ""
	write(8,*) "Basis Order:"
	write(8,*) ijmin(1,1),(ijmin(num(nttm)+i,1) ,i=1,nttm-1)

	do i=0,nt-1
		if (i == 0) then
			order(i+1) = ijmin(1,1)
		else
			order(i+1) = ijmin(num(nttm)+i,1)
		end if
		write(15,*) order(i+1)
	end do

	write(14,*) nttm
	do i=1,nttm
		write(14,*) ci(nttm,i)
	end do

	! Testing the final matrix by making sure the energy matches.
	counter = 1
	ALLOCATE( phpl( nttm, nttm ) )
	ALLOCATE( ppl( nttm, nttm ) )
	ALLOCATE( phpu( nttm, nttm ) )
	ALLOCATE( ppu( nttm, nttm ) )
	ALLOCATE( eigv( nttm ) )
	lwork = 3*nttm-1
	ALLOCATE( work( lwork ) )
	phpl = 0.0
	ppl = 0.0
	phpu = 0.0
	ppu = 0.0
	eigv = 0.0
	work = 0.0
	do j=1,nttm
		do k=1,j
			phpl(j,k) = phihphi(ijmin(counter,1),ijmin(counter,2))
			phpl(k,j) = phpl(j,k)
			ppl(j,k) = phiphi(ijmin(counter,1),ijmin(counter,2))
			ppl(k,j) = ppl(j,k)
							
			phpu(j,k) = phihphi(ijmin(counter,1),ijmin(counter,2))
			phpu(k,j) = phpu(j,k)
			ppu(j,k) = phiphi(ijmin(counter,1),ijmin(counter,2))
			ppu(k,j) = ppu(j,k)
			counter = counter + 1
		end do
	end do
	
	call dsygv(ITYPE,jobz,uplo,nttm,phpl,nttm,ppl,nttm,eigv,work,lwork,info)
	etemp1 = eigv(1)
			
	call dsygv(ITYPE,jobz,uplot,nttm,phpu,nttm,ppu,nttm,eigv,work,lwork,infot)
	etemp2 = eigv(1)
	eavg = 0.5*(etemp1+etemp2)
	if(abs(eavg-ebas(nttm)).gt.1d-8) then
		print *, "Energies don't match!!"
	end if
End Program