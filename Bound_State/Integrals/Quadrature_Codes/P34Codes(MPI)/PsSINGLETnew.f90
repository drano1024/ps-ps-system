Program PsSINGLETnew
	implicit none
	include "mpif.h"
	integer, parameter :: nfi=500
	real*8, allocatable :: rx(:),rw(:),rx0(:),rw0(:)
	real*8, allocatable :: costp(:),sintp(:),ang(:,:,:)
	real*8, allocatable :: rcm(:,:,:,:),rcp(:,:,:,:)
	real*8, allocatable :: rij(:,:,:),volelemrd(:,:,:),volelemt(:,:)
	real*8 alp,bet,gam,pinv,qtpinv, t1, t2
	real*8 lam,mu,pptemp,phptemp
	real*8 r0t,r1c1,r23,r2c2,rt1,rt2,pi
	real*8 vol,volrt,voltt,w0t,w1t,w2t,wa,wtp,wtt,r12angt
	real*8, allocatable :: phiphi(:,:),phihphi(:,:),pcoeffl(:,:)
	real*8, allocatable :: respp(:,:),resphp(:,:)
	integer, allocatable :: intlist(:,:)
	integer :: j12(nfi),j13(nfi),j14(nfi),j23(nfi),j24(nfi),j34(nfi)
	integer iphi,ir0,ir1,ir2,it1,it2,qr0
	integer nt,mega,i,j,jp(58,6),ie,je,lk,ival,jval
	integer qr,qtp,root_process,proc
	integer ierr,iproc,rank,nproc,icomm,nthreads,tid,mega_old,nt_old
	logical, allocatable :: finished(:,:)
	character(LEN=100) :: infile
    call MPI_INIT(ierr)
    icomm = MPI_COMM_WORLD
    call MPI_COMM_SIZE(icomm,nproc,ierr)
	call MPI_COMM_RANK(icomm,iproc,ierr)
	t1 = MPI_WTIME()
	root_process = 0
	pi = 4.d0*ATAN(1.d0)
	pinv = 1.d0/pi

	CALL get_command_argument(1, infile)
	IF (iproc == 0) print *, 'file: ', trim(adjustl(infile))

	open(unit=1,file=trim(adjustl(infile)),status='old')
	open(unit=10,file='P34',status='unknown') !Output file for trail function 5
	open(unit=11,file='old_ints',status='unknown')

	read(1,*) mega
	write(10,*) mega
	IF (iproc == 0) print *, mega
	call const(j12,j13,j14,j23,j24,j34,mega,nt,nfi)
	write(10,*) nt
	IF (iproc == 0) print *, nt
	allocate (intlist(nt,nt),phiphi(nt,nt),phihphi(nt,nt),pcoeffl(nt,58))
	allocate(finished(nt,nt),respp(nt,nt),resphp(nt,nt))
	read(1,*) alp
	read(1,*) bet
	read(1,*) gam
	read(1,*) mu
	read(1,*) lam
	read(1,*) qr0,qr,qtp
	allocate(rx0(qr0),rw0(qr0),rx(qr),rw(qr),costp(qtp),sintp(qtp),ang(qtp,qtp,qtp))
	allocate(rcm(qr,qr,qtp,qtp),rcp(qr,qr,qtp,qtp),rij(qr0,qr,qr),volelemrd(qr,qr,qr0))
	allocate(volelemt(qtp,qtp))
	qtpinv = 1.d0/real(qtp,8)
	phiphi = 0.d0
	phihphi = 0.d0
	respp = 0.d0
	resphp = 0.d0
	pcoeffl = 0.d0
	finished = .false.

	!Saving the finished integrals
	read(11,*) mega_old
	read(11,*) nt_old
	do ie=1,nt_old
		do je=1,nt_old
			if ( iproc == root_process ) then
				read(11,*) i,j,phiphi(ie,je),phihphi(ie,je)
				if ( .not. phiphi(ie,je) == 0.d0 ) then
					finished(ie,je) = .true.
					print *, i,j,phiphi(ie,je), phihphi(ie,je)
				end if
			end if
		end do
	end do

	call mpi_bcast( finished, nt**2, MPI_LOGICAL, 0, icomm, ierr )

	!Splitting up the tasks between the processors
	proc = 0
	intlist = -1
	do ie=1,nt
		do je=1,nt
			if ( .not. finished(ie,je) ) then
				intlist(ie,je) = proc
				if ( proc == nproc - 1 ) then
					proc = 0
				else
					proc = proc + 1
				end if
			end if
		end do
	end do
	
	!Computing quadrature points
	!RADIAL QUADRATURE POINTS CALCULATIONS
	call gaulag(rx0,rw0,qr0)
	call gaulag(rx,rw,qr)

	!ANGULAR QUADRATURE POINTS CALCULATIONS
	do it1=1,qtp
		costp(it1)=Cos((2.d0*it1-1.d0)*pi*0.5d0*qtpinv)
		sintp(it1)=Sqrt(1.d0-costp(it1)**2.d0)
	end do!it1

	!Running all the precalculations
	call pre_calc(alp,bet,lam,qr0,qr,qtp,rx,rx0,costp,sintp,ang,rcm,rcp,rij,volelemrd,volelemt)
	
	do ie=1,nt
		call powercoeff(alp,bet,gam,j12,j13,j14,j23,j24,j34,ie,nfi,nt,pcoeffl)
	end do
	
	call jpmatrix(jp)

	call MPI_BARRIER( icomm, ierr )
	!Calculating the integrals seperatly
	do ie=1,nt
		do je=1,nt
			if ( iproc == intlist(ie,je) .and. (.not. finished(ie,je)) ) then
				print *, iproc, ie, je
				call Directint(rx,rx0,rw,rw0,qr,qr0,qtp,alp,bet,gam,lam,rcp,rcm,volelemrd, &
& volelemt,ang,rij,phptemp,pptemp,pcoeffl,j12,j13,j14,j23,j24,j34,jp,nfi,ie,je,nt)
				phihphi(ie,je) = phptemp
				phiphi(ie,je) = pptemp
				finished(ie,je) = .true.
				print *, ie,je,phiphi(ie,je),phihphi(ie,je)
				flush(6)
			end if
		end do
	end do
	
	call MPI_BARRIER( icomm, ierr )
    CALL MPI_REDUCE( phiphi, respp, nt**2, MPI_REAL8, MPI_SUM, 0, icomm, ierr )
	CALL MPI_REDUCE( phihphi, resphp, nt**2, MPI_REAL8, MPI_SUM, 0, icomm, ierr )
	if (iproc.eq.root_process) then
		do ie=1,nt
			do je=1,nt
				write(10,*) ie,je,respp(ie,je),resphp(ie,je)
			end do
		end do
	end if

	t2 = MPI_WTIME()
	IF (iproc == 0) print *, (t2-t1), 'seconds for quad points: ',qr0, qr, qtp
	
	call MPI_BARRIER( icomm, ierr )
	call MPI_FINALIZE(ierr)

End Program PsSINGLETnew

Subroutine Directint(rx,rx0,rw,rw0,qr,qr0,qtp,alp,bet,gam,lam,rcp,rcm,volelemrd, &
			& volelemt,ang,rij,phptemp,pptemp,pcoeffl,j12,j13,j14,j23,j24,j34,jp,nfi,ie,je,nt)
	implicit none
	integer nfi
	integer iphi,ir0,ir1,ir2,it1,it2,qr0,qtp,nt,qr
	real*8 phptemp,pptemp,pcoeffl(nt,58)
	real*8 rx(qr),rw(qr),rx0(qr0),rw0(qr0),pi
	real*8 ang(qtp,qtp,qtp),r12angt
	real*8 rcm(qr,qr,qtp,qtp),rcp(qr,qr,qtp,qtp)
	real*8 rij(qr0,qr,qr),volelemrd(qr,qr,qr0),volelemt(qtp,qtp)
	real*8 alp,bet,gam,funlk,fun,precal,exppv,vol,lam
	real*8 expqt,pinv,qtpinv,r0rcmt,r0rcpt
	real*8 r0t,r12,r14,r23,r34,rijt,rt1,rt2
	real*8 volrt,voltt,w0t,w1t,w2t,wa,wtp,wtt,fac
	real*8 j12e,j13e,j14e,j23e,j24e,j34e,j12p,j13p,j14p,j23p,j24p,j34p
	integer j12(nfi),j13(nfi),j14(nfi),j23(nfi),j24(nfi),j34(nfi)
	integer jp(58,6),ie,je,lk
	pi = 4.d0*ATAN(1.d0)
	pinv = 1.d0/pi
	qtpinv = 1.d0/real(qtp,8)
	wtp = pi*qtpinv
	wa = 2.d0*wtp*wtp*wtp
	fac = 2.d0*pi/((alp+lam)*(bet+lam)*(1.d0+lam))
	pptemp = 0.d0
	phptemp = 0.d0
	
	j12e=real(j12(ie)+j12(je),8)
	j13e=real(j13(ie)+j14(je),8)
	j14e=real(j14(ie)+j13(je),8)
	j23e=real(j23(ie)+j24(je),8)
	j24e=real(j24(ie)+j23(je),8)
	j34e=real(j34(ie)+j34(je),8)
	
	do ir2=1,qr
		rt2 = rx(ir2)/(alp+lam)
		w2t = rw(ir2)
		do ir1=1,qr
			rt1 = rx(ir1)/(bet+lam)
			w1t = rw(ir1)
			do ir0=1,qr0
				r0t = rx0(ir0)/(1.d0+lam)
				w0t = rw0(ir0)
				rijt = rij(ir0,ir1,ir2)
				expqt = Exp((1.d0+lam)*r0t+lam*(rt1+rt2))
				wtt=w0t*w1t*w2t*wa*expqt
				volrt = volelemrd(ir1,ir2,ir0)
				do it1=1,qtp
					do it2=1,qtp
						r0rcpt=r0t*rcp(ir1,ir2,it1,it2)
						r0rcmt=r0t*rcm(ir1,ir2,it1,it2)
						voltt=volelemt(it1,it2)
						do iphi=1,qtp
							!Direct Short Range (phi,phi) Integrals
							!R1 = r13
							!R2 = r24
							r12angt=0.5d0*rt1*rt2*ang(it1,it2,iphi)
							r12=Sqrt(rijt-r0rcmt-r12angt)
							r14=Sqrt(rijt+r0rcpt+r12angt)
							r23=Sqrt(rijt-r0rcpt+r12angt)
							r34=Sqrt(rijt+r0rcmt-r12angt)
							vol=volrt*wtt*voltt
							exppv=Exp(-bet*r23-2.d0*gam*r34-alp*r14)
							precal=vol*exppv
							
							fun=(r12**j12e)*(rt2**j13e)*(r14**j14e)*(r23**j23e)*(rt1**j24e)*(r34**j34e)
							pptemp=pptemp+precal*fun
							
							do lk=1,58
								j12p=real(j12e+jp(lk,1),8)
								j13p=real(j13e+jp(lk,2),8)
								j14p=real(j14e+jp(lk,3),8)
								j23p=real(j23e+jp(lk,4),8)
								j24p=real(j24e+jp(lk,5),8)
								j34p=real(j34e+jp(lk,6),8)
								
								funlk=(r12**j12p)*(rt2**j13p)*(r14**j14p)*(r23**j23p)*(rt1**j24p)*(r34**j34p)
								phptemp=phptemp+funlk*pcoeffl(ie,lk)*precal
							end do
						end do!iphi
					end do!it2
				end do!it1
			end do!ir2
		end do!ir1
	end do!ir0
	pptemp = pptemp*fac
	phptemp = phptemp*fac
End Subroutine

!
! This subroutine precalculates a lot of the simple and small calculations
! that are used in the integral calculations. Its faster to reference an array
! than it is to repeat the same calculations multiple times.
!

Subroutine pre_calc(alp,bet,lam,qr0,qr,qtp,rx,rx0,costp,sintp,ang,rcm,rcp,rij,volelemrd,volelemt)
	implicit none
	real*8, intent(in) :: alp,bet,lam
	integer, intent(in) :: qr,qtp,qr0
	real*8, intent(in) :: rx(qr),rx0(qr0),costp(qtp),sintp(qtp)
	real*8, intent(inout) :: ang(qtp,qtp,qtp),rcm(qr,qr,qtp,qtp),rcp(qr,qr,qtp,qtp)
	real*8, intent(inout) :: rij(qr0,qr,qr),volelemrd(qr,qr,qr0),volelemt(qtp,qtp)
	real*8 r0t,r1c1,r2c2,rt1,rt2
	integer iphi,ir0,ir1,ir2,it1,it2

	!ANGULAR CALCULATIONS
	do it1=1,qtp
		do it2=1,qtp
			do iphi=1,qtp
				ang(it1,it2,iphi)=costp(it1)*costp(it2)+sintp(it1)*sintp(it2)*costp(iphi)
			end do!iphi
			volelemt(it1,it2)=sintp(it1)*sintp(it2)
		end do!it2
	end do!it1
	
	!Short Range Integrals
	!RADIAL & ANGULAR P34 CALCULATION
	do ir1=1,qr
		rt1 = rx(ir1)/(bet+lam)
		do it1=1,qtp
			r1c1=rt1*costp(it1)
			do ir2=1,qr
				rt2 = rx(ir2)/(alp+lam)
				do it2=1,qtp
					r2c2=rt2*costp(it2)
					rcp(ir1,ir2,it1,it2)=r1c1+r2c2
					rcm(ir1,ir2,it1,it2)=r1c1-r2c2
				end do!qtp
			end do!ir1
		end do!it1
	end do!ir0
	
	!RADIAL
	do ir2=1,qr
		rt2 = rx(ir2)/(alp+lam)
		do ir1=1,qr
			rt1 = rx(ir1)/(bet+lam)
			do ir0=1,qr0
				r0t = rx0(ir0)/(1.d0+lam)
				rij(ir0,ir1,ir2)=r0t**(2.d0)+0.25d0*(rt1**(2.d0)+rt2**(2.d0))
				volelemrd(ir1,ir2,ir0) = (r0t**2.d0)*(rt1**2.d0)*(rt2**2.d0)
			end do!ir2
		end do!ir1
	end do!ir0
End Subroutine

      !
      ! This subroutine creates the constants a1,a2,a3,a4,a5, and a6
      ! for the exponential terms.
      !

Subroutine const(a1,a2,a3,a4,a5,a6,mega,nt,nfi) ! Obtained from Peter Van Reeth
	implicit none
	integer nfi
	integer a1(nfi),a2(nfi),a3(nfi),a4(nfi),a5(nfi),a6(nfi) !exponential constants!
	integer nt,mega,inx,nt2
	integer megap,i,i12,i13,i14,i23,i24,i34
	integer i12p1,i13p1,i14p1,i23p1,i24p1,i34p1
	integer i14p1m,i13p1m,i24p1m,i23p1m,j
	logical track

	megap = mega + 1
	inx = 0

	do i = 1,megap
		do i12p1 = 1,i,1
			i12 = i12p1 - 1
			i14p1m = i - i12
			do i14p1 = 1,i14p1m
				i14 = i14p1 - 1
				i13p1m = i14p1m - i14
				do i13p1 = 1,i13p1m
					i13 = i13p1 - 1
					i24p1m = i13p1m - i13
					do i24p1 = 1,i24p1m
						i24 = i24p1 - 1
						i23p1m = i24p1m - i24
						do i23p1 = 1,i23p1m
							i23 = i23p1 - 1
							i34 = i23p1m - i23p1
							track = .true.
							
							inx = inx + 1
							a1(inx) = i12
							a2(inx) = i13
							a3(inx) = i14
							a4(inx) = i23
							a5(inx) = i24
							a6(inx) = i34
						end do
					end do
				end do
			end do
		end do
		!write(5,*) i,inx
	end do
	nt = inx
	!write(5,*) ' N    r12    r13    r14    r23    r24    r34'
	!write(5,"(7I7)") (j,a1(j),a2(j),a3(j),a4(j),a5(j),a6(j),j=1,nt)
End Subroutine

Subroutine gaulag(x,w,n)
	implicit none
	integer n
	real*8 x(n),w(n)
	real*8 anu(n)
	real*8, parameter :: eps=3.0d-13
	real*8, parameter :: pi=4.d0*atan(1.d0)
	integer j,its,i
	integer, parameter :: maxit=10
	real*8, parameter :: c1=9.084064d-1,c2=5.214976d-2
	real*8, parameter :: c3=2.579930d-3,c4=3.986126d-3
	real*8 rhs(n),r2(n),r3(n),theta(n),p1(n),p2(n),p3(n)
	real*8 pp(n),z(n),z1(n)
	logical unfinished(n)
	x=0.d0
	w=0.d0
	anu = 4.d0*n+2.d0
	rhs(1) = (4.d0*n-1.d0)*pi/anu(1)
	do i=2,n
		rhs(i)=(4.d0*n-4.d0*i+3.d0)*pi/anu(i)
	end do
	r3 = rhs**(1.d0/3.d0)
	r2 = r3**2.d0
	theta = r3*(c1+r2*(c2+r2*(c3+r2*c4)))
	z = anu*(Cos(theta)**2.d0)
	unfinished = .true.
	do its=1,maxit
		where (unfinished)
			p1=1.d0
			p2=0.d0
		end where
		do j=1,n
			where (unfinished)
				p3=p2
				p2=p1
				p1=((2.d0*real(j,8)-1.d0-z)*p2-(real(j,8)-1.d0)*p3)/real(j,8)
			end where
		end do
		where (unfinished)
			pp=(n*p1-n*p2)/z
			z1=z
			z=z1-p1/pp
			unfinished = (abs(z-z1) > eps*z)
		end where
		if (.not. any(unfinished)) exit
	end do
	if (its == maxit+1) print *, 'too many iterations in gaulag'
	x=z
	w=-1.d0/(pp*real(n,8)*p2)
End Subroutine

      !
      !jpmatrix creates a matrix of the powers of the interparticle
      !distances that are from the H operation on phi.
      !

Subroutine jpmatrix(jp)
	implicit none
	integer jp(58,6),jp2(58,6)
	integer ipp,jpp

	jp=0
	jp2=0

	do ipp=2,7
		jp2(ipp,ipp-1) = -1
		jp2(ipp+6,ipp-1) = -2
	end do

	jp2(14,2) = 1
	jp2(14,4) = -1
	jp2(15,2) = 1
	jp2(15,6) = -1

	jp2(16,4) = 1
	jp2(16,2) = -1
	jp2(17,4) = 1
	jp2(17,6) = -1

	jp2(18,6) = 1
	jp2(18,2) = -1
	jp2(19,6) = 1
	jp2(19,4) = -1

	jp2(20,2) = 1
	jp2(20,1) = -2
	jp2(21,2) = 1
	jp2(21,3) = -2
	jp2(22,2) = 1
	jp2(22,4) = -2
	jp2(23,2) = 1
	jp2(23,6) = -2

	jp2(24,4) = 1
	jp2(24,1) = -2
	jp2(25,4) = 1
	jp2(25,2) = -2
	jp2(26,4) = 1
	jp2(26,5) = -2
	jp2(27,4) = 1
	jp2(27,6) = -2

	jp2(28,6) = 1
	jp2(28,2) = -2
	jp2(29,6) = 1
	jp2(29,3) = -2
	jp2(30,6) = 1
	jp2(30,4) = -2
	jp2(31,6) = 1
	jp2(31,5) = -2

	jp2(32,1) = 2
	jp2(32,3) = -2
	jp2(32,5) = -2
	jp2(33,1) = 2
	jp2(33,2) = -1
	jp2(33,4) = -1
	jp2(34,1) = 2
	jp2(34,2) = -1
	jp2(34,4) = -2
	jp2(35,1) = 2
	jp2(35,2) = -2 
	jp2(35,4) = -1
	jp2(36,1) = 2
	jp2(36,2) = -2
	jp2(36,4) = -2

	jp2(37,2) = 2
	jp2(37,1) = -2
	jp2(37,4) = -2
	jp2(38,2) = 2
	jp2(38,1) = -2
	jp2(38,4) = -1
	jp2(39,2) = 2
	jp2(39,3) = -2
	jp2(39,6) = -2
	jp2(40,2) = 2
	jp2(40,3) = -2
	jp2(40,6) = -1
	
	jp2(41,3) = 2
	jp2(41,1) = -2
	jp2(41,5) = -2
	jp2(42,3) = 2
	jp2(42,2) = -1
	jp2(42,6) = -1
	jp2(43,3) = 2
	jp2(43,2) = -1
	jp2(43,6) = -2
	jp2(44,3) = 2
	jp2(44,2) = -2
	jp2(44,6) = -1
	jp2(45,3) = 2
	jp2(45,2) = -2
	jp2(45,6) = -2

	jp2(46,4) = 2
	jp2(46,1) = -2
	jp2(46,2) = -2
	jp2(47,4) = 2
	jp2(47,1) = -2
	jp2(47,2) = -1
	jp2(48,4) = 2
	jp2(48,5) = -2
	jp2(48,6) = -2
	jp2(49,4) = 2
	jp2(49,5) = -2
	jp2(49,6) = -1
	
	jp2(50,5) = 2
	jp2(50,1) = -2
	jp2(50,3) = -2
	jp2(51,5) = 2
	jp2(51,4) = -1
	jp2(51,6) = -1
	jp2(52,5) = 2
	jp2(52,4) = -1
	jp2(52,6) = -2
	jp2(53,5) = 2
	jp2(53,4) = -2
	jp2(53,6) = -1
	jp2(54,5) = 2
	jp2(54,4) = -2
	jp2(54,6) = -2

	jp2(55,6) = 2
	jp2(55,2) = -2
	jp2(55,3) = -2
	jp2(56,6) = 2
	jp2(56,2) = -1
	jp2(56,3) = -2
	jp2(57,6) = 2
	jp2(57,4) = -2
	jp2(57,5) = -2
	jp2(58,6) = 2
	jp2(58,4) = -1
	jp2(58,5) = -2

	do ipp=1,58
		do jpp=1,6
			jp(ipp,jpp) = jp2(ipp,jpp)
		end do
	end do
	
	!write(7,*) "jp matrix"
	!write(7,*) 'N ','r12 ','r13 ','r14 ','r23 ','r24 ','r34 '
	!do ipp=1,58
	!	write(7,*) ipp,jp(ipp,1),jp(ipp,2),jp(ipp,3),jp(ipp,4),jp(ipp,5),jp(ipp,6)
	!end do
End Subroutine

      !
      !This subroutine creates the coefficients for the phi H phi matrix elements
      !

Subroutine powercoeff(alp,bet,gam,ii,ij,ik,il,im,in2,ie,nfi,nt,pcoeffl)
	implicit none
	integer nfi,nt
	real*8 pcoeffl(nt,58)
	real*8 alp,bet,gam
	real*8 c13,c23,c34,c13h,c23h,c34h
	integer ii(nfi),ij(nfi),ik(nfi),il(nfi),im(nfi),in2(nfi)
	integer ie

	c13 = alp
	c23 = bet
	c34 = gam
	c13h = c13*0.5d0
	c23h = c23*0.5d0
	c34h = c34*0.5d0
	
	pcoeffl(ie,1)=-c13**(2.d0)-c23**(2.d0)-c34**(2.d0)
	pcoeffl(ie,2)=1.d0
	pcoeffl(ie,3)=-1.d0+2.d0*c13*(ij(ie)+1.d0)+c13h*(ii(ie)+ik(ie)+il(ie)+in2(ie))
	pcoeffl(ie,4)=-1.d0
	pcoeffl(ie,5)=-1.d0+2.d0*c23*(il(ie)+1.d0)+c23h*(ii(ie)+ij(ie)+im(ie)+in2(ie))
	pcoeffl(ie,6)=-1.d0
	pcoeffl(ie,7)=1.d0+2.d0*c34*(in2(ie)+1.d0)+c34h*(ij(ie)+ik(ie)+il(ie)+im(ie))
	pcoeffl(ie,8)=-ii(ie)*(ii(ie)+1.d0)-0.5d0*ii(ie)*(ij(ie)+ik(ie)+il(ie)+im(ie))
	pcoeffl(ie,9)=-ij(ie)*(ij(ie)+1.d0)-0.5d0*ij(ie)*(ii(ie)+ik(ie)+il(ie)+in2(ie))
	pcoeffl(ie,10)=-ik(ie)*(ik(ie)+1.d0)-0.5d0*ik(ie)*(ii(ie)+ij(ie)+im(ie)+in2(ie))
	pcoeffl(ie,11)=-il(ie)*(il(ie)+1.d0)-0.5d0*il(ie)*(ii(ie)+ij(ie)+im(ie)+in2(ie))
	pcoeffl(ie,12)=-im(ie)*(im(ie)+1.d0)-0.5d0*im(ie)*(ii(ie)+ik(ie)+il(ie)+in2(ie))
	pcoeffl(ie,13)=-in2(ie)*(in2(ie)+1.d0)-0.5d0*in2(ie)*(ij(ie)+ik(ie)+il(ie)+im(ie))
	pcoeffl(ie,14)=-0.5d0*c13*c23
	pcoeffl(ie,15)=-0.5d0*c13*c34
	pcoeffl(ie,16)=-0.5d0*c13*c23
	pcoeffl(ie,17)=-0.5d0*c23*c34
	pcoeffl(ie,18)=-0.5d0*c13*c34
	pcoeffl(ie,19)=-0.5d0*c23*c34
	pcoeffl(ie,20)=0.5d0*ii(ie)*c13
	pcoeffl(ie,21)=0.5d0*ik(ie)*c13
	pcoeffl(ie,22)=0.5d0*il(ie)*c13
	pcoeffl(ie,23)=0.5d0*in2(ie)*c13
	pcoeffl(ie,24)=0.5d0*ii(ie)*c23
	pcoeffl(ie,25)=0.5d0*ij(ie)*c23
	pcoeffl(ie,26)=0.5d0*im(ie)*c23
	pcoeffl(ie,27)=0.5d0*in2(ie)*c23
	pcoeffl(ie,28)=0.5d0*ij(ie)*c34
	pcoeffl(ie,29)=0.5d0*ik(ie)*c34
	pcoeffl(ie,30)=0.5d0*il(ie)*c34
	pcoeffl(ie,31)=0.5d0*im(ie)*c34
	pcoeffl(ie,32)=0.5d0*ik(ie)*im(ie)
	pcoeffl(ie,33)=0.5d0*c13*c23
	pcoeffl(ie,34)=-0.5d0*il(ie)*c13
	pcoeffl(ie,35)=-0.5d0*ij(ie)*c23
	pcoeffl(ie,36)=0.5d0*ij(ie)*il(ie)
	pcoeffl(ie,37)=0.5d0*ii(ie)*il(ie)
	pcoeffl(ie,38)=-0.5d0*ii(ie)*c23
	pcoeffl(ie,39)=0.5d0*ik(ie)*in2(ie)
	pcoeffl(ie,40)=-0.5d0*ik(ie)*c34
	pcoeffl(ie,41)=0.5d0*ii(ie)*im(ie)
	pcoeffl(ie,42)=0.5d0*c13*c34
	pcoeffl(ie,43)=-0.5d0*in2(ie)*c13
	pcoeffl(ie,44)=-0.5d0*ij(ie)*c34
	pcoeffl(ie,45)=0.5d0*ij(ie)*in2(ie)
	pcoeffl(ie,46)=0.5d0*ii(ie)*ij(ie)
	pcoeffl(ie,47)=-0.5d0*ii(ie)*c13
	pcoeffl(ie,48)=0.5d0*im(ie)*in2(ie)
	pcoeffl(ie,49)=-0.5d0*im(ie)*c34
	pcoeffl(ie,50)=0.5d0*ii(ie)*ik(ie)
	pcoeffl(ie,51)=0.5d0*c23*c34
	pcoeffl(ie,52)=-0.5d0*in2(ie)*c23
	pcoeffl(ie,53)=-0.5d0*il(ie)*c34
	pcoeffl(ie,54)=0.5d0*il(ie)*in2(ie)
	pcoeffl(ie,55)=0.5d0*ij(ie)*ik(ie)
	pcoeffl(ie,56)=-0.5d0*ik(ie)*c13
	pcoeffl(ie,57)=0.5d0*il(ie)*im(ie)
	pcoeffl(ie,58)=-0.5d0*im(ie)*c23
End Subroutine
