#!/bin/bash

write_fort() {
    # Writes the fort.95 input file used for DrakePsPsBound.f
    # $1 is the omega value
    echo $1
    echo "1" > fort.95
    echo "0.385" >> fort.95
    echo "0.48" >> fort.95
    echo "0.37" >> fort.95
    echo "15" >> fort.95
    echo $1 >> fort.95
}

make ints
for omega in 0 1
do
    write_fort $omega
    ./integrals
done

make clean