      Program AsymptoticExpansion
      	parameter(nfi=1000)
      	real*8 directPP(nfi,nfi),directPHP(nfi,nfi)
      	real*8 P12PP(nfi,nfi),P12PHP(nfi,nfi)
        real*8 phiphiT(nfi,nfi),phihphiT(nfi,nfi)
        real*8, dimension(-2:90,-2:90,-2:90,6):: resus
        real*8, dimension(-2:90,-2:90,-2:90,6):: resusd
        real*8 rcterm(130,130,130),pcoeffl(58)
        real*8 phi2,phid2,start,finish,time
        real*8 alpk(5),betk(5),gamk(5)
        real*8 alpkd(5),betkd(5),gamkd(5)
        real*16 zetacom(100),s,zetaf
        integer jp(58,6),diff,id
        integer i,j,nt,mega,nt2,nphi,nlam,biglam
        integer ii(nfi),ij(nfi),ik(nfi),il(nfi),im(nfi),in2(nfi)
		    character(len=1024) :: direct,p12
        call cpu_time(start)
        pi = 4.0*ATAN(1.0)
        directPP = 0.0
        directPHP = 0.0
        P12PP = 0.0
        P12PHP = 0.0
        biglam = 7

        open(unit=1,file='fort.95',status='old')
        open(unit=4,file='constasy',status='unknown')
        open(unit=8,file='outonedrakeasy',status='unknown') !General information output
        open(unit=7,file='jpmatrixasy',status='unknown') !jp matrix values. Comes from Hphi action

        read(1,*) nphi !number of short range terms with different exp

        do i=1,nphi
          read(1,*) alpk(i)
          read(1,*) betk(i)
          read(1,*) gamk(i)

          !add exchange terms here
          !P12
          alpkd(i) = betk(i)
          betkd(i) = alpk(i)
          gamkd(i) = gamk(i)

          write(8,*) 'alpha',i,alpk(i)
          write(8,*) 'beta',i,betk(i)
          write(8,*) 'gamma',i,gamk(i)
        end do

        !max q value in eq. 5 Drake&Yan
        read(1,*) nqs
        write(8,*) 'q=',nqs
        nq = nqs
        nlam = nq-biglam+1

        !Setting up the zeta values
        s = 1.5
        do i=1,90
          zetacom(i) = zetaf(s,500,nq) !Change the 500 for more accuracy!!
          s = s + 0.5
        end do

        read(1,*) mega
		    write(direct,"(A9,I1)") "ssDirect,",mega
		    write(p12,"(A6,I1)") "ssP12,",mega
		    open(unit=14,file=direct,status='unknown') !Overlap matrix
		    open(unit=15,file=p12,status='unknown') !Hamiltonian Matrix
        call const(ii,ij,ik,il,im,in2,mega,nt,nt2,nfi)
        write(8,*) 'N =',nt
        write(14,"(I4)") mega
        write(14,"(I4)") nt
        write(15,"(I4)") mega
        write(15,"(I4)") nt

        do kc=1,nphi
          do lc=kc,kc
            a1 = alpk(kc)+alpk(lc)
            a2 = betk(kc)+betk(lc)
            a3 = gamk(kc)+gamk(lc)

            !Here are the exchange exponential fall offs P12
            a1d = alpk(kc)+alpkd(lc)
            a2d = betk(kc)+betkd(lc)
            a3d = gamk(kc)+gamkd(lc)

            !Here we calculate the W function from Drake&Yan for all
            !combinations. To be called later.

            lmax = 2*mega+2*nqs+10
            mmax = 2*mega+10
            nmax = 2*mega+2*nqs+10

            do lh=0,lmax
              do mh=0,mmax
                do nh=0,nmax

                  lht=lh
                  mht=mh
                  nht=nh-2*nq-1
                  
                  if((lht+mht+nht+2).gt.0)then
                    call wfunction(lht,mht,nht,a1,a2,a3,res1)
                    resus(lh,mh,nh,1) = res1
                    call wfunction(lht,mht,nht,a1,a3,a2,res2)
                    resus(lh,mh,nh,2) = res2
                    call wfunction(lht,mht,nht,a2,a1,a3,res3)
                    resus(lh,mh,nh,3) = res3
                    call wfunction(lht,mht,nht,a2,a3,a1,res4)
                    resus(lh,mh,nh,4) = res4
                    call wfunction(lht,mht,nht,a3,a1,a2,res5)
                    resus(lh,mh,nh,5) = res5
                    call wfunction(lht,mht,nht,a3,a2,a1,res6)
                    resus(lh,mh,nh,6) = res6

                    !P12 exchange terms
                    call wfunction(lht,mht,nht,a1d,a2d,a3d,res1d)
                    resusd(lh,mh,nh,1) = res1d
                    call wfunction(lht,mht,nht,a1d,a3d,a2d,res2d)
                    resusd(lh,mh,nh,2) = res2d
                    call wfunction(lht,mht,nht,a2d,a1d,a3d,res3d)
                    resusd(lh,mh,nh,3) = res3d
                    call wfunction(lht,mht,nht,a2d,a3d,a1d,res4d)
                    resusd(lh,mh,nh,4) = res4d
                    call wfunction(lht,mht,nht,a3d,a1d,a2d,res5d)
                    resusd(lh,mh,nh,5) = res5d
                    call wfunction(lht,mht,nht,a3d,a2d,a1d,res6d)
                    resusd(lh,mh,nh,6) = res6d
                    
                  end if
                end do
              end do
            end do

            !Creating the Cjqk terms from Drake & Yan, equation 4
            jx = 2*mega+12
            call ccoeff(jx,rcterm)

            !Create the matrix for the extra powers in the linear terms
            !that come from the Hamiltonian acting on the wavefunction

            call jpmatrix(jp)

            !Here begins the phiphi and phiHphi integration loops
            pihalf=0.25/pi

            do ie=1,nt
              do je=1,nt

                !Calling the coefficients for each term in phiHphi
                call powercoeff(alpk,betk,gamk,ii,ij,ik,il,im,in2,nfi,kc,ie,pcoeffl)

     			      j12e = ii(ie)+ii(je)
                j13e = ij(ie)+ij(je)
                j14e = ik(ie)+ik(je)
                j23e = il(ie)+il(je)
                j24e = im(ie)+im(je)
                j34e = in2(ie)+in2(je)

                do lk=1,58 !58 is the number of terms for the phiHphi. Change as neccessary
                  j12 = j12e+jp(lk,1)
                  j13 = j13e+jp(lk,2)
                  j14 = j14e+jp(lk,3)
                  j23 = j23e+jp(lk,4)
                  j24 = j24e+jp(lk,5)
                  j34 = j34e+jp(lk,6)
                  nq = nqs

     			        phires2 = 0.0
                  do jq=0,nq
                    call Tval(nq,jq,j13,j23,j34,j12,j14,j24,rcterm,resus,Tp)
                  	phires2 = phires2+Tp
                  end do
                  call AsyExp(nq,j13,j23,j34,j12,j14,j24,biglam,rcterm,resus,asypt,zetacom)
     			        phires2 = phires2 + asypt
                  
                  !P12 exchange
                  j12d = ii(ie)+ii(je)+jp(lk,1)
                  j13d = ij(ie)+il(je)+jp(lk,2)
                  j14d = ik(ie)+im(je)+jp(lk,3)
                  j23d = il(ie)+ij(je)+jp(lk,4)
                  j24d = im(ie)+ik(je)+jp(lk,5)
                  j34d = in2(ie)+in2(je)+jp(lk,6)
                  nq = nqs

                  phiresd2 = 0.0
                  do jq=0,nq
                    call Tval(nq,jq,j13d,j23d,j34d,j12d,j14d,j24d,rcterm,resusd,Tp)
                  	phiresd2 = phiresd2+Tp
                  end do
                  call AsyExp(nq,j13d,j23d,j34d,j12d,j14d,j24d,biglam,rcterm,resusd,asypt,zetacom)
     			        phiresd2 = phiresd2 + asypt
                  
                  phi2 = phires2*pihalf
                  phid2 = phiresd2*pihalf

                  id = nt*(kc-1)+ie
                  jf = nt*(lc-1)+je
                  directPHP(id,jf)=directPHP(id,jf)+phi2*pcoeffl(lk)
                  P12PHP(id,jf)=P12PHP(id,jf)+phid2*pcoeffl(lk)
     
                  if(lk.eq.1) then
                    directPP(id,jf) = phi2
                    P12PP(id,jf) = phid2
                  end if
                  
                end do
              end do
            end do
          end do
        end do

        ntnphi = nt*nphi

        do i=1,ntnphi
			    do j=1,ntnphi
		    		write(14,"(2I4,2F50.30,2X)") i, j, directPP(i,j), directPHP(i,j)
		    		write(15,"(2I4,2F50.30,2X)") i, j, P12PP(i,j), P12PHP(i,j)
			    end do
        end do

        call cpu_time(finish)
        diff = int(finish - start)
        seconds = int(mod(diff,60))
        minutes = int((diff-seconds)/60.0)
        time = finish - start
        write(8,*) 'Time:',time,'Seconds'
        write(8,*) 'Total Time:',minutes,'Minutes and',seconds,'Seconds'

      End Program AsymptoticExpansion

      !
      ! This subroutine creates the constants a1,a2,a3,a4,a5, and a6
      ! for the exponential terms.
      !

      Subroutine const(ii,ij,ik,il,im,in2,mega,nt,nt2,nfi) ! Obtained from Peter Van Reeth
      	integer ii(nfi),ij(nfi),ik(nfi),il(nfi),im(nfi),in2(nfi)
      	integer nt,mega,inx
        integer megap,i,i12,i13,i14,i23,i24,i34
        integer i12p1,i13p1,i14p1,i23p1,i24p1,i34p1
        integer i14p1m,i13p1m,i24p1m,i23p1m

        megap = mega + 1
        inx = 0

        !First 126 Terms
        do i = 1,megap
          do i12p1 = 1,i,1
            i12 = i12p1 - 1
            i14p1m = i - i12
            do i14p1 = 1,i14p1m
              i14 = i14p1 - 1
              i13p1m = i14p1m - i14
              do i13p1 = 1,i13p1m
                i13 = i13p1 - 1
                i24p1m = i13p1m - i13
                do i24p1 = 1,i24p1m
                  i24 = i24p1 - 1
                  i23p1m = i24p1m - i24
                  do i23p1 = 1,i23p1m
                  	i23 = i23p1 - 1
                    i34 = i23p1m - i23p1

                    inx = inx + 1
                    ii(inx) = i12
                    ij(inx) = i13
                    ik(inx) = i14
                    il(inx) = i23
                    im(inx) = i24
                    in2(inx) = i34

                  end do
                end do
              end do
            end do
          end do
          write(4,*) (i-1),inx
		  write(8,*) (i-1),inx
        end do
        
        nt = inx
        nt2 = inx
        write(4,*) ' N    r12    r13    r14    r23    r24    r34'
        write(4,"(7I7)")(j,ii(j),ij(j),ik(j),il(j),im(j),in2(j),j=1,nt2)
      End Subroutine

      Subroutine Tval(nq,nqt,j1,j2,j3,j12,j13,j23,rcterm,resus,Tp)
      	real*8 rcterm(130,130,130)
        real*8 ,dimension(-2:90,-2:90,-2:90,6)::resus
        real*8 pi,piterm,Tp
        real*8 cterm,rqt
        integer lht,mht,nht,j1,j2,j3,j12,j13,j23
        integer nqt,nq

        Tp = 0.0
        cterm = 0.0

        pi = 4.0*atan(1.0)
        piterm = (4.0*pi)**3.0

        L12 = int((j12+1)/2)
        L13 = int((j13+1)/2)
        L23 = int((j23+1)/2)

        j1j12 = j1+2+j12
        j1j13 = j1+2+j13
        j2j12 = j2+2+j12
        j2j23 = j2+2+j23
        j3j13 = j3+2+j13
        j3j23 = j3+2+j23
          
        j1q = j1+2+2*nqt
        j2q = j2+2+2*nqt
        j3q = j3+2+2*nqt
        j1j12q = j1j12+j13-2*nqt
        j2j12q = j2j12+j23-2*nqt
        j3j13q = j3j13+j23-2*nqt
        
        rqt = piterm/((2.0*nqt+1.0)**2.0)

        do k12=0,L12
          do k13=0,L13
            do k23=0,L23
              nq2 = 2*nq+1
              rcoeff=rcterm(j12+2,nqt+2,k12+2)*rcterm(j13+2,nqt+2,k13+2)*rcterm(j23+2,nqt+2,k23+2)*rqt
            
              !First W(...) function integral
              lht = j1q+2*k12+2*k13
              mht = j2j12-2*k12+2*k23
              nht = j3j13q-2*k23-2*k13+nq2
              res1 = resus(lht,mht,nht,1)

              !Second W(...) function integral
              lht = j1q+2*k12+2*k13
              mht = j3j13-2*k13+2*k23
              nht = j2j12q-2*k23-2*k12+nq2
              res2 = resus(lht,mht,nht,2)

              !Third W(...) function integral
              lht = j2q+2*k12+2*k23
              mht = j1j12-2*k12+2*k13
              nht = j3j13q-2*k23-2*k13+nq2
              res3 = resus(lht,mht,nht,3)

              !Fourth W(...) function integral
              lht = j2q+2*k12+2*k23
              mht = j3j23-2*k23+2*k13
              nht = j1j12q-2*k12-2*k13+nq2
              res4 = resus(lht,mht,nht,4)

              !Fifth W(...) function integral
              lht = j3q+2*k13+2*k23
              mht = j1j13-2*k13+2*k12
              nht = j2j12q-2*k12-2*k23+nq2
              res5 = resus(lht,mht,nht,5)

              !Sixth W(...) function integral
              lht = j3q+2*k13+2*k23
              mht = j2j23-2*k23+2*k12
              nht = j1j12q-2*k12-2*k13+nq2
              res6 = resus(lht,mht,nht,6)

              Tp = Tp + rcoeff*(res1+res2+res3+res4+res5+res6)
            end do
          end do
        end do
      End Subroutine

      Subroutine AsyExp(nq,j1,j2,j3,j12,j13,j23,biglam,rcterm,resus,asypt,zetacom)
        integer j1,j2,j3,j12,j13,j23
        integer nq,biglam,info,bl,nlam
        integer ipiv(biglam+1),qr,max,jt
      	real*8 rcterm(130,130,130),bot
        real*8 ,dimension(-2:90,-2:90,-2:90,6)::resus
        real*8 pi,Tp,Tmat(biglam+1),A(biglam+1,biglam+1)
        real*8 litlam,asypt,pow
        real*16 zetanq,zetacom(100),check,zeta0
        external dgesv
        litlam = 0.5*(j12+1.d0)+0.5*(j23+1.d0)+0.5*(j13+1.d0)+4.0
        pi = 4.d0*atan(1.d0)
        Tmat = 0.d0
        A = 0.d0
        max = biglam + 1
        nlam = nq - biglam
        jq = nlam
        asypt = 0.d0
        
        !Setting up the left hand side of eq.19
        do ke=1,max
          call Tval(nq,jq,j1,j2,j3,j12,j13,j23,rcterm,resus,Tp)
          Tmat(ke) = Tp
          jq=nlam+ke
        end do

        !Setting up A matrix
        bot = real(nlam,8)
        do jq=1,max
          do iq=1,max
            pow = real(iq,8)+litlam - 1.0
            A(jq,iq) = 1.d0/((bot)**(pow))
          end do
          bot = bot + 1.d0
        end do

        !dgesv solves Ax=B problems. The Tmat array contains the solutions
        !after the routine finishes as long as info=0. If info does not 
        !equal zero, there is an error somewhere.
        call dgesv(max,1,A,max,ipiv,Tmat,max,info)

        asypt = 0.d0
        doulitlam = int(2.0*litlam)
        do j=1,max
          jt = 2*j + doulitlam - 4
          zeta0 = zetacom(jt)
          zetanq = 0.q0
          do i=1,nq
            zetanq = zetanq + 1.q0/(real(i,16)**(real(j,16)+real(litlam,16)-1.q0))
          end do
          check = (zeta0 - zetanq)*real(Tmat(j),16)
          asypt = asypt + check
        end do
        
      End Subroutine

      Subroutine ccoeff(jx,rcterm)
      	real*8 rcterm(130,130,130)
        real*8 rkterm,rkterm2,rterm,binom,facnq
        integer n,nq,nk,nt,nqj,nq2,n2,jx

        do n=0,jx,2
          n2 = int(n/2)
          rnp2 = real(n+2,8)
          np2 = n+2

          do nq=0,n2
            facnq = real(2*nq+1,8)/rnp2
            nq2 = int(n2-nq)
            nqj = int(nq-1)
            do nk=0,nq2
              nk2 = 2*nk+1

              call binomcoeff(np2,nk2,binom)
              rterm=1.0

              if(nq .ne. 0) then
                do nt=0,nqj
                  rkterm = real(2*nk+2*nt-n,8)
                  rkterm2 = real(2*nk+2*nq-2*nt+1,8)
                  rterm = rterm*rkterm/rkterm2
                end do
              end if

              rcterm(n+2,nq+2,nk+2) = rterm*binom*facnq
              rcterm(1,nq+2,2)=1.0
            end do
          end do
        end do

        do n=1,25,2 !Change the 25 to improve accuracy, if needed.
          n2 = 120
          rnp2 = real(n+2,8)
          np2 = n+2
          
          do nq=0,n2
            facnq = real(2*nq+1,8)/rnp2
            nq2 = int((n+1)/2)
            nqj = int(nq-1)

            do nk=0,nq2
              if(nq2 .lt. int(nq-1))then
                nqj = nq2
              end if
              nk2 = 2*nk+1
              call binomcoeff(np2,nk2,binom)
              rterm=1.0

              if (nq .ne. 0) then
                do nt=0,nqj
                  rkterm = real(2*nk+2*nt-n,8)
                  rkterm2 = real(2*nk+2*nq-2*nt+1,8)
                  rterm = rterm*rkterm/rkterm2
                end do
              end if

              rcterm(n+2,nq+2,nk+2) = rterm*binom*facnq
              rcterm(1,nq+2,2) = 1.0
              
            end do
          end do
        end do

      End Subroutine

      Subroutine wfunction(lht,mht,nht,alpha,beta,gamma,res)
      	real*8 hyper(300,3)
        real*8 factl,expterm,exptermb,termb
        real*8 termc,res,zd
        integer lht,mht,nht,ip,ht1,ht2,ht3

        factl = exp(factln(lht))

        nn = 65
        ht1 = 1.0
        ht2 = lht+mht+nht+3+nn
        ht3 = lht+mht+3+nn
        zd = (alpha+beta)/(alpha+beta+gamma)
        call hypergeom(nn,ht1,ht2,ht3,zd,hyper)

        expterm = (alpha+beta+gamma)**(lht+mht+nht+3.0)
        exptermb = alpha/(alpha+beta+gamma)
        termb = 1.0
        res = 0.0
        do ip=0,nn-1
          htb = lht+mht+2+ip

          facta = exp(factln(lht+mht+nht+2+ip))
          factb = exp(factln(lht+1+ip))

          termb = exptermb**ip
          ip1 = lht+mht+nht+3+ip
          termc = hyper(ip1,1)*facta*termb/(factb*htb)

          res = res + factl/expterm*termc
        end do
      End Subroutine

      Subroutine hypergeom(nn,ht1,ht2,ht3,zd,hyper)
      	real*8 f1,f2,f3,ft1,ft2,rj,zd
        real*8 hyper(300,3)
        integer nn,ht1,ht2,ht3

        hyper = 0.0
        coeff = zd*ht2/ht3
        hyper11 = 1.0
        ft2 = 1.0

        do i=1,nn
          rj = real(i-1,8)
          f1 = ht1+rj
          f2 = ht2+rj
          f3 = (ht3+rj)*i

          ft1 = f1*f2*zd/f3
          hyper11 = hyper11+ft1*ft2
          ft2 = ft1*ft2
        end do

        nht2 = int(ht2)
        nht21 = int(ht2)-1
        hyper(nht2,1) = hyper11
        ss = ht3
        st = ht2
        do ik=nht21,1,-1
          ss=ss-1
          if(ss .ne. 0.0) then
            st = st-1
            hyper(ik,1) = st/ss*zd*hyper(ik+1,1)+1.0
          end if
        end do
      End Subroutine

      Function fact(n)
      	integer n
        real*8 fact

        if(n .eq. 0) then
          fact = 1.0
        else
          ns = 1
          do l=1,n
            ns=ns*l
          end do
          fact = real(ns,8)
        end if
        return
      End Function

      Subroutine binomcoeff(j,l,binom)
      	real*8 binom
        integer j,l
        external fact

        binom = exp(factln(j))/(exp(factln(l))*exp(factln(j-l)))

        return
      End Subroutine

      Function factln(n)
      	real*8 a(100)
        data a/100*-1.0/

        if(n .le. 99) then
          if(a(n+1).lt.0.0) then
            a(n+1) = gammaln(n+1.0)
          end if
          factln = a(n+1)
        else
          factln = gammaln(n+1.0)
        end if
        return
      End Function

      Function gamman(ss,nn)
      	real*16 gamm,ss,gamman,gammalnq
        integer nn
        gamm = Exp(gammalnq(ss))
        term1 = 2.q0/((3.q0+Sqrt(8.q0))**real(nn,16))
        term2 = 1.q0/gamm
        term3= 1.q0/ABS(1.q0-2.q0**(1.q0-ss))
        gamman = term1*term2*term3
      End Function

      Function dk(kk,nn)
      	integer kk,nn
        real*16 sum,x1,x2,x3,fac
        real*16 g1,g2,g3,dk,gammalnq
        dk = 0.q0
        sum = 0.q0

        do i =0,kk
          x1 = real(nn+i,16)
          x2 = real(nn-i+1,16)
          x3 = real(2*i+1,16)
          g1 = gammalnq(x1)
          g2 = gammalnq(x2)
          g3 = gammalnq(x3)
          fac = Exp(g1-g2-g3)
          sum = sum + fac*(4.q0**real(i,16))
        end do
        dk = real(nn,16)*sum
      End Function

      Function zetaf(ss,nn,nq)
      	integer nn,nq
      	real*16 ss,fac,dn,dk,sum,sum2
        real*16 zetafull,sumz,gamman,zetaf
        sumz = 0.q0

        dn = dk(nn,nn)
        fac = -1.q0/(dn*(1.q0-2.q0**(1.q0-ss)))

        nnm = nn-1
        do k=0,nnm
          kr = real(k,16)
          sumz = sumz + ((-1.q0)**(kr))*(dk(k,nn)-dn)/((kr+1.q0)**(ss))
        end do

        zetafull = fac*sumz + gamman(ss,nn)
        zetaf = zetafull
      End Function

      Function gammaln(xx)
      	real*8 cof(6),stp,half,one,fpf,x,tmp,ser

        data cof,stp/76.18009173Q0,-86.50532033Q0,24.01409822Q0,-1.231739516Q0,0.120858003Q-02,-0.536382Q-05,2.50662827465Q0/
     	
        data half,one,fpf/0.5Q0,1.0Q0,5.5Q0/

        x = xx-one
        tmp = x+fpf
        tmp = (x+half)*log(tmp)-tmp
        ser=one

        do j=1,6
          x = x+one
          ser=ser+cof(j)/x
        end do
        gammaln = tmp+log(stp*ser)
      End Function

      Function gammalnq(xx)
      	real*16 cof(6),stp,half,one,fpf,x,tmp,ser,xx,gammalnq

        data cof,stp/76.18009173Q0,-86.50532033Q0,24.01409822Q0,-1.231739516Q0,0.120858003Q-02,-0.536382Q-05,2.50662827465Q0/
     	
        data half,one,fpf/0.5Q0,1.0Q0,5.5Q0/

        x = xx-one
        tmp = x+fpf
        tmp = (x+half)*log(tmp)-tmp
        ser=one

        do j=1,6
          x = x+one
          ser=ser+cof(j)/x
        end do
        gammaln = tmp+log(stp*ser)
      End Function

      !
      !jpmatrix creates a matrix of the powers of the interparticle
      !distances that are from the H operation on phi.
      !

      Subroutine jpmatrix(jp)
      	integer jp(58,6),jp2(58,6)
        integer ipp,jpp

        jp=0
        jp2=0

        do ipp=2,7
          jp2(ipp,ipp-1) = -1
          jp2(ipp+6,ipp-1) = -2
        end do

        jp2(14,2) = 1
        jp2(14,4) = -1
        jp2(15,2) = 1
        jp2(15,6) = -1

        jp2(16,4) = 1
        jp2(16,2) = -1
        jp2(17,4) = 1
        jp2(17,6) = -1

        jp2(18,6) = 1
        jp2(18,2) = -1
        jp2(19,6) = 1
        jp2(19,4) = -1

        jp2(20,2) = 1
        jp2(20,1) = -2
        jp2(21,2) = 1
        jp2(21,3) = -2
        jp2(22,2) = 1
        jp2(22,4) = -2
        jp2(23,2) = 1
        jp2(23,6) = -2

        jp2(24,4) = 1
        jp2(24,1) = -2
        jp2(25,4) = 1
        jp2(25,2) = -2
        jp2(26,4) = 1
        jp2(26,5) = -2
        jp2(27,4) = 1
        jp2(27,6) = -2

        jp2(28,6) = 1
        jp2(28,2) = -2
        jp2(29,6) = 1
        jp2(29,3) = -2
        jp2(30,6) = 1
        jp2(30,4) = -2
        jp2(31,6) = 1
        jp2(31,5) = -2

        jp2(32,1) = 2
        jp2(32,3) = -2
        jp2(32,5) = -2
        jp2(33,1) = 2
        jp2(33,2) = -1
        jp2(33,4) = -1
        jp2(34,1) = 2
        jp2(34,2) = -1
        jp2(34,4) = -2
        jp2(35,1) = 2
        jp2(35,2) = -2 
        jp2(35,4) = -1
        jp2(36,1) = 2
        jp2(36,2) = -2
        jp2(36,4) = -2

        jp2(37,2) = 2
        jp2(37,1) = -2
        jp2(37,4) = -2
        jp2(38,2) = 2
        jp2(38,1) = -2
        jp2(38,4) = -1
        jp2(39,2) = 2
        jp2(39,3) = -2
        jp2(39,6) = -2
        jp2(40,2) = 2
        jp2(40,3) = -2
        jp2(40,6) = -1
        
        jp2(41,3) = 2
        jp2(41,1) = -2
        jp2(41,5) = -2
        jp2(42,3) = 2
        jp2(42,2) = -1
        jp2(42,6) = -1
        jp2(43,3) = 2
        jp2(43,2) = -1
        jp2(43,6) = -2
        jp2(44,3) = 2
        jp2(44,2) = -2
        jp2(44,6) = -1
        jp2(45,3) = 2
        jp2(45,2) = -2
        jp2(45,6) = -2

        jp2(46,4) = 2
        jp2(46,1) = -2
        jp2(46,2) = -2
        jp2(47,4) = 2
        jp2(47,1) = -2
        jp2(47,2) = -1
        jp2(48,4) = 2
        jp2(48,5) = -2
        jp2(48,6) = -2
        jp2(49,4) = 2
        jp2(49,5) = -2
        jp2(49,6) = -1
        
        jp2(50,5) = 2
        jp2(50,1) = -2
        jp2(50,3) = -2
        jp2(51,5) = 2
        jp2(51,4) = -1
        jp2(51,6) = -1
        jp2(52,5) = 2
        jp2(52,4) = -1
        jp2(52,6) = -2
        jp2(53,5) = 2
        jp2(53,4) = -2
        jp2(53,6) = -1
        jp2(54,5) = 2
        jp2(54,4) = -2
        jp2(54,6) = -2

        jp2(55,6) = 2
        jp2(55,2) = -2
        jp2(55,3) = -2
        jp2(56,6) = 2
        jp2(56,2) = -1
        jp2(56,3) = -2
        jp2(57,6) = 2
        jp2(57,4) = -2
        jp2(57,5) = -2
        jp2(58,6) = 2
        jp2(58,4) = -1
        jp2(58,5) = -2

        do ipp=1,58
          do jpp=1,6
            jp(ipp,jpp) = jp2(ipp,jpp)
          end do
        end do
        
        write(7,*) "jp matrix"
        write(7,*) 'N ','r12 ','r13 ','r14 ','r23 ','r24 ','r34 '
        do ipp=1,58
          write(7,*) ipp,jp(ipp,1),jp(ipp,2),jp(ipp,3),jp(ipp,4),jp(ipp,5),jp(ipp,6)
        end do

      End Subroutine

      !
      !This subroutine creates the coefficients for the phi H phi matrix elements
      !

      Subroutine powercoeff(alpk,betk,gamk,ii,ij,ik,il,im,in2,nfi,kc,ie,pcoeffl)

     	real*8 pcoeffl(58)
        real*8 alpk(5),betk(5),gamk(5)
        real*8 c13,c23,c34,c13h,c23h,c34h
        integer ii(nfi),ij(nfi),ik(nfi),il(nfi),im(nfi),in2(nfi)
        integer kc,ie

        pcoeff = 0.0
        c13 = alpk(kc)
        c23 = betk(kc)
        c34 = gamk(kc)
        c13h = c13*0.5
        c23h = c23*0.5
        c34h = c34*0.5
        
        pcoeffl(1)=-c13**(2.0)-c23**(2.0)-c34**(2.0)
        pcoeffl(2)=1.0
        pcoeffl(3)=-1.0+2.0*c13*(ij(ie)+1.0)+c13h*(ii(ie)+ik(ie)+il(ie)+in2(ie))
        pcoeffl(4)=-1.0
        pcoeffl(5)=-1.0+2.0*c23*(il(ie)+1.0)+c23h*(ii(ie)+ij(ie)+im(ie)+in2(ie))
        pcoeffl(6)=-1.0
        pcoeffl(7)=1.0+2.0*c34*(in2(ie)+1.0)+c34h*(ij(ie)+ik(ie)+il(ie)+im(ie))
        pcoeffl(8)=-ii(ie)*(ii(ie)+1.0)-0.5*ii(ie)*(ij(ie)+ik(ie)+il(ie)+im(ie))
        pcoeffl(9)=-ij(ie)*(ij(ie)+1.0)-0.5*ij(ie)*(ii(ie)+ik(ie)+il(ie)+in2(ie))
        pcoeffl(10)=-ik(ie)*(ik(ie)+1.0)-0.5*ik(ie)*(ii(ie)+ij(ie)+im(ie)+in2(ie))
        pcoeffl(11)=-il(ie)*(il(ie)+1.0)-0.5*il(ie)*(ii(ie)+ij(ie)+im(ie)+in2(ie))
        pcoeffl(12)=-im(ie)*(im(ie)+1.0)-0.5*im(ie)*(ii(ie)+ik(ie)+il(ie)+in2(ie))
        pcoeffl(13)=-in2(ie)*(in2(ie)+1.0)-0.5*in2(ie)*(ij(ie)+ik(ie)+il(ie)+im(ie))
        pcoeffl(14)=-0.5*c13*c23
        pcoeffl(15)=-0.5*c13*c34
        pcoeffl(16)=-0.5*c13*c23
        pcoeffl(17)=-0.5*c23*c34
        pcoeffl(18)=-0.5*c13*c34
        pcoeffl(19)=-0.5*c23*c34
        pcoeffl(20)=0.5*ii(ie)*c13
        pcoeffl(21)=0.5*ik(ie)*c13
        pcoeffl(22)=0.5*il(ie)*c13
        pcoeffl(23)=0.5*in2(ie)*c13
        pcoeffl(24)=0.5*ii(ie)*c23
        pcoeffl(25)=0.5*ij(ie)*c23
        pcoeffl(26)=0.5*im(ie)*c23
        pcoeffl(27)=0.5*in2(ie)*c23
        pcoeffl(28)=0.5*ij(ie)*c34
        pcoeffl(29)=0.5*ik(ie)*c34
        pcoeffl(30)=0.5*il(ie)*c34
        pcoeffl(31)=0.5*im(ie)*c34
        
        pcoeffl(32)=0.5*ik(ie)*im(ie)
        pcoeffl(33)=0.5*c13*c23
        pcoeffl(34)=-0.5*il(ie)*c13
        pcoeffl(35)=-0.5*ij(ie)*c23
        pcoeffl(36)=0.5*ij(ie)*il(ie)
        
        pcoeffl(37)=0.5*ii(ie)*il(ie)
        pcoeffl(38)=-0.5*ii(ie)*c23
        pcoeffl(39)=0.5*ik(ie)*in2(ie)
        pcoeffl(40)=-0.5*ik(ie)*c34
       
        pcoeffl(41)=0.5*ii(ie)*im(ie)
        pcoeffl(42)=0.5*c13*c34
        pcoeffl(43)=-0.5*in2(ie)*c13
        pcoeffl(44)=-0.5*ij(ie)*c34
        pcoeffl(45)=0.5*ij(ie)*in2(ie)
        
        pcoeffl(46)=0.5*ii(ie)*ij(ie)
        pcoeffl(47)=-0.5*ii(ie)*c13
        pcoeffl(48)=0.5*im(ie)*in2(ie)
        pcoeffl(49)=-0.5*im(ie)*c34
        
        pcoeffl(50)=0.5*ii(ie)*ik(ie)
        pcoeffl(51)=0.5*c23*c34
        pcoeffl(52)=-0.5*in2(ie)*c23
        pcoeffl(53)=-0.5*il(ie)*c34
        pcoeffl(54)=0.5*il(ie)*in2(ie)
        
        pcoeffl(55)=0.5*ij(ie)*ik(ie)
        pcoeffl(56)=-0.5*ik(ie)*c13
        pcoeffl(57)=0.5*il(ie)*im(ie)
        pcoeffl(58)=-0.5*im(ie)*c23
        
      End Subroutine