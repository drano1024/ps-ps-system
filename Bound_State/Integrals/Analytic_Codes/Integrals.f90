Program Integrals
	implicit none
	real*16 fact(0:500),binom(0:500,0:500),hyper(1000)
	real*16 A3,alp,bet,gam,alpd,betd,gamd
	real*16 a1,a2,a32,a1d,a2d,a32d
	real*16, dimension(-2:400,-5:90,-400:90,6):: resus
	real*16, dimension(-2:400,-5:90,-400:90,6):: resusd
	real*16 pi,pifac,pcoeffl(58),phires,phiresd
	real*16 phi1,phid1
  real*16, allocatable :: phiphi(:,:,:),phihphi(:,:,:)
	real*16 rcterm(130,130,130)
	real*8 start,finish,time,diff,seconds,minutes
	integer i,j,nq,mega,nt
	integer, allocatable :: ii(:),ij(:),ik(:),il(:),im(:),in2(:)
	integer kh,lh,mh,kmax,lmax,mmax,jp(58,6),ie,je,lk
	integer j12e,j13e,j14e,j23e,j24e,j34e
	integer j12,j13,j14,j23,j24,j34,trash
	integer j12d,j13d,j14d,j23d,j24d,j34d
	character(len=1024) :: directfile,P12file
	call cpu_time(start)
	call facterms(fact)
	call binomalcoeff(binom)
	pi = 4.q0*atan(1.q0)
	resus = 0.q0
	resusd = 0.q0
	
	open(unit=1,file='fort.95',status='old')
	read(1,*) trash
	read(1,*) alp
	read(1,*) bet
	read(1,*) gam
	read(1,*) nq
	read(1,*) mega

  call get_nt(mega,nt)
  allocate(ii(nt),ij(nt),ik(nt),il(nt),im(nt),in2(nt))
  allocate(phiphi(nt,nt,2),phihphi(nt,nt,2))
  ii = 0
  ij = 0
  ik = 0
  il = 0
  im = 0
  in2 = 0
  phiphi = 0.q0
  phihphi = 0.q0
	
	open(unit=4,file='const',status='unknown')
	open(unit=8,file='outonedrake',status='unknown')
	write(directfile,"(A9,I1)") "ssDirect,",mega
	write(P12file,"(A6,I1)") "ssP12,",mega
	open(unit=12,file=directfile,status='unknown')
	open(unit=13,file=P12file,status='unknown')
	call const(ii,ij,ik,il,im,in2,mega,nt)
	write(8,*) 'N=',nt
	write(12,*) mega
	write(13,*) mega
	write(12,*) nt
	write(13,*) nt
	write(8,*) ""
	
	!Direct terms
	a1 = alp+alp
	a2 = bet+bet
	a32 = gam+gam
	
	!P12 Exchange terms
	a1d = alp+bet
	a2d = bet+alp
	a32d = gam+gam
	
	print*, "Starting A3 Function Calculations."
	kmax = 2*mega+2*nq
	lmax = 2*mega
	mmax = -2*mega-2*nq
	
	do kh=0,kmax
		do lh=-3,lmax
			do mh=int(mmax/2.0),mmax,-1
				
				if(((kh+lh+mh+2).ge.0).and.((kh+lh+1).ge.0).and.(kh.ge.0))then
					!Direct
					resus(kh,lh,mh,1) = A3(kh,lh,mh,a1,a2,a32,fact,binom)
					resus(kh,lh,mh,2) = A3(kh,lh,mh,a1,a32,a2,fact,binom)
					resus(kh,lh,mh,3) = A3(kh,lh,mh,a2,a1,a32,fact,binom)
					resus(kh,lh,mh,4) = A3(kh,lh,mh,a2,a32,a1,fact,binom)
					resus(kh,lh,mh,5) = A3(kh,lh,mh,a32,a1,a2,fact,binom)
					resus(kh,lh,mh,6) = A3(kh,lh,mh,a32,a2,a1,fact,binom)
					
					!P12 Exchange
					resusd(kh,lh,mh,1) = A3(kh,lh,mh,a1d,a2d,a32d,fact,binom)
					resusd(kh,lh,mh,2) = A3(kh,lh,mh,a1d,a32d,a2d,fact,binom)
					resusd(kh,lh,mh,3) = A3(kh,lh,mh,a2d,a1d,a32d,fact,binom)
					resusd(kh,lh,mh,4) = A3(kh,lh,mh,a2d,a32d,a1d,fact,binom)
					resusd(kh,lh,mh,5) = A3(kh,lh,mh,a32d,a1d,a2d,fact,binom)
					resusd(kh,lh,mh,6) = A3(kh,lh,mh,a32d,a2d,a1d,fact,binom)
				end if
			end do
		end do
	end do
	
	print *, "Finished A3 Function Calculations."
	call jpmatrix(jp)
	call ccoeff(100,rcterm,binom)
	pifac = 0.25d0/pi
	print*, "Starting Integral Calculations."
	
	do ie=1,nt
		call powercoeff(alp,bet,gam,ii,ij,ik,il,im,in2,ie,pcoeffl,nt)
		do je=1,nt
			
			j12e = ii(ie)+ii(je)
			j13e = ij(ie)+ij(je)
			j14e = ik(ie)+ik(je)
			j23e = il(ie)+il(je)
			j24e = im(ie)+im(je)
			j34e = in2(ie)+in2(je)
			
			do lk=1,58
				j12 = j12e+jp(lk,1)
				j13 = j13e+jp(lk,2)
				j14 = j14e+jp(lk,3)
				j23 = j23e+jp(lk,4)
				j24 = j24e+jp(lk,5)
				j34 = j34e+jp(lk,6)
				
				call integration(nq,j13,j23,j34,j12,j14,j24,resus,a1,a2,a32,phires,rcterm,fact,binom)
				
				phiresd = 0.q0
				!P12 Exchange
				j12d = ii(ie)+ii(je)+jp(lk,1)
				j13d = ij(ie)+il(je)+jp(lk,2)
				j14d = ik(ie)+im(je)+jp(lk,3)
				j23d = il(ie)+ij(je)+jp(lk,4)
				j24d = im(ie)+ik(je)+jp(lk,5)
				j34d = in2(ie)+in2(je)+jp(lk,6)

				call integration(nq,j13d,j23d,j34d,j12d,j14d,j24d,resusd,a1d,a2d,a32d,phiresd,rcterm,fact,binom)
				
				phi1 = phires*pifac
				phid1 = phiresd*pifac
				
				phihphi(ie,je,1) = phihphi(ie,je,1)+phi1*pcoeffl(lk)
				phihphi(ie,je,2) = phihphi(ie,je,2)+phid1*pcoeffl(lk)
				if(lk.eq.1) then
					phiphi(ie,je,1) = phi1
					phiphi(ie,je,2) = phid1
				end if
			end do
		end do
	end do
	
	print*, ""
	print*, "Finished Integral Calculations."
	
	do i=1,nt
		do j=1,nt
			write(12,"(2I5,2F60.30,2X)") i,j,phiphi(i,j,1),phihphi(i,j,1)
			write(13,"(2I5,2F60.30,2X)") i,j,phiphi(i,j,2),phihphi(i,j,2)
		end do
	end do
	
	call cpu_time(finish)
	diff = int(finish-start)
	seconds = int(mod(diff,60.0))
	minutes = int((diff-seconds)/60.0)
	time = finish - start
	print*, "Time:",time,"Seconds"
	write(8,*) "Time:",time,"Seconds"
End Program

Subroutine get_nt(mega,nt) ! Obtained from Peter Van Reeth
	implicit none
	integer nt,mega,inx
	integer megap,i,i12,i13,i14,i23,i24,i34
	integer i12p1,i13p1,i14p1,i23p1,i24p1,i34p1
	integer i14p1m,i13p1m,i24p1m,i23p1m
	megap = mega + 1
	inx = 0

	!First 126 Terms
	do i = 1,megap
		do i12p1 = 1,i,1
			i12 = i12p1 - 1
			i14p1m = i - i12
			do i14p1 = 1,i14p1m
				i14 = i14p1 - 1
				i13p1m = i14p1m - i14
				do i13p1 = 1,i13p1m
					i13 = i13p1 - 1
					i24p1m = i13p1m - i13
					do i24p1 = 1,i24p1m
						i24 = i24p1 - 1
						i23p1m = i24p1m - i24
						do i23p1 = 1,i23p1m
							i23 = i23p1 - 1
							i34 = i23p1m - i23p1
							inx = inx + 1
						end do
					end do
				end do
			end do
		end do
	end do
        
	nt = inx
End Subroutine

Subroutine const(ii,ij,ik,il,im,in2,mega,nt) ! Obtained from Peter Van Reeth
	implicit none
	integer nt,mega,inx
	integer ii(nt),ij(nt),ik(nt),il(nt),im(nt),in2(nt)
	integer megap,i,j,i12,i13,i14,i23,i24,i34
	integer i12p1,i13p1,i14p1,i23p1,i24p1,i34p1
	integer i14p1m,i13p1m,i24p1m,i23p1m
	megap = mega + 1
	inx = 0

	!First 126 Terms
	do i = 1,megap
		do i12p1 = 1,i,1
			i12 = i12p1 - 1
			i14p1m = i - i12
			do i14p1 = 1,i14p1m
				i14 = i14p1 - 1
				i13p1m = i14p1m - i14
				do i13p1 = 1,i13p1m
					i13 = i13p1 - 1
					i24p1m = i13p1m - i13
					do i24p1 = 1,i24p1m
						i24 = i24p1 - 1
						i23p1m = i24p1m - i24
						do i23p1 = 1,i23p1m
							i23 = i23p1 - 1
							i34 = i23p1m - i23p1
							
							!if((i13.ge.i23).and.(i14.ge.i24)) then
								inx = inx + 1
								ii(inx) = i12
								ij(inx) = i13
								ik(inx) = i14
								il(inx) = i23
								im(inx) = i24
								in2(inx) = i34
							!end if
						end do
					end do
				end do
			end do
		end do
		write(4,*) (i-1),inx
		write(8,*) (i-1),inx
	end do
        
	nt = inx
	write(4,*) ' N    r12    r13    r14    r23    r24    r34'
	write(4,"(7I7)")(j,ii(j),ij(j),ik(j),il(j),im(j),in2(j),j=1,nt)
End Subroutine

Subroutine integration(nq,j1,j2,j3,j12,j13,j23,resus,a1,a2,a32,phi,rcterm,fact,binom)
	implicit none
	real*16, dimension(-2:400,-5:90,-400:90,6)::resus
	real*16 a1,a2,a32,phi,pi,piterm,rqt,rcterm(130,130,130),rcoeff,A3
	real*16 res1,res2,res3,res4,res5,res6,fact(0:500),binom(0:500,0:500)
	integer nq,j1,j2,j3,j12,j13,j23,jq
	integer L12,L13,L23,M12,M13,M23,nqnew
	integer j1j12,j1j13,j2j12,j2j23,j3j13,j3j23
	integer j1q,j2q,j3q,j1j12q,j2j12q,j3j13q
	integer kht,lht,mht,k12,k13,k23
	
	phi = 0.q0
	pi = 4.q0*atan(1.q0)
	piterm = (4.q0*pi)**3.q0
	
	L12 = int((j12+1)/2)
	L13 = int((j13+1)/2)
	L23 = int((j23+1)/2)
	if(mod(j12,2).eq.0) then
		M12 = int(j12/2)
	else
		M12 = nq
	end if
	
	if(mod(j13,2).eq.0) then
		M13 = int(j13/2)
	else
		M13 = nq
	end if
	
	if(mod(j23,2).eq.0) then
		M23 = int(j23/2)
	else
		M23 = nq
	end if
	nqnew = min(M12,M13,M23)
	
	j1j12 = j1+2+j12
	j1j13 = j1+2+j13
	j2j12 = j2+2+j12
	j2j23 = j2+2+j23
	j3j13 = j3+2+j13
	j3j23 = j3+2+j23
	
	do jq=0,nqnew
		j1q = j1+2+2*jq
		j2q = j2+2+2*jq
		j3q = j3+2+2*jq
		j1j12q = j1j12+j13-2*jq
		j2j12q = j2j12+j23-2*jq
		j3j13q = j3j13+j23-2*jq
		
		rqt = piterm/((2.q0*jq+1.q0)**2.q0)
		
		do k12=0,L12
			do k13=0,L13
				do k23=0,L23
					rcoeff = rqt*rcterm(j12+2,jq+2,k12+2)*rcterm(j13+2,jq+2,k13+2)*rcterm(j23+2,jq+2,k23+2)
					
					!First A3 Function
					kht = j1q+2*k12+2*k13
					lht = j2j12-2*k12+2*k23
					mht = j3j13q-2*k23-2*k13
					res1 = resus(kht,lht,mht,1)
					if(res1.eq.0.0) then
						res1 = A3(kht,lht,mht,a1,a2,a32,fact,binom)
						resus(kht,lht,mht,1) = res1
					end if
					
					!Second W(...) function integral
					kht = j1q+2*k12+2*k13
					lht = j3j13-2*k13+2*k23
					mht = j2j12q-2*k23-2*k12
					res2 = resus(kht,lht,mht,2)
					if(res2.eq.0.0) then
						res2 = A3(kht,lht,mht,a1,a32,a2,fact,binom)
						resus(kht,lht,mht,2) = res2
					end if
					
					!Third W(...) function integral
					kht = j2q+2*k12+2*k23
					lht = j1j12-2*k12+2*k13
					mht = j3j13q-2*k23-2*k13
					res3 = resus(kht,lht,mht,3)
					if(res3.eq.0.0) then
						res3 = A3(kht,lht,mht,a2,a1,a32,fact,binom)
						resus(kht,lht,mht,3) = res3
					end if
					
					!Fourth W(...) function integral
					kht = j2q+2*k12+2*k23
					lht = j3j23-2*k23+2*k13
					mht = j1j12q-2*k12-2*k13
					res4 = resus(kht,lht,mht,4)
					if(res4.eq.0.0) then
						res4 = A3(kht,lht,mht,a2,a32,a1,fact,binom)
						resus(kht,lht,mht,4) = res4
					end if
					
					!Fifth W(...) function integral
					kht = j3q+2*k13+2*k23
					lht = j1j13-2*k13+2*k12
					mht = j2j12q-2*k12-2*k23
					res5 = resus(kht,lht,mht,5)
					if(res5.eq.0.0) then
						res5 = A3(kht,lht,mht,a32,a1,a2,fact,binom)
						resus(kht,lht,mht,5) = res5
					end if
					
					!Sixth W(...) function integral
					kht = j3q+2*k13+2*k23
					lht = j2j23-2*k23+2*k12
					mht = j1j12q-2*k12-2*k13
					res6 = resus(kht,lht,mht,6)
					if(res6.eq.0.0) then
						res6 = A3(kht,lht,mht,a32,a2,a1,fact,binom)
						resus(kht,lht,mht,6) = res6
					end if
					
					phi = phi + rcoeff*(res1+res2+res3+res4+res5+res6)
				end do
			end do
		end do
	end do
End Subroutine

Subroutine hypergeom(a,b,c,zd,hyper,fact)
	implicit none
	real*16 fact(0:500),coeff,ss,st
	real*16 f1,f2,f3,ft1,ft2,rj,zd
	real*16 hyper(1000),hyper11
	integer a,b,c,i,ik,nb,nb1

	coeff = zd*b/c
	hyper11 = 1.0q0
	ft2 = 1.0q0
	
	do i=1,500
		rj = real(i-1,16)
		f1 = a+rj
		f2 = b+rj
		f3 = (c+rj)*i

		ft1 = f1*f2*zd/f3
		hyper11 = hyper11+ft1*ft2
		ft2 = ft1*ft2
	end do
	
	nb = int(b)
	nb1 = int(b)-1
	hyper(nb) = hyper11
	ss = c
	st = b
	do ik=nb1,1,-1
		ss=ss-1
		if(ss .ne. 0.q0) then
			st = st-1
			hyper(ik) = st*zd*hyper(ik+1)/ss+1.q0
		end if
	end do
End Subroutine

Subroutine ccoeff(jx,rcterm,binom)
	implicit none
	real*16 binom(0:500,0:500)
	real*16 rcterm(130,130,130)
	real*16 rkterm,rkterm2,rterm,facnq,rnp2
	integer n,nq,nk,nt,nqj,nq2,n2,jx,nk2,np2

	do n=0,jx,2
		n2 = int(n/2)
		rnp2 = real(n+2,16)
		np2 = n+2

		do nq=0,n2
			facnq = real(2*nq+1,16)/rnp2
			nq2 = int(n2-nq)
			nqj = int(nq-1)
				do nk=0,nq2
					nk2 = 2*nk+1
					rterm=1.q0

					if(nq .ne. 0) then
						do nt=0,nqj
							rkterm = real(2*nk+2*nt-n,16)
							rkterm2 = real(2*nk+2*nq-2*nt+1,16)
							rterm = rterm*rkterm/rkterm2
						end do
					end if

					rcterm(n+2,nq+2,nk+2) = rterm*binom(np2,nk2)*facnq
					rcterm(1,nq+2,2)=1.q0
				end do
			end do
		end do

	do n=1,jx,2 !Change the 25 to improve accuracy, if needed.
		n2 = 120
		rnp2 = real(n+2,16)
		np2 = n+2

		do nq=0,n2
			facnq = real(2*nq+1,16)/rnp2
			nq2 = int((n+1)/2)
			nqj = int(nq-1)

			do nk=0,nq2
				if(nq2 .lt. int(nq-1))then
					nqj = nq2
				end if
				nk2 = 2*nk+1
				rterm=1.q0

				if (nq .ne. 0) then
					do nt=0,nqj
						rkterm = real(2*nk+2*nt-n,16)
						rkterm2 = real(2*nk+2*nq-2*nt+1,16)
						rterm = rterm*rkterm/rkterm2
					end do
				end if

				rcterm(n+2,nq+2,nk+2) = rterm*binom(np2,nk2)*facnq
				rcterm(1,nq+2,2) = 1.q0

			end do
		end do
	end do
End Subroutine

Function A3(k,l,m,a,b,c,fact,binom)
	implicit none
	real*16 fact(0:500),binom(0:500,0:500)
	real*16 a,b,c,A3,A1,mfac,lfac
	real*16 hyper(1000),zd,factfac,abcfac
	integer k,l,m,il,im,ilm
	integer nn,ht1,ht2,ht3,n,nh
	
	Select Case(m)
		Case ( :-1) !less than zero
			A3 = 0.q0
			nn=150
			ht1 = 1
			ht2 = k+l+m+3+nn
			ht3 = k+l+3+nn
			zd = (a+b)/(a+b+c)
			call hypergeom(ht1,ht2,ht3,zd,hyper,fact)
			do n=0,nn
				factfac = fact(k)*fact(k+l+m+n+2)/(fact(k+n+1)*fact(k+l+m+2))
				abcfac = (a/(a+b+c))**real(n,16)
				nh = k+l+m+3+n
				A3 = A3+factfac*abcfac*hyper(nh)/real(k+l+n+2,16)
			end do
			A3 = A3*A1(k+l+m+2,a+b+c,fact)
		Case(0: )!greater than zero
			A3 = 0.q0
			do im=0,m
				mfac = binom(m,im)*A1(im,c,fact)
				ilm = l+m-im
				do il=0,ilm
					lfac = binom(ilm,il)*A1(il,b+c,fact)
					A3 = A3+mfac*lfac*A1(k+l-il+m-im,a+b+c,fact)
				end do
			end do
	End Select
End Function

Function A1(k,a,fact)
	implicit none
	real*16 a,A1
	real*16 fact(0:500)
	integer k
	A1 = 0.q0
	
	A1 = fact(k)/a**(real(k,16)+1.q0)
End Function

Subroutine jpmatrix(jp)
	implicit none
	integer jp(58,6),jp2(58,6)
	integer ipp,jpp

	jp=0
	jp2=0

	do ipp=2,7
		jp2(ipp,ipp-1) = -1
		jp2(ipp+6,ipp-1) = -2
	end do

	jp2(14,2) = 1
	jp2(14,4) = -1
	jp2(15,2) = 1
	jp2(15,6) = -1

	jp2(16,4) = 1
	jp2(16,2) = -1
	jp2(17,4) = 1
	jp2(17,6) = -1

	jp2(18,6) = 1
	jp2(18,2) = -1
	jp2(19,6) = 1
	jp2(19,4) = -1

	jp2(20,2) = 1
	jp2(20,1) = -2
	jp2(21,2) = 1
	jp2(21,3) = -2
	jp2(22,2) = 1
	jp2(22,4) = -2
	jp2(23,2) = 1
	jp2(23,6) = -2

	jp2(24,4) = 1
	jp2(24,1) = -2
	jp2(25,4) = 1
	jp2(25,2) = -2
	jp2(26,4) = 1
	jp2(26,5) = -2
	jp2(27,4) = 1
	jp2(27,6) = -2

	jp2(28,6) = 1
	jp2(28,2) = -2
	jp2(29,6) = 1
	jp2(29,3) = -2
	jp2(30,6) = 1
	jp2(30,4) = -2
	jp2(31,6) = 1
	jp2(31,5) = -2

	jp2(32,1) = 2
	jp2(32,3) = -2
	jp2(32,5) = -2
	jp2(33,1) = 2
	jp2(33,2) = -1
	jp2(33,4) = -1
	jp2(34,1) = 2
	jp2(34,2) = -1
	jp2(34,4) = -2
	jp2(35,1) = 2
	jp2(35,2) = -2 
	jp2(35,4) = -1
	jp2(36,1) = 2
	jp2(36,2) = -2
	jp2(36,4) = -2

	jp2(37,2) = 2
	jp2(37,1) = -2
	jp2(37,4) = -2
	jp2(38,2) = 2
	jp2(38,1) = -2
	jp2(38,4) = -1
	jp2(39,2) = 2
	jp2(39,3) = -2
	jp2(39,6) = -2
	jp2(40,2) = 2
	jp2(40,3) = -2
	jp2(40,6) = -1
	
	jp2(41,3) = 2
	jp2(41,1) = -2
	jp2(41,5) = -2
	jp2(42,3) = 2
	jp2(42,2) = -1
	jp2(42,6) = -1
	jp2(43,3) = 2
	jp2(43,2) = -1
	jp2(43,6) = -2
	jp2(44,3) = 2
	jp2(44,2) = -2
	jp2(44,6) = -1
	jp2(45,3) = 2
	jp2(45,2) = -2
	jp2(45,6) = -2

	jp2(46,4) = 2
	jp2(46,1) = -2
	jp2(46,2) = -2
	jp2(47,4) = 2
	jp2(47,1) = -2
	jp2(47,2) = -1
	jp2(48,4) = 2
	jp2(48,5) = -2
	jp2(48,6) = -2
	jp2(49,4) = 2
	jp2(49,5) = -2
	jp2(49,6) = -1
	
	jp2(50,5) = 2
	jp2(50,1) = -2
	jp2(50,3) = -2
	jp2(51,5) = 2
	jp2(51,4) = -1
	jp2(51,6) = -1
	jp2(52,5) = 2
	jp2(52,4) = -1
	jp2(52,6) = -2
	jp2(53,5) = 2
	jp2(53,4) = -2
	jp2(53,6) = -1
	jp2(54,5) = 2
	jp2(54,4) = -2
	jp2(54,6) = -2

	jp2(55,6) = 2
	jp2(55,2) = -2
	jp2(55,3) = -2
	jp2(56,6) = 2
	jp2(56,2) = -1
	jp2(56,3) = -2
	jp2(57,6) = 2
	jp2(57,4) = -2
	jp2(57,5) = -2
	jp2(58,6) = 2
	jp2(58,4) = -1
	jp2(58,5) = -2

	do ipp=1,58
		do jpp=1,6
			jp(ipp,jpp) = jp2(ipp,jpp)
		end do
	end do
End Subroutine

Subroutine powercoeff(alp,bet,gam,ii,ij,ik,il,im,in2,ie,pcoeffl,nt)
	implicit none
  integer nt
	real*16 pcoeffl(58)
	real*16 alp,bet,gam
	real*16 c13,c23,c34,c13h,c23h,c34h
	integer ii(nt),ij(nt),ik(nt),il(nt),im(nt),in2(nt)
	integer ie,i

	pcoeffl = 0.q0
	c13 = alp
	c23 = bet
	c34 = gam
	c13h = c13*0.5q0
	c23h = c23*0.5q0
	c34h = c34*0.5q0
	
	pcoeffl(1)=-c13**(2.q0)-c23**(2.q0)-c34**(2.q0)
	pcoeffl(2)=1.q0
	pcoeffl(3)=-1.q0+2.q0*c13*(ij(ie)+1.q0)+c13h*(ii(ie)+ik(ie)+il(ie)+in2(ie))
	pcoeffl(4)=-1.q0
	pcoeffl(5)=-1.q0+2.q0*c23*(il(ie)+1.q0)+c23h*(ii(ie)+ij(ie)+im(ie)+in2(ie))
	pcoeffl(6)=-1.q0
	pcoeffl(7)=1.q0+2.q0*c34*(in2(ie)+1.q0)+c34h*(ij(ie)+ik(ie)+il(ie)+im(ie))
	pcoeffl(8)=-ii(ie)*(ii(ie)+1.q0)-0.5q0*ii(ie)*(ij(ie)+ik(ie)+il(ie)+im(ie))
	pcoeffl(9)=-ij(ie)*(ij(ie)+1.q0)-0.5q0*ij(ie)*(ii(ie)+ik(ie)+il(ie)+in2(ie))
	pcoeffl(10)=-ik(ie)*(ik(ie)+1.q0)-0.5q0*ik(ie)*(ii(ie)+ij(ie)+im(ie)+in2(ie))
	pcoeffl(11)=-il(ie)*(il(ie)+1.q0)-0.5q0*il(ie)*(ii(ie)+ij(ie)+im(ie)+in2(ie))
	pcoeffl(12)=-im(ie)*(im(ie)+1.q0)-0.5q0*im(ie)*(ii(ie)+ik(ie)+il(ie)+in2(ie))
	pcoeffl(13)=-in2(ie)*(in2(ie)+1.q0)-0.5q0*in2(ie)*(ij(ie)+ik(ie)+il(ie)+im(ie))
	pcoeffl(14)=-0.5q0*c13*c23
	pcoeffl(15)=-0.5q0*c13*c34
	pcoeffl(16)=-0.5q0*c13*c23
	pcoeffl(17)=-0.5q0*c23*c34
	pcoeffl(18)=-0.5q0*c13*c34
	pcoeffl(19)=-0.5q0*c23*c34
	pcoeffl(20)=0.5q0*ii(ie)*c13
	pcoeffl(21)=0.5q0*ik(ie)*c13
	pcoeffl(22)=0.5q0*il(ie)*c13
	pcoeffl(23)=0.5q0*in2(ie)*c13
	pcoeffl(24)=0.5q0*ii(ie)*c23
	pcoeffl(25)=0.5q0*ij(ie)*c23
	pcoeffl(26)=0.5q0*im(ie)*c23
	pcoeffl(27)=0.5q0*in2(ie)*c23
	pcoeffl(28)=0.5q0*ij(ie)*c34
	pcoeffl(29)=0.5q0*ik(ie)*c34
	pcoeffl(30)=0.5q0*il(ie)*c34
	pcoeffl(31)=0.5q0*im(ie)*c34
	pcoeffl(32)=0.5q0*ik(ie)*im(ie)
	pcoeffl(33)=0.5q0*c13*c23
	pcoeffl(34)=-0.5q0*il(ie)*c13
	pcoeffl(35)=-0.5q0*ij(ie)*c23
	pcoeffl(36)=0.5q0*ij(ie)*il(ie)
	pcoeffl(37)=0.5q0*ii(ie)*il(ie)
	pcoeffl(38)=-0.5q0*ii(ie)*c23
	pcoeffl(39)=0.5q0*ik(ie)*in2(ie)
	pcoeffl(40)=-0.5q0*ik(ie)*c34
	pcoeffl(41)=0.5q0*ii(ie)*im(ie)
	pcoeffl(42)=0.5q0*c13*c34
	pcoeffl(43)=-0.5q0*in2(ie)*c13
	pcoeffl(44)=-0.5q0*ij(ie)*c34
	pcoeffl(45)=0.5q0*ij(ie)*in2(ie)
	pcoeffl(46)=0.5q0*ii(ie)*ij(ie)
	pcoeffl(47)=-0.5q0*ii(ie)*c13
	pcoeffl(48)=0.5q0*im(ie)*in2(ie)
	pcoeffl(49)=-0.5q0*im(ie)*c34
	pcoeffl(50)=0.5q0*ii(ie)*ik(ie)
	pcoeffl(51)=0.5q0*c23*c34
	pcoeffl(52)=-0.5q0*in2(ie)*c23
	pcoeffl(53)=-0.5q0*il(ie)*c34
	pcoeffl(54)=0.5q0*il(ie)*in2(ie)
	pcoeffl(55)=0.5q0*ij(ie)*ik(ie)
	pcoeffl(56)=-0.5q0*ik(ie)*c13
	pcoeffl(57)=0.5q0*il(ie)*im(ie)
	pcoeffl(58)=-0.5q0*im(ie)*c23
	do i=1,58
		if(pcoeffl(i).ne.0.d0) then
			print *, ie,i,pcoeffl(i)
		end if
	end do
End Subroutine

Subroutine facterms(fact)
	implicit none
	real*16 fact(0:500)
	integer i
	do i=0,498
		if(i.eq.0) then
			fact(i) = 1.q0
		else
			fact(i) = fact(i-1)*real(i,16)
		end if
	end do
End Subroutine

Subroutine binomalcoeff(binom)
	implicit none
	real*16 binom(0:500,0:500)
	integer count,n,k
	do n=0,498
		count = 0
		do k=0,n
			if((k.eq.0).or.(k.eq.n)) then
				binom(n,k) = 1.q0
			else
				binom(n,k) = binom(n-1,count)+binom(n-1,count+1)
				count = count+1
			end if
		end do
	end do
End Subroutine