Program PsSINGLETnew
	implicit none
	integer, parameter :: nfi=60
	integer, parameter :: nfi2=20
	real*16 fact(0:500),pi
	real*8 start,finish,time
	real*8 pinv,qtpinv,denom
	real*8 alp1,alp2,alp0,phiphiint
	real*8 ci(nfi),avgval(3),citmp,ci_new(nfi)
	integer j0(nfi),j1(nfi),j2(nfi),j0e,j1e,j2e,j0p,j1p,j2p
	integer nt,mega,jp(3,3),ie,je,lk,num,numo,i,cii,cij
	integer qr,qtp,diff,seconds,minutes,order(nfi)
	character(len=1024) :: directfile,cifile,orderfile
	logical runi,runj
	pi = 4.d0*ATAN(1.d0)
	pinv = 1.d0/pi
	call cpu_time(start)
	call facterms(fact)

	open(unit=1,file='fort.new',status='old')
	open(unit=5,file='constnew',status='unknown') !linear powers for phi_i
	open(unit=12,file='outonenew',status='unknown') !General information output
	open(unit=7,file='jpmatrixnew',status='unknown') !Powers that get added to phiphj b/c of Hphi action
	read(1,*) mega
	call const(j0,j1,j2,mega,nt,nfi)
	write(12,*) 'N =',nt
	read(1,*) alp1
	read(1,*) alp2
	read(1,*) alp0
	write(directfile,"(A7,I1)") "avgval,",mega
	open(unit=10,file=directfile,status='unknown')
	write(cifile,"(A3,I1)") "ci,",mega
	open(unit=11,file=cifile,status='old')
	write(orderfile,"(A6,I1)") "order,",mega
	open(unit=13,file=orderfile,status='old')
	write(10,*) mega
	write(10,*) nt

	qtpinv = 1.d0/real(qtp,8)
	denom = 0.d0
	avgval = 0.d0
	print *, 'Omega:',mega

	read(11,*) num
	ci = 0.d0
	ci_new = 0.d0
	do ie=1,num
		read(11,*) ci(ie)
		!print *, ci(ie)
	end do

	read(13,*) numo
	do ie=1,numo
		read(13,*) order(ie)
	end do

	do ie = 1,num
		ci_new(order(ie)) = ci(ie)
	end do
	
	!PhiPhi and <T> Integral Using Equation
	call jpmatrix(jp)
	cii = 1
	do ie=1,nt
		cij = 1
		do je=1,nt
			runi = .false.
			runj = .false.
			do i=1,numo
				if (ie.eq.order(i)) then
					runi = .true.
				end if
				if (je.eq.order(i)) then
					runj = .true.
				end if
			end do
			if (runi.and.runj) then
				j0e = j0(ie)+j0(je)
				j1e = j1(ie)+j1(je)
				j2e = j2(ie)+j2(je)
				citmp = ci_new(cii)*ci_new(cij)
			
				denom = denom + citmp*phiphiint(j0e,j1e,j2e,alp0,alp1,alp2,fact)
				do lk=1,3
					j0p = j0e+jp(lk,1)
					j1p = j1e+jp(lk,2)
					j2p = j2e+jp(lk,3)
					avgval(lk) = avgval(lk)+citmp*phiphiint(j0p,j1p,j2p,alp0,alp1,alp2,fact)
				end do
			end if
			cij = cij +1
		end do
		cii = cii + 1
	end do

	print *, 'R0 avgval:', avgval(1)/denom
	print *, 'R1 avgval:', avgval(2)/denom
	print *, 'R2 avgval:', avgval(3)/denom

	call cpu_time(finish)
	diff = int((finish-start))
	seconds = int(mod(diff,60))
	minutes = int((diff-seconds)/60.0)
	time = finish - start
	write(12,*) 'Time:',time,'Seconds'
	write(12,*) 'Total Time:',minutes,'Minutes and',seconds,'Seconds'
	print *, "Time:",time,"Seconds"

End Program PsSINGLETnew

Function phiphiint(j0,j1,j2,alp0,alp1,alp2,fact)
	real*16 fact(0:500)
	real*8 alp0,alp1,alp2,pi,denom,phiphiint
	integer j0,j1,j2
	pi = 4.d0*ATAN(1.d0)
	
	denom = (2.d0**real(j0+j1+j2+3,16))*(alp0**(j0+3))*(alp1**(j1+3))*(alp2**(j2+3))
	phiphiint = (pi**3.d0)*fact(j0+2)*fact(j1+2)*fact(j2+2)/denom/(4.d0*pi)
End Function

      !
      ! This subroutine creates the constants a1,a2,a3,a4,a5, and a6
      ! for the exponential terms.
      !

Subroutine const(a1,a2,a3,mega,nt,nfi) ! Obtained from Peter Van Reeth
	implicit none
	integer nfi
	integer a1(nfi),a2(nfi),a3(nfi),a4(nfi),a5(nfi),a6(nfi) !exponential constants!
	integer nt,mega,inx,nt2
	integer megap,i,i12,i13,i14,i23,i24,i34
	integer i12p1,i13p1,i14p1,i23p1,i24p1,i34p1
	integer i14p1m,i13p1m,i24p1m,i23p1m,j
	logical track

	megap = mega + 1
	inx = 0

	do i = 1,megap
		do i12p1 = 1,i,1
			i12 = i12p1 - 1
			i13p1m = i - i12
			do i13p1 = 1,i13p1m
				i13 = i13p1 - 1
				i14 = i13p1m - i13p1
							
				inx = inx + 1
				a1(inx) = i12
				a2(inx) = i13
				a3(inx) = i14
			end do
		end do
		write(5,*) i,inx
	end do
	nt = inx
	write(5,*) ' N    R0    R1    R2'
	write(5,"(7I7)") (j,a1(j),a2(j),a3(j),j=1,nt)
End Subroutine

Subroutine facterms(fact)
	implicit none
	real*16 fact(0:500)
	integer i
	do i=0,498
		if(i.eq.0) then
			fact(i) = 1.q0
		else
			fact(i) = fact(i-1)*real(i,16)
		end if
	end do
End Subroutine

      !
      !jpmatrix creates a matrix of the powers of the interparticle
      !distances that are from the H operation on phi.
      !

Subroutine jpmatrix(jp)
	implicit none
	integer jp(3,3),jp2(3,3)
	integer ipp,jpp

	jp=0
	jp2=0
	
	jp2(1,1) = 1
	jp2(2,2) = 1
	jp2(3,3) = 1

	do ipp=1,3
		do jpp=1,3
			jp(ipp,jpp) = jp2(ipp,jpp)
		end do
	end do
	
	write(7,*) "jp matrix"
	write(7,*) 'N ','R0 ','R1 ','R2 '
	do ipp=1,3
		write(7,*) ipp,jp(ipp,1),jp(ipp,2),jp(ipp,3)
	end do
End Subroutine