import sys
import numpy as np
from math import factorial

def ppint(j0,j1,j2,alp0,alp1,alp2):
    denom = ((2**(j0+j1+j2+5))*(alp0**(j0+3))*(alp1**(j1+3))*(alp2**(j2+3)))
    top = (np.pi**2)*factorial(j0+2)*factorial(j1+2)*factorial(j2+2)
    #print(top,denom,top/denom)
    return top/denom

def const(mega,prin=False):
    mega1 = mega +1
    nt = 0
    j0 = []
    j1 = []
    j2 = []
    for i in range(1,mega1+1):
        for i12p1 in range(1,i+1):
            i12 = i12p1 - 1
            i13p1m = i - i12
            for i13p1 in range(1,i13p1m+1):
                i13 = i13p1 - 1
                i14 = i13p1m - i13p1
                nt += 1
                j0.append(i12)
                j1.append(i13)
                j2.append(i14)
    if prin:
        for i in range(nt):
            print(i+1,j0[i],j1[i],j2[i])
    return j0,j1,j2,nt

def read_ci(file):
    with open(file) as f:
        cont = True
        ci = []
        for line in f:
            if cont:
                l = line.strip().split()
                mat = int(l[0])
                cont = False
                ci = []
                count = 0
            else:
                l = line.strip().split()
                ci.append(float(l[0]))
                count += 1
                if count == mat:
                    cont = True
        return ci

def read_order(file):
    with open(file) as f:
        order = []
        for idx, line in enumerate(f):
            l = line.strip().split()
            if idx == 0:
                continue
            else:
                for ele in l:
                    order.append(int(ele)-1)
        return order

def re_org_ci(order,ci,nt):
    ci_new = np.zeros((nt), dtype=float)
    count = 0
    for i in range(nt):
        if i in order:
            index = order[count]
            ci_new[index] = ci[count]
            count += 1
        else:
            ci_new[i] = 0
    return ci_new

def calc_avg_val(ci,j0,j1,j2,nt):
    bot = 0
    top0 = 0
    top1 = 0
    top2 = 0
    for i in range(nt):
        for j in range(nt):
            j00 = j0[i] + j0[j]
            j10 = j1[i] + j1[j]
            j20 = j2[i] + j2[j]
            c_val = ci[i]*ci[j]
            bot += c_val*ppint(j00,j10,j20,0.49,0.51,0.01)
            j00 += 1
            top0 += c_val*ppint(j00,j10,j20,0.49,0.51,0.01)
    for i in range(nt):
        for j in range(nt):
            j00 = j0[i] + j0[j]
            j10 = j1[i] + j1[j]+1
            j20 = j2[i] + j2[j]
            c_val = ci[i]*ci[j]
            top1 += c_val*ppint(j00,j10,j20,0.49,0.51,0.01)
    for i in range(nt):
        for j in range(nt):
            j00 = j0[i] + j0[j]
            j10 = j1[i] + j1[j]
            j20 = j2[i] + j2[j]+1
            c_val = ci[i]*ci[j]
            top2 += c_val*ppint(j00,j10,j20,0.49,0.51,0.01)
    return top0/bot, top1/bot, top2/bot

if __name__ == "__main__":
    #mega = sys.argv[1]
    for mega in range(2):
        print('omega = ',mega)
        j0,j1,j2,nt = const(mega,prin=True)
        for i in range(nt):
            for j in range(nt):
                j00 = j0[i] + j0[j]
                j10 = j1[i] + j1[j]
                j20 = j2[i] + j2[j]
                print(i+1,j+1,j00,j10,j20,ppint(j00,j10,j20,0.49,0.51,0.01))
        ci_temp = read_ci('ci,'+str(mega))
        order = read_order('order,'+str(mega))
        ci = re_org_ci(order,ci_temp,nt)
        #avg_vals = calc_avg_val(ci,j0,j1,j2,nt)
        #r_stri = ['R0','R1','R2']
        #for idx, avg in enumerate(avg_vals):
        #    print(r_stri[idx],avg)
        #print()