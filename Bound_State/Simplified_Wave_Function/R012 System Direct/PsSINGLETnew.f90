Program PsSINGLETnew
#ifdef _OPENMP
	use omp_lib
#endif
	implicit none
	integer, parameter :: nfi=40
	integer, parameter :: nfi2=20
	real*16 fact(0:500),denom
	real*8 rx(nfi),rw(nfi),rx0(nfi),rw0(nfi),pi
	real*8 start,finish,time
	real*8 costp(nfi),sintp(nfi),ang(nfi,nfi,nfi)
	real*8 rcm(nfi,nfi,nfi,nfi),rcp(nfi,nfi,nfi,nfi)
	real*8 rij(nfi,nfi,nfi),volelemrd(nfi,nfi,nfi),volelemt(nfi,nfi)
	real*8 pinv,qtpinv,fac,Tavg(nfi,nfi),Uavg(nfi2,nfi2)
	real*8 alp1,alp2,alp0,phiphiint,lam
	real*8 r0t,r1c1,r23,r2c2,rt1,rt2
	real*8 sint,vol,volrt,voltt,w0t,w1t,w2t,wa,wtp,wtt,r12angt
	real*8 phiphi(nfi2,nfi2),phihphi(nfi2,nfi2),pcoeffl(nfi,7)
	integer j0(nfi),j1(nfi),j2(nfi),j0e,j1e,j2e,j0p,j1p,j2p
	integer iphi,ir0,ir1,ir2,it1,it2,qr0
	integer nt,mega,i,j,jp(7,3),ie,je,lk
	integer qr,qtp,diff,seconds,minutes
	character(len=1024) :: directfile,P12file
	pi = 4.d0*ATAN(1.d0)
	pinv = 1.d0/pi
	call cpu_time(start)
	call facterms(fact)
	
#ifdef _OPENMP
	print *, 'We are using',OMP_GET_NUM_THREADS(),' threads'
#endif

	open(unit=1,file='fort.new',status='old')
	open(unit=5,file='constnew',status='unknown') !linear powers for phi_i
	open(unit=12,file='outonenew',status='unknown') !General information output
	open(unit=7,file='jpmatrixnew',status='unknown') !Powers that get added to phiphj b/c of Hphi action
	read(1,*) mega
	call const(j0,j1,j2,mega,nt,nfi)
	write(12,*) 'N =',nt
	read(1,*) alp1
	read(1,*) alp2
	read(1,*) alp0
	read(1,*) lam
	read(1,*) qr0,qr,qtp
	write(directfile,"(A7,I1)") "Direct,",mega
	open(unit=10,file=directfile,status='unknown') !Output file for trail function 5
	open(unit=15,file='virial',status='unknown')
	write(10,*) mega
	write(10,*) nt

	qtpinv = 1.d0/real(qtp,8)
	phiphi = 0.d0
	phihphi = 0.d0
	pcoeffl = 0.d0
	Tavg = 0.d0
	Uavg = 0.d0
	
	print *, 'quad points',qr0,qr,qtp
	print *, 'Omega:',mega
	
	!PhiPhi and <T> Integral Using Equation
	call jpmatrix(jp)
	
	!$OMP SINGLE
	do ie=1,nt
		call powercoeff(alp0,alp1,alp2,j0,j1,j2,ie,nfi,pcoeffl)
	end do
	!$OMP END SINGLE
	
	do ie=1,nt
		do je=1,nt
			j0e = j0(ie)+j0(je)
			j1e = j1(ie)+j1(je)
			j2e = j2(ie)+j2(je)
			
			phiphi(ie,je) = phiphiint(j0e,j1e,j2e,alp0,alp1,alp2,fact)
			do lk=1,7
				j0p = j0e+jp(lk,1)
				j1p = j1e+jp(lk,2)
				j2p = j2e+jp(lk,3)
				Tavg(ie,je) = Tavg(ie,je)+pcoeffl(ie,lk)*phiphiint(j0p,j1p,j2p,alp0,alp1,alp2,fact)
			end do
		end do
	end do
	
	call gaulag(rx0,rw0,qr0,nfi)
	call gaulag(rx,rw,qr,nfi)

	!ANGULAR QUADRATURE POINTS CALCULATIONS
	do it1=1,qtp
		costp(it1)=Cos((2.d0*it1-1.d0)*pi*0.5d0*qtpinv)
		sintp(it1)=Sqrt(1.d0-costp(it1)**2.d0)
	end do!it1

	!ANGULAR CALCULATIONS
	do it1=1,qtp
		do it2=1,qtp
			do iphi=1,qtp
				ang(it1,it2,iphi)=costp(it1)*costp(it2)+sintp(it1)*sintp(it2)*costp(iphi)
			end do!iphi
			volelemt(it1,it2)=sintp(it1)*sintp(it2)
		end do!it2
	end do!it1
	
	!Short Range Integrals
	!RADIAL & ANGULAR CALCULATION
	do ir1=1,qr
		rt1 = rx(ir1)/(2.d0*alp1+lam)
		do it1=1,qtp
			r1c1=rt1*costp(it1)
			do ir2=1,qr
				rt2 = rx(ir2)/(2.d0*alp2+lam)
				do it2=1,qtp
					r2c2=rt2*costp(it2)
					rcp(ir1,ir2,it1,it2)=r1c1+r2c2
					rcm(ir1,ir2,it1,it2)=r1c1-r2c2
				end do!qtp
			end do!ir1
		end do!it1
	end do!ir0
	
	!RADIAL
	do ir2=1,qr
		rt2 = rx(ir2)/(2.d0*alp2+lam)
		do ir1=1,qr
			rt1 = rx(ir1)/(2.d0*alp1+lam)
			do ir0=1,qr0
				r0t = rx0(ir0)/(2.d0*alp0+lam)
				rij(ir0,ir1,ir2)=r0t**(2.d0)+0.25d0*(rt1**(2.d0)+rt2**(2.d0))
				volelemrd(ir1,ir2,ir0) = (r0t**2.d0)*(rt1**2.d0)*(rt2**2.d0)
			end do!ir2
		end do!ir1
	end do!ir0
	
	print *, "Beginning Hamiltonian Integral Calculations"
	fac = 2.d0*pi/((2.d0*alp0+lam)*(2.d0*alp1+lam)*(2.d0*alp2+lam))
	!$OMP PARALLEL DEFAULT(SHARED)
	!$OMP DO PRIVATE(ie,je)
	do ie=1,nt
		do je=1,nt
			
			call phihphiint(rx,rx0,rw,rw0,qr,qr0,qtp,alp0,alp1,alp2,rcp,rcm,volelemrd, &
	& volelemt,ang,rij,Uavg,pcoeffl,j0,j1,j2,nfi,nfi2,ie,je,lam)
	
		end do!je
	end do!ie
	!$OMP END DO
	!$OMP END PARALLEL
	
	do ie=1,nt
		do je=1,nt
			Uavg(ie,je)=Uavg(ie,je)*fac
			phihphi(ie,je) = -Tavg(ie,je)!+Uavg(ie,je)
			write(10,*) ie,je,phiphi(ie,je),phihphi(ie,je)
			write(15,*) ie,je,Tavg(ie,je),Uavg(ie,je)
			print *, ie,je,phiphi(ie,je),phihphi(ie,je)
			print *, 'Energy: ',phihphi(ie,je)/phiphi(ie,je)
		end do
	end do

	call cpu_time(finish)
	diff = int((finish-start))
	seconds = int(mod(diff,60))
	minutes = int((diff-seconds)/60.0)
	time = finish - start
	write(12,*) 'Time:',time,'Seconds'
	write(12,*) 'Total Time:',minutes,'Minutes and',seconds,'Seconds'
	print *, "Time:",time,"Seconds"

End Program PsSINGLETnew

Function phiphiint(j0,j1,j2,alp0,alp1,alp2,fact)
	real*16 fact(0:500)
	real*8 alp0,alp1,alp2,pi,denom,phiphiint
	integer j0,j1,j2
	pi = 4.d0*ATAN(1.d0)
	
	denom = (2.d0**real(j0+j1+j2+3,16))*(alp0**(j0+3))*(alp1**(j1+3))*(alp2**(j2+3))
	phiphiint = (pi**3.d0)*fact(j0+2)*fact(j1+2)*fact(j2+2)/denom/(4.d0*pi)
End Function

Subroutine phihphiint(rx,rx0,rw,rw0,qr,qr0,qtp,alp0,alp1,alp2,rcp,rcm,volelemrd, &
			& volelemt,ang,rij,Uavg,pcoeffl,j0,j1,j2,nfi,nfi2,ie,je,lam)
	implicit none
	integer nfi,nfi2
	real*8 Uavg(nfi2,nfi2),pcoeffl(nfi,7)
	real*8 rx(nfi),rw(nfi),rx0(nfi),rw0(nfi),pi
	real*8 ang(nfi,nfi,nfi),r12angt
	real*8 rcm(nfi,nfi,nfi,nfi),rcp(nfi,nfi,nfi,nfi)
	real*8 rij(nfi,nfi,nfi),volelemrd(nfi,nfi,nfi),volelemt(nfi,nfi)
	real*8 alp0,alp1,alp2,funlk,fun,precal,exppv,vol,lam
	real*8 expqt,pinv,qtpinv,r0rcmt,r0rcpt
	real*8 r0t,r12,r14,r23,r34,rijt,rt1,rt2
	real*8 volrt,voltt,w0t,w1t,w2t,wa,wtp,wtt
	real*8 j0e,j1e,j2e,j0p,j1p,j2p,pot,exp0,exp1,exp2
	integer j0(nfi),j1(nfi),j2(nfi)
	integer iphi,ir0,ir1,ir2,it1,it2,qr0,qtp
	integer ie,je,lk,qr
	pi = 4.d0*ATAN(1.d0)
	pinv = 1.d0/pi
	qtpinv = 1.d0/real(qtp,8)
	wtp = pi*qtpinv
	wa = 2.d0*wtp*wtp*wtp
	
	j0e=real(j0(ie)+j0(je),8)
	j1e=real(j1(ie)+j1(je),8)
	j2e=real(j2(ie)+j2(je),8)
	
	do ir2=1,qr
		rt2 = rx(ir2)/(2.d0*alp2+lam)
		w2t = rw(ir2)
		do ir1=1,qr
			rt1 = rx(ir1)/(2.d0*alp1+lam)
			w1t = rw(ir1)
			do ir0=1,qr0
				r0t = rx0(ir0)/(2.d0*alp0+lam)
				w0t = rw0(ir0)
				rijt = rij(ir0,ir1,ir2)
				wtt=w0t*w1t*w2t*wa
				volrt = volelemrd(ir1,ir2,ir0)
				do it1=1,qtp
					do it2=1,qtp
						r0rcpt=r0t*rcp(ir1,ir2,it1,it2)
						r0rcmt=r0t*rcm(ir1,ir2,it1,it2)
						voltt=volelemt(it1,it2)
						do iphi=1,qtp
							!Direct Short Range (phi,phi) Integrals
							!R1 = r13;R2 = r24
							r12angt=0.5d0*rt1*rt2*ang(it1,it2,iphi)
							r12=Sqrt(rijt-r0rcmt-r12angt)
							r14=Sqrt(rijt+r0rcpt+r12angt)
							r23=Sqrt(rijt-r0rcpt+r12angt)
							r34=Sqrt(rijt+r0rcmt-r12angt)
							vol=volrt*wtt*voltt
							pot = (1.d0/r12+1.d0/r34-1.d0/rt1-1.d0/rt2-1.d0/r14-1.d0/r23)
							funlk = (r0t**j0e)*(rt1**j1e)*(rt2**j2e)
							exppv = Exp(r0t*lam)*Exp(rt1*lam)*Exp(rt2*lam)
							Uavg(ie,je) = Uavg(ie,je)+pot*funlk*vol*exppv
						end do!iphi
					end do!it2
				end do!it1
			end do!ir0
		end do!ir1
	end do!ir2
End Subroutine

      !
      ! This subroutine creates the constants a1,a2,a3,a4,a5, and a6
      ! for the exponential terms.
      !

Subroutine const(a1,a2,a3,mega,nt,nfi) ! Obtained from Peter Van Reeth
	implicit none
	integer nfi
	integer a1(nfi),a2(nfi),a3(nfi),a4(nfi),a5(nfi),a6(nfi) !exponential constants!
	integer nt,mega,inx,nt2
	integer megap,i,i12,i13,i14,i23,i24,i34
	integer i12p1,i13p1,i14p1,i23p1,i24p1,i34p1
	integer i14p1m,i13p1m,i24p1m,i23p1m,j
	logical track

	megap = mega + 1
	inx = 0

	do i = 1,megap
		do i12p1 = 1,i,1
			i12 = i12p1 - 1
			i13p1m = i - i12
			do i13p1 = 1,i13p1m
				i13 = i13p1 - 1
				i14 = i13p1m - i13p1
							
				inx = inx + 1
				a1(inx) = i12
				a2(inx) = i13
				a3(inx) = i14
			end do
		end do
		write(5,*) i,inx
	end do
	nt = inx
	write(5,*) ' N    R0    R1    R2'
	write(5,"(7I7)") (j,a1(j),a2(j),a3(j),j=1,nt)
End Subroutine

Subroutine facterms(fact)
	implicit none
	real*16 fact(0:500)
	integer i
	do i=0,498
		if(i.eq.0) then
			fact(i) = 1.q0
		else
			fact(i) = fact(i-1)*real(i,16)
		end if
	end do
End Subroutine

Subroutine gaulag(x,w,n,nfi)
	implicit none
	integer nfi,n
	real*8 x(nfi),w(nfi)
	real*8 anu(n)
	real*8, parameter :: eps=3.0d-13
	real*8, parameter :: pi=4.d0*atan(1.d0)
	integer j,its,i
	integer, parameter :: maxit=10
	real*8, parameter :: c1=9.084064d-1,c2=5.214976d-2
	real*8, parameter :: c3=2.579930d-3,c4=3.986126d-3
	real*8 rhs(n),r2(n),r3(n),theta(n),p1(n),p2(n),p3(n)
	real*8 pp(n),z(n),z1(n)
	logical unfinished(n)
	x=0.d0
	w=0.d0
	anu = 4.d0*n+2.d0
	rhs(1) = (4.d0*n-1.d0)*pi/anu(1)
	do i=2,n
		rhs(i)=(4.d0*n-4.d0*i+3.d0)*pi/anu(i)
	end do
	r3 = rhs**(1.d0/3.d0)
	r2 = r3**2.d0
	theta = r3*(c1+r2*(c2+r2*(c3+r2*c4)))
	z = anu*(Cos(theta)**2.d0)
	unfinished = .true.
	do its=1,maxit
		where (unfinished)
			p1=1.d0
			p2=0.d0
		end where
		do j=1,n
			where (unfinished)
				p3=p2
				p2=p1
				p1=((2.d0*real(j,8)-1.d0-z)*p2-(real(j,8)-1.d0)*p3)/real(j,8)
			end where
		end do
		where (unfinished)
			pp=(n*p1-n*p2)/z
			z1=z
			z=z1-p1/pp
			unfinished = (abs(z-z1) > eps*z)
		end where
		if (.not. any(unfinished)) exit
	end do
	if (its == maxit+1) print *, 'too many iterations in gaulag'
	x=z
	w=-1.d0/(pp*real(n,8)*p2)
End Subroutine

      !
      !jpmatrix creates a matrix of the powers of the interparticle
      !distances that are from the H operation on phi.
      !

Subroutine jpmatrix(jp)
	implicit none
	integer jp(7,3),jp2(7,3)
	integer ipp,jpp

	jp=0
	jp2=0
	
	jp2(2,1) = -1
	jp2(3,2) = -1
	jp2(4,3) = -1
	jp2(5,1) = -2
	jp2(6,2) = -2
	jp2(7,3) = -2

	do ipp=1,7
		do jpp=1,7
			jp(ipp,jpp) = jp2(ipp,jpp)
		end do
	end do
	
	write(7,*) "jp matrix"
	write(7,*) 'N ','R0 ','R1 ','R2 '
	do ipp=1,7
		write(7,*) ipp,jp(ipp,1),jp(ipp,2),jp(ipp,3)
	end do
End Subroutine

      !
      !This subroutine creates the coefficients for the phi H phi matrix elements
      !

Subroutine powercoeff(alp0,alp1,alp2,j0,j1,j2,ie,nfi,pcoeffl)
	implicit none
	integer nfi
	real*8 pcoeffl(nfi,7)
	real*8 alp0,alp1,alp2
	integer j0(nfi),j1(nfi),j2(nfi)
	integer ie
	
	pcoeffl(ie,1)=-(alp0**2.d0)/2.d0-alp1**2.d0-alp2**2.d0
	pcoeffl(ie,2)=alp0+alp0*j0(ie)
	pcoeffl(ie,3)=2.d0*alp1+2.d0*j1(ie)*alp1
	pcoeffl(ie,4)=2.d0*alp2+2.d0*j2(ie)*alp2
	pcoeffl(ie,5)=-j0(ie)/2.d0-(j0(ie)**2.d0)/2.d0
	pcoeffl(ie,6)=-j1(ie)-j1(ie)**2.d0
	pcoeffl(ie,7)=-j2(ie)-j2(ie)**2.d0
End Subroutine
