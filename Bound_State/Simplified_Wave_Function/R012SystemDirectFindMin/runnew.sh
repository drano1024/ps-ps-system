#!/bin/bash
ifort -O2 -c -qopenmp -fpp -mcmodel=large -o PsSINGLETnew.o PsSINGLETnew.f90
ifort -O2 -qopenmp -fpp -mcmodel=large PsSINGLETnew.o -o psps
alp0=0.0
alp1=0.49
alp2=0.51
del=0.1
i=0
j=0
k=0
while [ $i -lt 1 ]
do
	#alp1=$(echo $alp1 + $del | bc)
	while [ $j -lt 1 ]
	do
		#alp2=$(echo $alp2 + $del | bc)
		while [ $k -lt 20 ]
		do
			alp0=$(echo $alp0 + $del | bc)
			echo "0" > fort.new
			echo "0$alp1" >> fort.new
			echo "0$alp2" >> fort.new
			echo "0$alp0" >> fort.new
			echo "30 30 30" >> fort.new
			echo "alp1 = 0$alp1"
			echo "alp2 = 0$alp2"
			echo "alp0 = 0$alp0"
			set OMP_NUM_THREADS=4
			export OMP_NUM_THREADS=4
			./psps
			./runTodd.sh
			k=$[$k+1]
		done
		j=$[$j+1]
		alp0=0.1
		k=0
	done
	j=0
	alp2=0.51
	i=$[$i+1]
done
