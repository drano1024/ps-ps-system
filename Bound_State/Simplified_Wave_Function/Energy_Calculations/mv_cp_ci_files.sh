#!/bin/bash
for i in 01 15 30
do
	for j in 0 1 2 3 4 5
	do
		for fil in ci order
		do
			cp "R0_limited_$i/Singlet(+)/R012 w=$j/$fil" "./"
			mv $fil $fil,$j
			mv $fil,$j "../Average_Values_Simplified_Restricted_$i/"
		done
	done
done
