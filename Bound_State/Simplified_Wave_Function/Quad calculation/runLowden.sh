#!/bin/bash
res=""
alpha="0.15_"

for quad in 40 50 60 70 80
do
    echo "Direct $quad"
    Dirfil="Direct1_$alpha$quad$res"
    command="python ../../Lowden_Method/Lowden_routine.py $Dirfil"
    $command
    echo "Direct and Exchange $quad"
    files2=$Dirfil" Exchange1_$alpha$quad$res"
    command2="python ../../Lowden_Method/Lowden_routine.py $files2"
    $command2
    echo "*******************************"
done
