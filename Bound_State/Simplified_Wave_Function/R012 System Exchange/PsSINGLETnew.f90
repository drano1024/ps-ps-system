Program PsSINGLETnew
#ifdef _OPENMP
	use omp_lib
#endif
	implicit none
	integer, parameter :: nfi=40
	integer, parameter :: nfi2=10
	real*8 rx(nfi),rw(nfi),rx0(nfi),rw0(nfi),pi
	real*8 start,finish,time,lam
	real*8 costp(nfi),sintp(nfi),ang(nfi,nfi,nfi)
	real*8 rcm(nfi,nfi,nfi,nfi),rcp(nfi,nfi,nfi,nfi)
	real*8 rij(nfi,nfi,nfi),volelemrd(nfi,nfi,nfi),volelemt(nfi,nfi)
	real*8 pinv,qtpinv,fac,Tavg(nfi2,nfi2),Uavg(nfi2,nfi2)
	real*8 alp1,alp2,alp0,phiphiint
	real*8 r0t,r1c1,r23,r2c2,rt1,rt2
	real*8 sint,vol,volrt,voltt,w0t,w1t,w2t,wa,wtp,wtt,r12angt
	real*8 phiphi(nfi2,nfi2),phihphi(nfi2,nfi2),pcoeffl(nfi,7)
	integer j0(nfi),j1(nfi),j2(nfi),j0e,j1e,j2e,j0p,j1p,j2p
	integer iphi,ir0,ir1,ir2,it1,it2,qr0
	integer nt,mega,i,j,jp(7,3),ie,je,lk
	integer qr,qtp,diff,seconds,minutes
	character(len=1024) :: directfile,P12file
	pi = 4.d0*ATAN(1.d0)
	pinv = 1.d0/pi
	call cpu_time(start)
	
#ifdef _OPENMP
	print *, 'We are using',OMP_GET_NUM_THREADS(),' threads'
#endif

	open(unit=1,file='fort.new',status='old')
	open(unit=5,file='constnew',status='unknown') !linear powers for phi_i
	open(unit=12,file='outonenew',status='unknown') !General information output
	open(unit=7,file='jpmatrixnew',status='unknown') !Powers that get added to phiphj b/c of Hphi action
	read(1,*) mega
	call const(j0,j1,j2,mega,nt,nfi)
	write(12,*) 'N =',nt
	read(1,*) alp1
	read(1,*) alp2
	read(1,*) alp0
	read(1,*) lam
	read(1,*) qr0,qr,qtp
	write(P12file,"(A9,I1)") "Exchange,",mega
	open(unit=11,file=P12file,status='unknown')
	open(unit=15,file='virial',status='unknown')

	qtpinv = 1.d0/real(qtp,8)
	phiphi = 0.d0
	phihphi = 0.d0
	pcoeffl = 0.d0
	Tavg = 0.d0
	Uavg = 0.d0
	
	print *, 'quad points',qr0,qr,qtp
	print *, 'Omega:',mega
	
	call jpmatrix(jp)
	
	!$OMP SINGLE
	do ie=1,nt
		call powercoeff(alp0,alp1,alp2,j0,j1,j2,ie,nfi,pcoeffl)
	end do
	!$OMP END SINGLE
	
	call gaulag(rx0,rw0,qr0,nfi)
	call gaulag(rx,rw,qr,nfi)

	!ANGULAR QUADRATURE POINTS CALCULATIONS
	do it1=1,qtp
		costp(it1)=Cos((2.d0*it1-1.d0)*pi*0.5d0*qtpinv)
		sintp(it1)=Sqrt(1.d0-costp(it1)**2.d0)
	end do!it1

	!ANGULAR CALCULATIONS
	do it1=1,qtp
		do it2=1,qtp
			do iphi=1,qtp
				ang(it1,it2,iphi)=costp(it1)*costp(it2)+sintp(it1)*sintp(it2)*costp(iphi)
			end do!iphi
			volelemt(it1,it2)=sintp(it1)*sintp(it2)
		end do!it2
	end do!it1
	
	!Short Range Integrals
	!RADIAL & ANGULAR CALCULATION
	do ir1=1,qr
		rt1 = rx(ir1)/(1.d0+lam)
		do it1=1,qtp
			r1c1=rt1*costp(it1)
			do ir2=1,qr
				rt2 = rx(ir2)/(1.d0+lam)
				do it2=1,qtp
					r2c2=rt2*costp(it2)
					rcp(ir1,ir2,it1,it2)=r1c1+r2c2
					rcm(ir1,ir2,it1,it2)=r1c1-r2c2
				end do!qtp
			end do!ir1
		end do!it1
	end do!ir0
	
	!RADIAL
	do ir2=1,qr
		rt2 = rx(ir2)/(1.d0+lam)
		do ir1=1,qr
			rt1 = rx(ir1)/(1.d0+lam)
			do ir0=1,qr0
				r0t = rx0(ir0)/(1.d0+lam)
				rij(ir0,ir1,ir2)=r0t**(2.d0)+0.25d0*(rt1**(2.d0)+rt2**(2.d0))
				volelemrd(ir1,ir2,ir0) = (r0t**2.d0)*(rt1**2.d0)*(rt2**2.d0)
			end do!ir2
		end do!ir1
	end do!ir0
	
	print *, "Beginning Integral Calculations"
	fac = 2.d0*pi/((1.d0+lam)**3.d0)
	!$OMP PARALLEL DEFAULT(SHARED)
	!$OMP DO PRIVATE(ie,je)
	do ie=1,nt
		do je=1,nt
			
			call Exchangeint(rx,rx0,rw,rw0,qr,qr0,qtp,alp0,alp1,alp2,rcp,rcm,volelemrd, &
	& volelemt,ang,rij,phiphi,Tavg,Uavg,pcoeffl,j0,j1,j2,nfi,nfi2,ie,je,jp,lam)
	
		end do!je
	end do!ie
	!$OMP END DO
	!$OMP END PARALLEL
	
	do ie=1,nt
		do je=1,nt
			Uavg(ie,je)=Uavg(ie,je)*fac
			Tavg(ie,je)=Tavg(ie,je)*fac
			phiphi(ie,je)=phiphi(ie,je)*fac
			phihphi(ie,je) = Tavg(ie,je)+Uavg(ie,je)
			write(11,*) ie,je,phiphi(ie,je),phihphi(ie,je)
			write(15,*) ie,je,Tavg(ie,je),Uavg(ie,je)
			print *, ie,je,phiphi(ie,je),phihphi(ie,je)
		end do
	end do

	call cpu_time(finish)
	diff = int((finish-start))
	seconds = int(mod(diff,60))
	minutes = int((diff-seconds)/60.0)
	time = finish - start
	write(12,*) 'Time:',time,'Seconds'
	write(12,*) 'Total Time:',minutes,'Minutes and',seconds,'Seconds'
	print *, "Time:",time,"Seconds"

End Program PsSINGLETnew

Subroutine Exchangeint(rx,rx0,rw,rw0,qr,qr0,qtp,alp0,alp1,alp2,rcp,rcm,volelemrd, &
			& volelemt,ang,rij,phiphi,Tavg,Uavg,pcoeffl,j0,j1,j2,nfi,nfi2,ie,je,jp,lam)
	implicit none
	integer nfi,nfi2
	real*8 phiphi(nfi2,nfi2),Tavg(nfi2,nfi2),Uavg(nfi2,nfi2),pcoeffl(nfi,7)
	real*8 rx(nfi),rw(nfi),rx0(nfi),rw0(nfi),pi
	real*8 ang(nfi,nfi,nfi),r12angt
	real*8 rcm(nfi,nfi,nfi,nfi),rcp(nfi,nfi,nfi,nfi)
	real*8 rij(nfi,nfi,nfi),volelemrd(nfi,nfi,nfi),volelemt(nfi,nfi)
	real*8 alp0,alp1,alp2,funlk,fun,precal,exppv,vol
	real*8 expqt,pinv,qtpinv,r0rcmt,r0rcpt
	real*8 r0t,r12,r14,r23,r34,rijt,rt1,rt2,j0i,j0j,j1i,j1j,j2i,j2j
	real*8 volrt,voltt,w0t,w1t,w2t,wa,wtp,wtt,jpr(7,3)
	real*8 j0e,j1e,j2e,j0p,j1p,j2p,pot,r0p,r1p,r2p,funi,funj
	real*8 lam,exp0,exp1,exp2
	integer j0(nfi),j1(nfi),j2(nfi),jp(7,3)
	integer iphi,ir0,ir1,ir2,it1,it2,qr0,qtp
	integer ie,je,lk,qr
	pi = 4.d0*ATAN(1.d0)
	pinv = 1.d0/pi
	qtpinv = 1.d0/real(qtp,8)
	wtp = pi*qtpinv
	wa = 2.d0*wtp*wtp*wtp
	
	j0i = real(j0(ie),8)
	j0j = real(j0(je),8)
	j1i = real(j1(ie),8)
	j1j = real(j1(je),8)
	j2i = real(j2(ie),8)
	j2j = real(j2(je),8)
	jpr=real(jp,8)
	
	do ir2=1,qr
		rt2 = rx(ir2)/(1.d0+lam)
		w2t = rw(ir2)
		do ir1=1,qr
			rt1 = rx(ir1)/(1.d0+lam)
			w1t = rw(ir1)
			do ir0=1,qr0
				r0t = rx0(ir0)/(1.d0+lam)
				w0t = rw0(ir0)
				rijt = rij(ir0,ir1,ir2)
				wtt=w0t*w1t*w2t*wa
				volrt = volelemrd(ir1,ir2,ir0)
				do it1=1,qtp
					do it2=1,qtp
						r0rcpt=r0t*rcp(ir1,ir2,it1,it2)
						r0rcmt=r0t*rcm(ir1,ir2,it1,it2)
						voltt=volelemt(it1,it2)
						do iphi=1,qtp
							!R1 = r24;R2 = r13
							r12angt=0.5d0*rt1*rt2*ang(it1,it2,iphi)
							r12=Sqrt(rijt-r0rcmt-r12angt)
							r14=Sqrt(rijt+r0rcpt+r12angt)
							r23=Sqrt(rijt-r0rcpt+r12angt)
							r34=Sqrt(rijt+r0rcmt-r12angt)
							r1p=r14
							r2p=r23
							r0p=0.5d0*Sqrt(rt1**2.d0+rt2**2.d0-4.d0*r12angt)
							vol=volrt*wtt*voltt
							funi=(r1p**j1i)*(r2p**j2i)*(r0p**j0i)
							funj=(rt1**j1j)*(rt2**j2j)*(r0t**j0j)
							exp0=Exp(r0t*(1.0d0-alp0+lam))
							exp1=Exp(rt1*(1.0d0-alp1+lam))
							exp2=Exp(rt2*(1.0d0-alp2+lam))
							fun=vol*funi*funj*Exp(-alp0*r0p-alp1*r1p-alp2*r2p)*exp0*exp1*exp2
							phiphi(ie,je)=phiphi(ie,je)+fun
							pot=(1.d0/r12+1.d0/r34-1.d0/rt1-1.d0/rt2-1.d0/r14-1.d0/r23)
							Uavg(ie,je) = Uavg(ie,je)+fun*pot
							do lk=1,7
								funlk=fun*(r0t**jpr(lk,1))*(rt1**jpr(lk,2))*(rt2**jpr(lk,3))
								Tavg(ie,je)=Tavg(ie,je)+funlk*pcoeffl(ie,lk)
							end do
						end do!iphi
					end do!it2
				end do!it1
			end do!ir0
		end do!ir1
	end do!ir2
End Subroutine

      !
      ! This subroutine creates the constants a1,a2,a3,a4,a5, and a6
      ! for the exponential terms.
      !

Subroutine const(a1,a2,a3,mega,nt,nfi) ! Obtained from Peter Van Reeth
	implicit none
	integer nfi
	integer a1(nfi),a2(nfi),a3(nfi),a4(nfi),a5(nfi),a6(nfi) !exponential constants!
	integer nt,mega,inx,nt2
	integer megap,i,i12,i13,i14,i23,i24,i34
	integer i12p1,i13p1,i14p1,i23p1,i24p1,i34p1
	integer i14p1m,i13p1m,i24p1m,i23p1m,j
	logical track

	megap = mega + 1
	inx = 0

	do i = 1,megap
		do i12p1 = 1,i,1
			i12 = i12p1 - 1
			i13p1m = i - i12
			do i13p1 = 1,i13p1m
				i13 = i13p1 - 1
				i14 = i13p1m - i13p1
							
				inx = inx + 1
				a1(inx) = i12
				a2(inx) = i13
				a3(inx) = i14
			end do
		end do
		write(5,*) i,inx
	end do
	nt = inx
	write(5,*) ' N    R0    R1    R2'
	write(5,"(4I3)") (j,a1(j),a2(j),a3(j),j=1,nt)
End Subroutine

Subroutine facterms(fact)
	implicit none
	real*16 fact(0:500)
	integer i
	do i=0,498
		if(i.eq.0) then
			fact(i) = 1.q0
		else
			fact(i) = fact(i-1)*real(i,16)
		end if
	end do
End Subroutine

Subroutine gaulag(x,w,n,nfi)
	implicit none
	integer nfi,n
	real*8 x(nfi),w(nfi)
	real*8 anu(n)
	real*8, parameter :: eps=3.0d-13
	real*8, parameter :: pi=4.d0*atan(1.d0)
	integer j,its,i
	integer, parameter :: maxit=10
	real*8, parameter :: c1=9.084064d-1,c2=5.214976d-2
	real*8, parameter :: c3=2.579930d-3,c4=3.986126d-3
	real*8 rhs(n),r2(n),r3(n),theta(n),p1(n),p2(n),p3(n)
	real*8 pp(n),z(n),z1(n)
	logical unfinished(n)
	x=0.d0
	w=0.d0
	anu = 4.d0*n+2.d0
	rhs(1) = (4.d0*n-1.d0)*pi/anu(1)
	do i=2,n
		rhs(i)=(4.d0*n-4.d0*i+3.d0)*pi/anu(i)
	end do
	r3 = rhs**(1.d0/3.d0)
	r2 = r3**2.d0
	theta = r3*(c1+r2*(c2+r2*(c3+r2*c4)))
	z = anu*(Cos(theta)**2.d0)
	unfinished = .true.
	do its=1,maxit
		where (unfinished)
			p1=1.d0
			p2=0.d0
		end where
		do j=1,n
			where (unfinished)
				p3=p2
				p2=p1
				p1=((2.d0*real(j,8)-1.d0-z)*p2-(real(j,8)-1.d0)*p3)/real(j,8)
			end where
		end do
		where (unfinished)
			pp=(n*p1-n*p2)/z
			z1=z
			z=z1-p1/pp
			unfinished = (abs(z-z1) > eps*z)
		end where
		if (.not. any(unfinished)) exit
	end do
	if (its == maxit+1) print *, 'too many iterations in gaulag'
	x=z
	w=-1.d0/(pp*real(n,8)*p2)
End Subroutine

      !
      !jpmatrix creates a matrix of the powers of the interparticle
      !distances that are from the H operation on phi.
      !

Subroutine jpmatrix(jp)
	implicit none
	integer jp(7,3),jp2(7,3)
	integer ipp,jpp

	jp=0
	jp2=0
	
	jp2(2,1) = -1
	jp2(3,2) = -1
	jp2(4,3) = -1
	jp2(5,1) = -2
	jp2(6,2) = -2
	jp2(7,3) = -2

	do ipp=1,7
		do jpp=1,7
			jp(ipp,jpp) = jp2(ipp,jpp)
		end do
	end do
	
	write(7,*) "jp matrix"
	write(7,*) 'N ','R0 ','R1 ','R2 '
	do ipp=1,7
		write(7,*) ipp,jp(ipp,1),jp(ipp,2),jp(ipp,3)
	end do
End Subroutine

      !
      !This subroutine creates the coefficients for the phi H phi matrix elements
      !

Subroutine powercoeff(alp0,alp1,alp2,j0,j1,j2,ie,nfi,pcoeffl)
	implicit none
	integer nfi
	real*8 pcoeffl(nfi,7)
	real*8 alp0,alp1,alp2
	integer j0(nfi),j1(nfi),j2(nfi)
	integer ie
	
	pcoeffl(ie,1)=-(alp0**2.d0)/2.d0-alp1**2.d0-alp2**2.d0
	pcoeffl(ie,2)=alp0+alp0*j0(ie)
	pcoeffl(ie,3)=2.d0*alp1+2.d0*j1(ie)*alp1
	pcoeffl(ie,4)=2.d0*alp2+2.d0*j2(ie)*alp2
	pcoeffl(ie,5)=-j0(ie)/2.d0-(j0(ie)**2.d0)/2.d0
	pcoeffl(ie,6)=-j1(ie)-j1(ie)**2.d0
	pcoeffl(ie,7)=-j2(ie)-j2(ie)**2.d0
End Subroutine
