import argparse
import yaml
import os

parser = argparse.ArgumentParser()
parser.add_argument(
    "--list",
    action="store_true",
    help="List available calculations: --list",
    default=False
)
parser.add_argument(
    "--input",
    type=str,
    help="The input yaml file to use: --input [yaml file name]",
    default=""
)

def integrals( integrals : dict ) -> None:
    # Get omega values
    if not isinstance( integrals["omega"], list ):
        w = [ str( i ) for i in [ integrals["omega"] ] ]
    else:
        w = [ str( i ) for i in integrals["omega"] ]
    out_files = [ "ss%s,%s" %( ex, i ) for ex in ["Direct","P12"] for i in w ]
    
    # Get compiler and ensure its either ifort or gfortran
    if "compiler" in integrals:
        compiler = integrals["compiler"].lower()
        if compiler not in [ "ifort", "gfortran" ]:
            raise ValueError( "Unrecognized compiler. Options are ifort or gfortran." )
    else:
        compiler = "ifort"
    
    # Get integral type and ensure its either Analytic or Quadrature
    # TODO: work on quadrature codes
    if "integral_type" in integrals:
        int_type = integrals["integral_type"].capitalize()
        if int_type not in [ "Analytic", "Quadrature" ]:
            raise ValueError( "Unrecognized integral type. Options are Analytic or Quadrature." )
    else:
        int_type = "Analytic"
    
    if "use_expansion" in integrals:
        use_exp = integrals["use_expansion"]
    else:
        use_exp = False
    
    # Change directory and edits scripts as needed
    os.chdir( "Bound_State/Integrals/%s_Codes/" %( int_type ) )
    os.popen( "sed -i '/FC=/c\FC=%s' Makefile" %( compiler ) ).read()
    if use_exp:
        os.popen( "sed -i '/make ints/c\make ints-asy' run.sh" ).read()
        os.popen( "sed -i '/make clean/c\make clean-asy' run.sh" ).read()
        error_check = "rm constasy"
    else:
        os.popen( "sed -i '/make ints/c\make ints' run.sh" ).read()
        os.popen( "sed -i '/make clean/c\make clean' run.sh" ).read()
        error_check = "rm const"
    os.popen( "sed -i '/for omega in/c\%s' run.sh" %( "for omega in "+" ".join( w ) ) ).read()
    res = os.popen( "./run.sh" ).read().split( "\n" )
    if error_check not in res[-2]:
        raise ValueError( "An error occured when running: %s" %( "\n".join( res ) ) )
    
    # Move output files to root directory
    for ofile in out_files:
        os.popen( "mv %s ../../../" %( ofile ) ).read()
    os.chdir( "../../../" )

def energy( energies : dict ) -> None:
    # Check energy calculation type
    if "energy_type" in energies:
        ene_type = energies["energy_type"].capitalize()
        if ene_type not in [ "Todd", "Lowden" ]:
            raise ValueError( "Unrecognized energy calculation method. Options are Todd or Lowden." )
    else:
        raise ValueError( "energy_type must be specified in the input yaml file. Options are Todd or Lowden." )
    
    # Get compiler and ensure its either ifort or gfortran
    if "compiler" in energies:
        compiler = energies["compiler"].lower()
        if compiler not in [ "ifort", "gfortran" ]:
            raise ValueError( "Unrecognized compiler. Options are ifort or gfortran." )
    else:
        compiler = "ifort"

    # Get spin state of desired energies calculations
    if "spin_state" in energies:
        spin = energies["spin_state"]
        if spin not in [ "singlet", "triplet" ]:
            raise ValueError( "Unrecognized spin state. Options are singlet or triplet." )
    else:
        spin = "singlet"
    
    # Ensure directory containing integrals exists
    if "integrals_dir" in energies:
        int_dir = energies["integrals_dir"]+"/"
    else:
        raise ValueError( "integrals_dir must be specified in the input yaml file." )
    
    # Get exchanges
    if "exchanges" in energies:
        if not isinstance( energies["exchanges"], list ):
            ex = [ energies["exchanges"].capitalize() ]
        else:
            ex = [ e.capitalize() if e.upper() == "DIRECT" else e for e in energies["exchanges"] ]
        
        # Verify exchanges given are allowed
        all_exchanges = [ "Direct", "P12", "P34", "P12P34" ]
        for e in ex:
            if e not in all_exchanges:
                raise ValueError( "%s not in allowed exchanges." %( e ) )
        
        # Create array for integer exchanges
        ex_int = []
        for e, i in zip( all_exchanges, [0,1,2,3] ):
            if e in ex:
                if ( i > 0 ) and ( 0 not in ex_int ):
                    raise ValueError( "%s exchange energies cannot be calculated without Direct term." %( e ) )
                ex_int.append( i )
    else:
        raise ValueError( "exchanges must be specified in the input yaml file. Options are Direct, P12, P34, P12P34." )
    
    # Get file names of files containing integrals
    # Verify files exist
    if "omega" in energies:
        if not isinstance( energies["omega"], list ):
            w = [ str( i ) for i in [ energies["omega"] ] ]
        else:
            w = [ str( i ) for i in energies["omega"] ]
    else:
        raise ValueError( "omega must be specified in the input yaml file." )
    int_files = [ "ss%s,%s" %( exc, i ) for exc in ex for i in w ]
    for ifile in int_files:
        if not os.path.exists( int_dir + ifile ):
            raise ValueError( "%s file does not exist." %( ifile ) )
    
    # Symbolicly link integral files to energy calculation directory
    os.chdir( "./Bound_State/Energy_Calculations/%s_Method/" %( ene_type ) )
    dir_files = os.listdir( "./" )
    for ifile in int_files:
        if ifile in dir_files:
            raise ValueError( "%s file already exists in directory: ./Bound_State/Energy_Calculations/%s_Method/" %( ifile, ene_type ) )
        os.popen( "ln -sf ../../../%s ./" %( int_dir + ifile ) ).read()
    
    if ene_type == "Todd":
        # Change for loops in run script and compiler in Makefile
        os.popen( "sed -i '/for omega in/c\%s' run.sh" %( "for omega in "+" ".join( w ) ) ).read()
        os.popen( "sed -i '/for e in/c\%s' run.sh" %( "    for e in "+str( max( ex_int ) ) ) ).read()
        os.popen( "sed -i '/FC=/c\FC=%s' Makefile" %( compiler ) ).read()

        # Change all exchanges to false in Todd.f90
        os.popen( "sed -i '/P12=/c\	P12=.false.' Todd.f90" ).read()
        os.popen( "sed -i '/P34=/c\	P34=.false.' Todd.f90" ).read()
        os.popen( "sed -i '/P1234=/c\    P1234=.false.' Todd.f90" ).read()
        res = os.popen( "./run.sh" ).read()

        # Getting list of output files
        out_files = []
        base_out_files = [ "ci.", "Toddss", "Todd." ]
        for i, base in enumerate( base_out_files ):
            ext = ""
            if i == 2: ext = ".out"
            for i in w:
                out_files.append( base+ex[-1]+","+i+ext )
    else:
        # Change run script
        os.popen( "sed -i '/for i in/c\%s' run.sh" %( "for i in "+" ".join( w ) ) ).read()
        exch = "	python Lowden_routine.py "
        for e in ex:
            exch += " ss%s,$i" %( e )
        out_file = ex[-1]+",$i.out"
        exch += " > %s" %( out_file )
        os.popen( "sed -i '/python Lowden_routine.py/c\%s' run.sh" %( exch ) ).read()
        os.popen( "./run.sh" ).read()

        # Getting list of output files
        out_files = []
        for i in w:
            out_files.append( "%s,%s.out" %( ex[-1], i ) )

    # Remove sym linked integral files
    for ifile in int_files:
        os.popen( "rm %s" %( ifile ) ).read()
    
    # Move output files to root directory
    for ofile in out_files:
        os.popen( "mv %s ../../../" %( ofile ) ).read()
    os.chdir( "../../../" )

def average_vals( avg_vals : dict ) -> None:# Get compiler and ensure its either ifort or gfortran
    if "compiler" in avg_vals:
        compiler = avg_vals["compiler"].lower()
        if compiler not in [ "ifort", "gfortran" ]:
            raise ValueError( "Unrecognized compiler. Options are ifort or gfortran." )
    else:
        compiler = "ifort"
    
    # Ensure directory containing integrals exists
    if "ci_dir" in avg_vals:
        ci_dir = avg_vals["ci_dir"]+"/"
    else:
        raise ValueError( "ci_dir must be specified in the input yaml file." )
    
    # Get exchanges
    if "exchanges" in avg_vals:
        if not isinstance( avg_vals["exchanges"], list ):
            ex = [ avg_vals["exchanges"].capitalize() ]
        else:
            ex = [ e.capitalize() if e.upper() == "DIRECT" else e for e in avg_vals["exchanges"] ]
        
        # Verify exchanges given are allowed
        all_exchanges = [ "Direct", "P12" ]
        for e in ex:
            if e not in all_exchanges:
                raise ValueError( "%s not in allowed exchanges." %( e ) )
        
        # Create array for integer exchanges
        ex_int = []
        for e, i in zip( all_exchanges, [0,1,2,3] ):
            if e in ex:
                if ( i > 0 ) and ( 0 not in ex_int ):
                    raise ValueError( "%s exchange energies cannot be calculated without Direct term." %( e ) )
                ex_int.append( i )
    else:
        raise ValueError( "exchanges must be specified in the input yaml file. Options are Direct, P12, P34, P12P34." )
    
    # Get file names of files containing integrals
    # Verify files exist
    if "omega" in avg_vals:
        if not isinstance( avg_vals["omega"], list ):
            w = [ str( i ) for i in [ avg_vals["omega"] ] ]
        else:
            w = [ str( i ) for i in avg_vals["omega"] ]
    else:
        raise ValueError( "omega must be specified in the input yaml file." )
    
    out_files = []
    for i in w:
        out_files.append( "average.%s,%s" %( ex[-1], i ) )

    # Getting list of Todd output files
    ci_files = []
    base_out_files = [ "ci.", "Toddss" ]
    for i, base in enumerate( base_out_files ):
        for i in w:
            ci_files.append( base+ex[-1]+","+i )
    
    # Symbolicly link integral files to energy calculation directory
    os.chdir( "./Bound_State/Average_values/" )
    dir_files = os.listdir( "./" )
    for ifile in ci_files:
        if ifile in dir_files:
            raise ValueError( "%s file already exists in directory: ./Bound_State/Average_values/" %( ifile ) )
        os.popen( "ln -sf ../../%s ./" %( ci_dir + ifile ) ).read()
    
    # Change for loops in run script and compiler in Makefile
    os.popen( "sed -i '/for omega in/c\%s' run.sh" %( "for omega in "+" ".join( w ) ) ).read()
    os.popen( "sed -i '/for e in/c\%s' run.sh" %( "    for e in "+str( max( ex_int ) ) ) ).read()
    os.popen( "sed -i '/FC=/c\FC=%s' Makefile" %( compiler ) ).read()

    # Run code for given parameters
    os.popen( "./run.sh" ).read()

    # Remove sym linked integral files
    for ifile in ci_files:
        os.popen( "rm %s" %( ifile ) ).read()
    
    # Move output files to root directory
    for ofile in out_files:
        os.popen( "mv %s ../../" %( ofile ) ).read()
    os.chdir( "../../" )

def cli():
    args = parser.parse_args()

    # List available calculations
    if args.list is True:
        print( "Available calculations:" )
        print( "     Integral calculations for various omega values" )
        print( "     Energy calculations from perviously calculated integrals" )
        print( "          Todd method" )
        print( "          Lowden method" )
        print( "     Average values from Todd method calculations" )
        exit()
    
    if (input_file := args.input ) != "" and ".yaml" in args.input:
        with open( input_file ) as f:
            yaml_contents = "\n".join( f.readlines() )
        calcs = yaml.load( yaml_contents, yaml.SafeLoader )
    else:
        raise Exception( "Input file")
    
    if "integrals" in calcs:
        integrals( calcs['integrals'] )

    if "energy" in calcs:
        energy( calcs["energy"] )
    
    if "averages" in calcs:
        average_vals( calcs["averages"] )

if __name__ == "__main__":
    cli()